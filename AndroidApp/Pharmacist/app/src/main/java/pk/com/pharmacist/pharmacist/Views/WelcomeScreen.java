package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pk.com.pharmacist.pharmacist.BackGroundTasks.TokenVerifier;
import pk.com.pharmacist.pharmacist.R;

public class WelcomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);


        Thread logoScreen=new Thread(){
            @Override
            public void run(){
                try{
                    super.run();
                    sleep(2500);
                }
                catch (Exception ex){

                }
                finally {
                    Intent i=new Intent(WelcomeScreen.this,MainActivity.class);

                    startActivity(i);
                    finish();
                }
            }
        };

        logoScreen.start();
    }



}
