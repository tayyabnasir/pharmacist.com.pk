package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.SignUpDTO;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;

/**
 * Created by Recursive on 4/6/2017.
 */

public class SignUpHandler extends AsyncTask <SignUpDTO,Void,HttpResponse> {
    private Context context;

    public SignUpHandler(Context ctx){
        context=ctx;
    }


    @Override
    protected HttpResponse doInBackground(SignUpDTO... params) {
        URL url= CommonCode.getUrl("http://10.0.2.2:80/api/AccountsAPI/SignUp");

        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler handler=new WebRequestHandler();

        return handler.CommunicateViaPOST(url,params[0]);
    }

    @Override
    protected void onPostExecute(HttpResponse response) {
        if(response!=null && response.getStatusCode()==200){
            Toast.makeText(context,"Sign Up Success. A verification email is sent.",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(context,"Sign Up Fail. Please check your email for uniquness",Toast.LENGTH_LONG).show();
        }
    }
}
