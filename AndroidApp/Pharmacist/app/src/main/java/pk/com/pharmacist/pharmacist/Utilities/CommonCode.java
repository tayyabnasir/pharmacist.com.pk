package pk.com.pharmacist.pharmacist.Utilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Recursive on 4/4/2017.
 */

public class CommonCode {
    //check for null on result
    public static URL getUrl(String url){
        URL loginURL=null;
        try {
            loginURL=new URL(url);
        } catch (MalformedURLException e) {

            return null;
        }
        return loginURL;
    }

}
