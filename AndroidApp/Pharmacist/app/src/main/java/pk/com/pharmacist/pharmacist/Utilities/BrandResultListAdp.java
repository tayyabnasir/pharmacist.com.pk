package pk.com.pharmacist.pharmacist.Utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pk.com.pharmacist.pharmacist.Models.MedicineAmoutPair;
import pk.com.pharmacist.pharmacist.R;

/**
 * Created by Recursive on 4/6/2017.
 */

public class BrandResultListAdp extends ArrayAdapter {
    private Context context;
    private List<MedicineAmoutPair> medicineList;

    public BrandResultListAdp(Context ctx, int resource, List<MedicineAmoutPair> medList) {
        super(ctx, resource, medList);

        context=ctx;
        medicineList=medList;
    }

    @Override
    public int getCount(){
        return medicineList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View layout=null;
        ViewHolder views=null;

        if(convertView==null){
            LayoutInflater inf= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            layout=inf.inflate(R.layout.brand_search_result_cell,null,true);

            views=new ViewHolder();

            views.serialNumTxt= (TextView) layout.findViewById(R.id.brandResultSerialNum);
            views.brandNameTxt= (TextView) layout.findViewById(R.id.brandResultBrandTxt);
            views.saltTxt= (TextView) layout.findViewById(R.id.brandResultSaltTxt);
            views.retailPriceTxt=(TextView) layout.findViewById(R.id.brandResultRetailPriceTxt);
            views.requiredAmountTxt= (TextView) layout.findViewById(R.id.brandResultRequiredAmountTxt);

            layout.setTag(views);
        }
        else{
            layout=convertView;
            views=(ViewHolder) convertView.getTag();
        }

        //now set values for a cell

        MedicineAmoutPair medObj=medicineList.get(position);

        int snum=position+1;
        views.serialNumTxt.setText(""+snum);
        views.brandNameTxt.setText(medObj.medicine.BrandName);
        views.saltTxt.setText(medObj.medicine.Salt);
        views.retailPriceTxt.setText(medObj.medicine.TradePrice.toString());
        views.requiredAmountTxt.setText(""+medObj.RequiredAmount);

        return layout;
    }


    public class ViewHolder{
        public TextView serialNumTxt;
        public TextView brandNameTxt;
        public TextView saltTxt;
        public TextView retailPriceTxt;
        public TextView requiredAmountTxt;
    }
}
