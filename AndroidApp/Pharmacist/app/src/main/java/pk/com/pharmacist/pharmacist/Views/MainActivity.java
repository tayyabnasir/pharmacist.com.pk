package pk.com.pharmacist.pharmacist.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pk.com.pharmacist.pharmacist.BackGroundTasks.TokenVerifier;
import pk.com.pharmacist.pharmacist.R;

public class MainActivity extends AppCompatActivity {

    Button ExitBtn;
    Button ContinueBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ExitBtn= (Button) findViewById(R.id.exitBtn);
        ContinueBtn= (Button) findViewById(R.id.getStartedBtn);

        ContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckForToken();
            }
        });

        ExitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });

    }
    private void CheckForToken(){
        new TokenVerifier(this).execute();
    }

}
