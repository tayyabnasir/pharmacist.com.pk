package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pk.com.pharmacist.pharmacist.R;

public class SearchByBrand extends AppCompatActivity {
    private EditText brandTxt;
    private Button brandSearchBtn;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_brand);

        token=this.getIntent().getStringExtra("token");

        brandSearchBtn= (Button) findViewById(R.id.brandNameSearchBtn);
        brandTxt= (EditText) findViewById(R.id.brandFieldTxt);

        brandSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String brandName=brandTxt.getText().toString();

                if(brandName==null || brandName.equals("")){
                    Toast.makeText(SearchByBrand.this,"Please Enter a Salt Name",Toast.LENGTH_SHORT).show();

                }
                else{
                    //forward the salt to next activity
                    Intent i=new Intent(SearchByBrand.this,BrandSearchResult.class);

                    i.putExtra("Brand",brandName);
                    i.putExtra("token",token);

                    brandTxt.setText("");

                    startActivity(i);
                }
            }
        });
    }
}
