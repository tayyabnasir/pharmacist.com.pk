package pk.com.pharmacist.pharmacist.Models;

/**
 * Created by Recursive on 4/6/2017.
 */

public class TokenSearchStringPair {
    private String token;
    private String searchStr;

    public String getSearchStr() {
        return searchStr;
    }

    public void setSearchStr(String searchStr) {
        this.searchStr = searchStr;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
