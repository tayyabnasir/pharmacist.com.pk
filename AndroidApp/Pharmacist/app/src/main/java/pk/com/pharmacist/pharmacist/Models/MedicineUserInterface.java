package pk.com.pharmacist.pharmacist.Models;

import java.io.Serializable;

/**
 * Created by Recursive on 4/6/2017.
 */

public class MedicineUserInterface implements Serializable{
    public int MedicineId;
    public String BrandName ;
    public String Manufacturer ;
    public String DrugForm ;
    public String Salt ;
    public String PackSize ;
    public String SaltAmount ;
    public Double PricePerUnit ;
    public Double TradePrice ;
}
