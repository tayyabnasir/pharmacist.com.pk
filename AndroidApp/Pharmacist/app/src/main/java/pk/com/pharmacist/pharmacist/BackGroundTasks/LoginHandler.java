package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.LoginDTO;
import pk.com.pharmacist.pharmacist.Models.Token;
import pk.com.pharmacist.pharmacist.Models.TokenError;
import pk.com.pharmacist.pharmacist.Models.TokenUserNamePair;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.GsonConverter;
import pk.com.pharmacist.pharmacist.Utilities.SharedPreferencesTasks;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;
import pk.com.pharmacist.pharmacist.Views.SearchSelectScreen;

/**
 * Created by Recursive on 4/5/2017.
 */

public class LoginHandler extends AsyncTask <LoginDTO,Void,HttpResponse>{
    private Context context;
    private LoginDTO dto;

    public LoginHandler(Context ctx){
        context=ctx;
    }


    private HttpResponse VerifyLogin(LoginDTO dto){
        //create the url
        URL url= CommonCode.getUrl("http://10.0.2.2:80/token");
        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler handler=new WebRequestHandler();

        return handler.GetAccessToken(url,dto);
    }


    @Override
    protected HttpResponse doInBackground(LoginDTO... params) {
        dto=params[0];
        return VerifyLogin(dto);
    }

    @Override
    protected void onPostExecute(HttpResponse response){
        if(response!=null && response.getStatusCode()==200){
            Toast.makeText(context,"Login Success",Toast.LENGTH_SHORT).show();
            GetTokenAndStoreInSharedPref(response);
            Intent i=new Intent(context,SearchSelectScreen.class);

            context.startActivity(i);
            ((Activity)context).finish();
        }else{
            Toast.makeText(context,"Login Failed. Please Try again.",Toast.LENGTH_LONG).show();
        }
    }

    private Boolean GetTokenAndStoreInSharedPref(HttpResponse response){
        //store token in sp
        Token token= (Token) GsonConverter.ParseFromJson(response.getResposne(),Token.class);
        
        return SharedPreferencesTasks.WriteTokenEmailToSharedPreferences(new TokenUserNamePair(token.getAccess_token(),dto.getUsername()),context);
    }

    //not functional yet
    private void GetAndDisplayError(HttpResponse response){
        TokenError error= (TokenError) GsonConverter.ParseFromJson(response.getResposne(),TokenError.class);

        Toast.makeText(context,error.getError_description(),Toast.LENGTH_LONG).show();
    }
}
