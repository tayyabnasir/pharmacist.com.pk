package pk.com.pharmacist.pharmacist.Utilities;

import com.google.gson.Gson;

import pk.com.pharmacist.pharmacist.Models.LoginDTO;

/**
 * Created by Recursive on 4/5/2017.
 */

public class GsonConverter {
    public static String ParseToJson(Object obj){
        Gson gson=new Gson();

        return gson.toJson(obj);
    }

    public static Object ParseFromJson(String jsonData,Class dtoType){
        Gson gson=new Gson();
        return gson.fromJson(jsonData,dtoType);
    }
}
