package pk.com.pharmacist.pharmacist.Models;

/**
 * Created by Recursive on 4/5/2017.
 */
//not functional yet
public class TokenError {
    private String error;
    private String error_description;

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
