package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import pk.com.pharmacist.pharmacist.BackGroundTasks.GetUserInfo;
import pk.com.pharmacist.pharmacist.Models.TokenUserNamePair;
import pk.com.pharmacist.pharmacist.Models.UserEmailName;
import pk.com.pharmacist.pharmacist.R;
import pk.com.pharmacist.pharmacist.Utilities.SharedPreferencesTasks;

public class SearchSelectScreen extends AppCompatActivity {

    public UserEmailName userDTO;
    private Button saltSearchBtn;
    private Button brandSearchBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_select_screen);

        final TokenUserNamePair token=GetTokenInfo();

        if(token==null){
            Intent i=new Intent(this,LoginScreen.class);
            startActivity(i);

            finish();
        }
        else{
            ShowUserInfo(token);
        }


        //set events
        //setting the events on buttons
        saltSearchBtn= (Button) findViewById(R.id.saltSearchBtnCoose);
        brandSearchBtn= (Button) findViewById(R.id.brandSearchBtnCoose);

        saltSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SearchSelectScreen.this,SearchBySalt.class);
                i.putExtra("token",token.getToken());
                startActivity(i);
            }
        });

        brandSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SearchSelectScreen.this,SearchByBrand.class);
                i.putExtra("token",token.getToken());
                startActivity(i);
            }
        });
    }

    private TokenUserNamePair GetTokenInfo(){
        return SharedPreferencesTasks.ReadTokenEmail(this);
    }

    private void ShowUserInfo(TokenUserNamePair dto){
        //get from shared preference the

        new GetUserInfo(this).execute(dto.getToken());
    }
}
