package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.UserEmailName;
import pk.com.pharmacist.pharmacist.R;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.GsonConverter;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;
import pk.com.pharmacist.pharmacist.Views.LoginScreen;
import pk.com.pharmacist.pharmacist.Views.SearchSelectScreen;

/**
 * Created by Recursive on 4/5/2017.
 */

public class GetUserInfo extends AsyncTask<String,Void,HttpResponse> {
    private Context context;


    public GetUserInfo(Context ctx){
        context=ctx;
    }

    private HttpResponse GetUserInfoFromAPI(String token){
        //first make a url
        URL url= CommonCode.getUrl("http://10.0.2.2:80/api/AccountsAPI/GetLoggedInUserDTO");

        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler obj=new WebRequestHandler();

        return obj.CommunicateWithAPI(url,token);
    }

    @Override
    protected HttpResponse doInBackground(String... params) {
        return GetUserInfoFromAPI(params[0]);
    }

    @Override
    protected void onPostExecute(HttpResponse response) {
        if(response!=null && response.getStatusCode()==200){
            //display the user info
            UserEmailName dto= (UserEmailName) GsonConverter.ParseFromJson(response.getResposne(),UserEmailName.class);

            ((SearchSelectScreen)context).userDTO=dto;

            TextView msg= (TextView) ((SearchSelectScreen) context).findViewById(R.id.homeScreenWelcomeMsg);

            msg.setText(msg.getText()+"\n"+dto.getName());
        }
        else{
            //go to login
            Intent i=new Intent(context, LoginScreen.class);

            context.startActivity(i);

            ((Activity)context).finish();
        }
    }
}
