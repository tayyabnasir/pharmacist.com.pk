package pk.com.pharmacist.pharmacist.Models;

/**
 * Created by Recursive on 4/6/2017.
 */

public class TokenUserNamePair {
    private String token;
    private String username;

    public TokenUserNamePair(String t,String u){
        token=t;
        username=u;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
