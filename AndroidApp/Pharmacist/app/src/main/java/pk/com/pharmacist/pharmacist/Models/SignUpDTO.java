package pk.com.pharmacist.pharmacist.Models;

/**
 * Created by Recursive on 4/5/2017.
 */

public class SignUpDTO {
    private String Email;
    private String FirstName;
    private String LastName;
    private String Password;

    public String getEmail() {
        return Email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
