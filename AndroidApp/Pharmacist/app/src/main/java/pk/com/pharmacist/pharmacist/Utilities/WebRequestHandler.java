package pk.com.pharmacist.pharmacist.Utilities;


import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.LoginDTO;
import pk.com.pharmacist.pharmacist.Models.SignUpDTO;

/**
 * Created by Recursive on 4/4/2017.
 */

public class WebRequestHandler {
    private void AddProperties(HttpURLConnection conn,String token) throws ProtocolException {
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + token);
        conn.setDoInput(true);
        conn.setRequestMethod("GET");
    }

    private void AddPropertiesForGET(HttpURLConnection conn) throws ProtocolException {
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("GET");
    }

    private void AddPropertiesForPOST(HttpURLConnection conn) throws ProtocolException {
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestMethod("POST");
    }


    public HttpResponse CommunicateViaPOST(URL url, Object dto){
        HttpURLConnection conn=null;
        try{
            //1. open connection
            conn= (HttpURLConnection)url.openConnection();

            //2. set method to be post and datatype to be json
            AddPropertiesForPOST(conn);


            conn.connect();

            //3. convert data to post to json
            String dataToSend=GsonConverter.ParseToJson(dto);

            //4. write data to server
            OutputStream out=conn.getOutputStream();
            BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(out,"UTF-8"));

            bufferedWriter.write(dataToSend);
            //flush data
            bufferedWriter.close();
            out.close();

            InputStream inp=conn.getInputStream();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inp));
            String response="";
            String buff="";

            while ((buff=bufferedReader.readLine())!=null){

                response+=buff;
            }

            //flush data
            bufferedReader.close();
            inp.close();

            return new HttpResponse(response,conn.getResponseCode());
        }
        catch (ProtocolException ex){

            return null;
        } catch (IOException e) {

            return null;
        }
    }

    public HttpResponse CommunicateViaGET(URL url){
        HttpURLConnection conn=null;
        try{
            //1. open connection
            conn= (HttpURLConnection)url.openConnection();

            //2. set method to be post and datatype to be json
            AddPropertiesForGET(conn);

            conn.connect();

            //3. now read resonse back from server
            InputStream inp=conn.getInputStream();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inp));
            String response="";
            String buff="";

            while ((buff=bufferedReader.readLine())!=null){
                response+=buff;
            }

            //flush data
            bufferedReader.close();
            inp.close();

            return new HttpResponse(response,conn.getResponseCode());
        }
        catch (ProtocolException ex){
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public HttpResponse CommunicateWithAPI(URL url,String token){
        HttpURLConnection conn=null;
        try{
            //1. open connection
            conn= (HttpURLConnection)url.openConnection();

            //2. set method to be post and datatype to be json
            AddProperties(conn,token);

            conn.connect();

            //3. now read resonse back from server
            InputStream inp=conn.getInputStream();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inp));
            String response="";
            String buff="";

            while ((buff=bufferedReader.readLine())!=null){

                response+=buff;
            }

            //flush data
            bufferedReader.close();
            inp.close();

            return new HttpResponse(response,conn.getResponseCode());
        }
        catch (ProtocolException ex){

            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public HttpResponse GetAccessToken(URL url,LoginDTO dto){
        HttpResponse httpReponse=new HttpResponse();

        try{
            HttpURLConnection conn= (HttpURLConnection) url.openConnection();
            String parameter="grant_type=password&username="+dto.getUsername()+"&password="+dto.getPassword();

            conn.setDoOutput(true);

            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            conn.setRequestProperty("Content_Length",""+parameter.length());

            conn.setRequestMethod("POST");
            conn.connect();

            OutputStreamWriter out=new OutputStreamWriter(conn.getOutputStream());

            out.write(parameter);

            out.flush();

            out.close();

            int resposnseCode=conn.getResponseCode();

            InputStream inp=conn.getInputStream();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inp));
            String response="";
            String buff="";

            while ((buff=bufferedReader.readLine())!=null){

                response+=buff;
            }
            //flush data
            bufferedReader.close();
            inp.close();


            httpReponse.setResposne(response);
            httpReponse.setStatusCode(resposnseCode);
        }
        catch (Exception ex){
            return null;
        }

        return httpReponse;
    }
}
