package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pk.com.pharmacist.pharmacist.R;

public class SearchBySalt extends AppCompatActivity {

    private EditText saltTxt;
    private Button saltSearchBtn;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_salt);

        token=this.getIntent().getStringExtra("token");

        //get the views
        saltSearchBtn= (Button) findViewById(R.id.saltSearchBtn);
        saltTxt= (EditText) findViewById(R.id.saltFieldTxt);

        //set click action on button
        saltSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String saltName=saltTxt.getText().toString();

                if(saltName==null || saltName.equals("")){
                    Toast.makeText(SearchBySalt.this,"Please Enter a Salt Name",Toast.LENGTH_SHORT).show();

                }
                else{
                    //forward the salt to next activity
                    Intent i=new Intent(SearchBySalt.this,SaltSearchResult.class);

                    i.putExtra("Salt",saltName);
                    i.putExtra("token",token);

                    saltTxt.setText("");

                    startActivity(i);
                }
            }
        });
    }
}
