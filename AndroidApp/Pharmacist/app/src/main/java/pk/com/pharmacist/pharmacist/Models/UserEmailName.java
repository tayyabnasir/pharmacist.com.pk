package pk.com.pharmacist.pharmacist.Models;

/**
 * Created by Recursive on 4/5/2017.
 */

public class UserEmailName {
    private String Name;
    private String Email;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
