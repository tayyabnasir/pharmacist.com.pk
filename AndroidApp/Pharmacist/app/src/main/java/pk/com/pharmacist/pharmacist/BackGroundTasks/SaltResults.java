package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.MedicineAmoutPair;
import pk.com.pharmacist.pharmacist.Models.MedicineUserInterface;
import pk.com.pharmacist.pharmacist.Models.TokenSearchStringPair;
import pk.com.pharmacist.pharmacist.R;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.GsonConverter;
import pk.com.pharmacist.pharmacist.Utilities.SaltResultListAdp;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;
import pk.com.pharmacist.pharmacist.Views.MedicineDetailScreen;

/**
 * Created by Recursive on 4/6/2017.
 */

public class SaltResults extends AsyncTask <TokenSearchStringPair,Void,List<MedicineUserInterface>>{
    private Context context;

    public SaltResults(Context ctx){
        context=ctx;
    }


    private List<MedicineUserInterface> GetSaltResults(String urlStr, String token){
        URL url= CommonCode.getUrl(urlStr);

        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler handler=new WebRequestHandler();
        HttpResponse resposne=handler.CommunicateWithAPI(url,token);

        return GetDataFromResponse(resposne);
    }

    private List<MedicineUserInterface> GetDataFromResponse(HttpResponse response){
        if(response!=null && response.getStatusCode()==200){
            MedicineUserInterface[] medObj= (MedicineUserInterface[]) GsonConverter.ParseFromJson(response.getResposne(),MedicineUserInterface[].class);

            List<MedicineUserInterface> dto=new ArrayList<MedicineUserInterface>(Arrays.asList(medObj));

            return dto;
        }
        else{
            return null;
        }
    }

    @Override
    protected List<MedicineUserInterface> doInBackground(TokenSearchStringPair... params) {
        TokenSearchStringPair dto=params[0];

        String url="http://10.0.2.2/api/SearchResults/GetAlternatesForSalt?Salt="+dto.getSearchStr();

        return GetSaltResults(url,dto.getToken());
    }


    @Override
    protected void onPostExecute(final List<MedicineUserInterface> medObj){
        //if medObj is not null then pass it to List adapter
        if(medObj==null){
            Toast.makeText(context,"No records Found",Toast.LENGTH_LONG).show();
        }
        else{
            SaltResultListAdp adp=new SaltResultListAdp(context, R.layout.salt_search_result_cell,medObj);
            ListView listView= (ListView) ((Activity)context).findViewById(R.id.saltResList);
            listView.setAdapter(adp);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MedicineUserInterface med=medObj.get(position);

                    Intent i=new Intent(context, MedicineDetailScreen.class);

                    i.putExtra("MedObj",med);

                    ((Activity)context).startActivity(i);
                }
            });
        }

    }

}
