package pk.com.pharmacist.pharmacist.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import pk.com.pharmacist.pharmacist.Models.Token;
import pk.com.pharmacist.pharmacist.Models.TokenUserNamePair;

/**
 * Created by Recursive on 4/6/2017.
 */

public class SharedPreferencesTasks {
    public static TokenUserNamePair ReadTokenEmail(Context ctx){
        SharedPreferences preference = ctx.getSharedPreferences("PharmacistSP", Context.MODE_PRIVATE);
        if (preference.contains("token") && preference.contains("username")) {
            String token=preference.getString("token",null);
            String username=preference.getString("username",null);

            return new TokenUserNamePair(token,username);
        }
        else{
            return null;
        }
    }

    public static Boolean WriteTokenEmailToSharedPreferences(TokenUserNamePair dto,Context ctx){
        SharedPreferences pref=ctx.getSharedPreferences("PharmacistSP", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=pref.edit();



        editor.putString("username",dto.getUsername());
        editor.putString("token",dto.getToken());
        editor.apply();

        return true;
    }
}
