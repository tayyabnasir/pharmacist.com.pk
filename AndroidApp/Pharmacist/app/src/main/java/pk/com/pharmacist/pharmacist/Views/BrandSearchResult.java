package pk.com.pharmacist.pharmacist.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pk.com.pharmacist.pharmacist.BackGroundTasks.BrandResults;
import pk.com.pharmacist.pharmacist.Models.TokenSearchStringPair;
import pk.com.pharmacist.pharmacist.R;

public class BrandSearchResult extends AppCompatActivity {
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_search_result);

        token=this.getIntent().getStringExtra("token");

        String brandName=getIntent().getStringExtra("Brand");

        TokenSearchStringPair pair=new TokenSearchStringPair();
        pair.setSearchStr(brandName);
        pair.setToken(token);

        new BrandResults(this).execute(pair);
    }
}
