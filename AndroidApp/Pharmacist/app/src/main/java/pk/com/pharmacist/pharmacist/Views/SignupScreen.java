package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pk.com.pharmacist.pharmacist.BackGroundTasks.SignUpHandler;
import pk.com.pharmacist.pharmacist.Models.SignUpDTO;
import pk.com.pharmacist.pharmacist.R;

public class SignupScreen extends AppCompatActivity {

    Button signUpBtn;
    EditText email;
    EditText password;
    EditText fname;
    EditText lname;
    TextView redirectToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_screen);

        //init the views
        signUpBtn= (Button) findViewById(R.id.signUpBtn);
        email= (EditText) findViewById(R.id.emailTxt);
        password= (EditText) findViewById(R.id.passwordTxt);
        fname= (EditText) findViewById(R.id.fnameTxt);
        lname= (EditText) findViewById(R.id.lnameTxt);
        redirectToLogin= (TextView) findViewById(R.id.signInRedirect);

        //set event for the SignUp Btn
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailStr=email.getText().toString();
                String passwordStr=password.getText().toString();
                String fnameStr=fname.getText().toString();
                String lnameStr=lname.getText().toString();

                //verify if fields are valid
                if(!ValidateSignUpFields(emailStr, passwordStr,fnameStr, lnameStr)){
                    //in case of empty field

                    Toast.makeText(SignupScreen.this,"Please Fill in All Fields",Toast.LENGTH_SHORT).show();
                }
                else{
                    //put all strings in DTO and try to hit server
                    SignUpDTO signUpDTO=PutDataInSignUpDTO(emailStr,passwordStr,fnameStr,lnameStr);

                    new SignUpHandler(SignupScreen.this).execute(signUpDTO);
                }
            }
        });

        redirectToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SignupScreen.this,LoginScreen.class);

                startActivity(i);

                finish();
            }
        });
    }

    private SignUpDTO PutDataInSignUpDTO(String email, String password, String fname, String lname){
        SignUpDTO dto=new SignUpDTO();

        dto.setPassword(password);
        dto.setEmail(email);
        dto.setFirstName(fname);
        dto.setLastName(lname);
        return dto;
    }

    private Boolean ValidateSignUpFields(String email,String password,String fname,String lname){
        if(email==null || email.equals("")){
            return false;
        }
        if(password==null || password.equals("")){
            return false;
        }
        if(fname==null || fname.equals("")){
            return false;
        }
        if(lname==null || lname.equals("")){
            return false;
        }

        return true;
    }
}
