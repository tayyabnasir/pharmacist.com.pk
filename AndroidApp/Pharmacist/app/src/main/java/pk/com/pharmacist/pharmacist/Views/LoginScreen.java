package pk.com.pharmacist.pharmacist.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pk.com.pharmacist.pharmacist.BackGroundTasks.LoginHandler;
import pk.com.pharmacist.pharmacist.Models.LoginDTO;
import pk.com.pharmacist.pharmacist.R;

public class LoginScreen extends AppCompatActivity {

    Button loginBtn;
    EditText emailTxt;
    EditText passwordTxt;
    TextView forgetPassword;
    TextView redirectToSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        loginBtn= (Button) findViewById(R.id.loginBtn);
        emailTxt= (EditText) findViewById(R.id.emailTxtLogin);
        passwordTxt= (EditText) findViewById(R.id.passwordTxtLogin);
        forgetPassword= (TextView) findViewById(R.id.forgetPasswordLink);
        redirectToSignUp= (TextView) findViewById(R.id.signUpRedirect);

        //set the click listner for the Login Button
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=emailTxt.getText().toString();
                String password=passwordTxt.getText().toString();

                if(IsLoginValid(email,password)==false){
                    //display message of filling in all fields
                    Toast.makeText(LoginScreen.this,"Please Fill in All Fields",Toast.LENGTH_SHORT).show();
                }
                else{
                    //start the back ground acivity to
                    LoginDTO lgnDTO=new LoginDTO();
                    lgnDTO.setUsername(email);
                    lgnDTO.setPassword(password);

                    new LoginHandler(LoginScreen.this).execute(lgnDTO);
                }
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=emailTxt.getText().toString();
                if(email==null || email.equals("")){
                    Toast.makeText(LoginScreen.this,"Please Enter Email or Username",Toast.LENGTH_SHORT).show();
                }
                else{

                }
            }
        });

        redirectToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginScreen.this,SignupScreen.class);
                startActivity(i);
                finish();
            }
        });
    }

    //check if the email and login are not empty
    private Boolean IsLoginValid(String email,String password){
        if(email==null || email.equals("") || password==null || password.equals("")){
            return false;
        }
        else{
            return true;
        }
    }


}
