package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Models.TokenUserNamePair;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.SharedPreferencesTasks;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;
import pk.com.pharmacist.pharmacist.Views.LoginScreen;
import pk.com.pharmacist.pharmacist.Views.MainActivity;
import pk.com.pharmacist.pharmacist.Views.SearchSelectScreen;
import pk.com.pharmacist.pharmacist.Views.WelcomeScreen;

/**
 * Created by Recursive on 4/4/2017.
 */

public class TokenVerifier extends AsyncTask <Void,String,HttpResponse> {

    Context ctx;

    public TokenVerifier(Context ctx){
        this.ctx=ctx;
    }

    private String ReadSharedPreference(){
        TokenUserNamePair temp= SharedPreferencesTasks.ReadTokenEmail(ctx);

        if(temp!=null){
            return temp.getToken();
        }
        else{
            return null;
        }
    }

    private HttpResponse IsTokenValid(String token){
        URL url= CommonCode.getUrl("http://10.0.2.2:80/api/AccountsAPI/IsLoggedIn");

        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler obj=new WebRequestHandler();

        return obj.CommunicateWithAPI(url,token);
    }


    @Override
    protected void onPostExecute(HttpResponse s) {

        if(s!=null && s.getStatusCode()==200){
            //go to main screen
            Intent i=new Intent(ctx,SearchSelectScreen.class);
            Toast.makeText(ctx,"Welcome Back",Toast.LENGTH_SHORT);
            ctx.startActivity(i);
        }
        else{
            //goto login screen
            Intent i=new Intent(ctx,LoginScreen.class);

            ctx.startActivity(i);

        }
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        //read from shared preference that there is bearer token set
        String token=ReadSharedPreference();
        if(token==null || token.equals("")){
            return null;
        }
        else{
            //do nothing
        }
        //validate token
        HttpResponse response=IsTokenValid(token);

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
