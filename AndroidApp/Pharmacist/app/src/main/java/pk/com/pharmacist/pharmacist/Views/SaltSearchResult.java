package pk.com.pharmacist.pharmacist.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pk.com.pharmacist.pharmacist.BackGroundTasks.BrandResults;
import pk.com.pharmacist.pharmacist.BackGroundTasks.SaltResults;
import pk.com.pharmacist.pharmacist.Models.TokenSearchStringPair;
import pk.com.pharmacist.pharmacist.R;

public class SaltSearchResult extends AppCompatActivity {
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salt_search_result);

        token=this.getIntent().getStringExtra("token");
        //get salt name from the intent
        String saltName=getIntent().getStringExtra("Salt");

        TokenSearchStringPair pair=new TokenSearchStringPair();
        pair.setSearchStr(saltName);
        pair.setToken(token);

        new SaltResults(this).execute(pair);

    }
}
