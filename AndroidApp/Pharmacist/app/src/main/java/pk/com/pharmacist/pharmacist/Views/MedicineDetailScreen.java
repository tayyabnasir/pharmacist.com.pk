package pk.com.pharmacist.pharmacist.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import pk.com.pharmacist.pharmacist.Models.MedicineUserInterface;
import pk.com.pharmacist.pharmacist.R;

public class MedicineDetailScreen extends AppCompatActivity {

    private TextView BrandName ;
    private TextView Manufacturer ;
    private TextView DrugForm ;
    private TextView Salt ;
    private TextView PackSize ;
    private TextView RetailPrice ;
    private TextView SaltAmount ;
    private TextView PricePerUnit ;
    private TextView TradePrice ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_detail_screen);

        MedicineUserInterface medObj= (MedicineUserInterface) getIntent().getExtras().getSerializable("MedObj");

        //put the values in the fields
        BrandName = (TextView) findViewById(R.id.brandNameLabel);
        BrandName.setText(medObj.BrandName);
        Manufacturer = (TextView) findViewById(R.id.manufacturerLabel);
        Manufacturer.setText(medObj.Manufacturer);
        DrugForm = (TextView) findViewById(R.id.formLabel);
        DrugForm.setText(medObj.DrugForm);
        Salt = (TextView) findViewById(R.id.saltLabel);
        Salt.setText(medObj.Salt);
        PackSize = (TextView) findViewById(R.id.packLabel);
        PackSize.setText(medObj.PackSize);
        RetailPrice = (TextView) findViewById(R.id.medidLabel);
        RetailPrice.setText(""+medObj.MedicineId);
        SaltAmount = (TextView) findViewById(R.id.amountLabel);
        SaltAmount.setText(medObj.SaltAmount);
        PricePerUnit = (TextView) findViewById(R.id.ppuLabel);
        PricePerUnit.setText(medObj.PricePerUnit.toString());
        TradePrice = (TextView) findViewById(R.id.tradeLabel);
        TradePrice.setText(medObj.TradePrice.toString());
    }
}
