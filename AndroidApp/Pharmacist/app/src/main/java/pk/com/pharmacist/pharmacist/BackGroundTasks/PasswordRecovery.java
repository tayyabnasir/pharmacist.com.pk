package pk.com.pharmacist.pharmacist.BackGroundTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.net.URL;

import pk.com.pharmacist.pharmacist.Models.HttpResponse;
import pk.com.pharmacist.pharmacist.Utilities.CommonCode;
import pk.com.pharmacist.pharmacist.Utilities.WebRequestHandler;

/**
 * Created by Recursive on 4/5/2017.
 */

public class PasswordRecovery extends AsyncTask<String,String,HttpResponse> {
    private Context context;

    PasswordRecovery(Context ctx){
        context=ctx;
    }

    private HttpResponse RecoverPassword(String email){
        URL url= CommonCode.getUrl("http://api/AccountsAPI/RecoverPassword?email="+email);

        if(url==null){
            return null;
        }
        else{
            //do nothing
        }

        WebRequestHandler handler=new WebRequestHandler();

        return handler.CommunicateViaGET(url);
    }

    @Override
    protected HttpResponse doInBackground(String... params) {
        return RecoverPassword(params[0]);
    }

    @Override
    protected void onPostExecute(HttpResponse reponse){
        if(reponse!=null && reponse.getStatusCode()==200){
            Toast.makeText(context,"An email was sent to you from where you can recover your password",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(context,"Email sending Failed. Try again later.",Toast.LENGTH_LONG).show();
        }
    }
}
