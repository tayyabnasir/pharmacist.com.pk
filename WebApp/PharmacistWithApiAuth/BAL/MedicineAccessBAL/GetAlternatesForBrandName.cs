﻿using Models;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.MedicineAccessBAL
{
    //contains helper functions for implemnting logic to get 
    //alternates for a brandname
    public class GetAlternatesForBrandName
    {
        

        /// For single salt functions-----------------------

        //function to calculte required amount for single salt
        //this calculates the desired amount of the alternates
        public MedicineAmoutPair AlternativesRequiredAmount(Medicine entered, Medicine Alternative)
        {
            MedicineAmoutPair obj = new MedicineAmoutPair();
            obj.medicine = new MedicineUserInterface(Alternative);
            //init MedicneUSerInterface

            //indicate nill consider in client
            obj.RequiredAmount = -1;
            //check if salt =0 then nill in required amount
            //check if untis mismatch then nil in required amount
            if (Alternative.Salt.ElementAt(0).Amount != 0 && Alternative.Salt.ElementAt(0).Unit.ToLower().Equals(entered.Salt.ElementAt(0).Unit.ToLower()))
            {
                Double amount = entered.Salt.ElementAt(0).Amount / Alternative.Salt.ElementAt(0).Amount;
                obj.RequiredAmount = amount;
            }

            return obj;
        }

        //this for given list<Medicine> will return list<MedicineAmountPair>
        public List<MedicineAmoutPair> GetRequiredAmountCalculatedListSingleSalt(List<Medicine> medList, Int32 medid)
        {
            //first find the record for the BrandName provided by user
            Medicine entered = medList.Where(x => x.MedicineId == medid).FirstOrDefault();


            //list to store required amount calculated records
            List<MedicineAmoutPair> resultList = new List<MedicineAmoutPair>();

            //now iterate for each record and calculate the required amount
            foreach (Medicine med in medList)
            {
                resultList.Add(AlternativesRequiredAmount(entered, med));
            }

            return resultList;
        }

        /// end single salt functions-----------------------


        /// For multi salt functions
        //a function to decide whether a medicine with multiple salts
        //with same salt names is  actually equal in units and amount
        //will not be called if only single salt involved
        public Boolean IsExactAltenativeSalt(Medicine sourceSalt, Medicine destinationSalt)
        {
            //first store destination salt in a dummy holder
            List<Salt> dummy = new List<Salt>(destinationSalt.Salt);

            //no need to check for count of both being equal
            //as that already has been checked in stored in StoredProcedure


            //now for each source salt check if a ditto is availible in dummy
            //then remove that from dummy
            //in any case if matching condition is false return false
            foreach (Salt salt in sourceSalt.Salt)
            {
                //check for matching name salt(s)
                foreach (Salt dummySalt in dummy)
                {
                    //if a dummy salt and salt are equal then remove dummy 
                    //salt and start next iteration of outer loop
                    if (dummySalt.Amount == salt.Amount && dummySalt.SaltName.ToLower().Equals(salt.SaltName.ToLower()) && dummySalt.Unit.ToLower().Equals(salt.Unit.ToLower()))
                    {
                        //reomve salt from dummy list
                        dummy.Remove(dummySalt);
                        break;
                    }
                    //control will only reach here if the salts were not exact
                    //same and thus here we return false
                    return false;
                }
            }

            return true;
        }

        //function to get the records in case of mutli salts in
        //provided brandname
        public List<MedicineAmoutPair> GetRequiredAmountCalculatedListMultiSalt(List<Medicine> medList, Int32 medid)
        {
            //first get the entered medicine
            Medicine entered = medList.Where(x => x.MedicineId == medid).FirstOrDefault();


            //list to store required amount calculated records
            List<MedicineAmoutPair> resultList = new List<MedicineAmoutPair>();

            //now for every medicine check if it is actual 
            //exact alternate
            foreach (Medicine med in medList)
            {
                if (IsExactAltenativeSalt(entered, med))
                {
                    //if exact match then enter to return list
                    MedicineAmoutPair pair = new MedicineAmoutPair();
                    pair.medicine = new MedicineUserInterface(med);
                    pair.RequiredAmount = 1;

                    resultList.Add(pair);
                }
                else
                {
                    //do nothing
                }
            }

            return resultList;

        }

        /// end multi salt functions


        //Get a list of medicine records and filters out only the 
        //correct alternatives according to the application logic
        //will return null so be cautious
        public List<MedicineAmoutPair> FilterMedicineRecordsForBrandNameAlternative(List<Medicine> medicines, Int32 medid)
        {
            if (medicines.Count < 1)
            {
                return null;
            }
            else
            {
                //do nothing
            }
            //in case of only one salt
            if (medicines.ElementAt(0).Salt.Count == 1)
            {
                return GetRequiredAmountCalculatedListSingleSalt(medicines, medid);
            }
            //in case of salt combinations
            else
            {
                return GetRequiredAmountCalculatedListMultiSalt(medicines, medid);
            }
        }


        //responsible to convert Datatable to Medicine List
        public List<Medicine> ConvertDataTableToMedicineList(DataTable table)
        {
            List<Medicine> medsList = new List<Medicine>();

            foreach (DataRow row in table.Rows)
            {
                //first check if a row is marked as Deleted
                if (row.RowState == DataRowState.Deleted)
                {
                    //if deleted get to next iteration
                    continue;
                }

                //for matching medid rows get comibned Medicine object
                Int32 medid = (Int32)row["MedicineId"];
                DataRow[] matchingRows = table.Select("MedicineId=" + medid);

                Medicine refObj = Medicine.InitMedicine(matchingRows);

                if (refObj != null)
                {
                    medsList.Add(refObj);
                }
                //and remove the matching rows from the datatable
                //so that there is no repetition
                foreach (DataRow matchingRow in matchingRows)
                {
                    matchingRow.Delete();
                    //table.Rows.Remove(matchingRow);
                }

            }

            return medsList;
        }


        //return the required MedincineAmountPairs
        //may return null so be cautious
        public List<MedicineAmoutPair> GetMedicineAmountPairs(DataTable table, Int32 sourceMedId)
        {
            //1. convert the datatable into list of Medicne objects
            List<Medicine> medsList = ConvertDataTableToMedicineList(table);

            //2. filter records and return the required MedicineAmountPair
            return FilterMedicineRecordsForBrandNameAlternative(medsList, sourceMedId);
        }
    }
}
