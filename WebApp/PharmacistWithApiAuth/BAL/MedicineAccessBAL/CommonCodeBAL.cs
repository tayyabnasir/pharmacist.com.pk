﻿using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.MedicineAccessBAL
{
    //some common operations on medicine objects
    class CommonCodeBAL
    {
        //purpose of this function is to recieve a saltname string
        //and tokenize it into list of saltnames
        //caution may return null
        public List<String> GetSaltNamesList(String saltNames)
        {
            String[] salts = saltNames.Split(new char[] { '+', ',', ';', ':' }, StringSplitOptions.RemoveEmptyEntries);

            List<String> saltNamesList = new List<string>(salts);


            return saltNamesList;
        }


        //responsible to get a DataTable containing medicine records for same id
        //and merge the duplicate ids by concatinating the salt part
        //may return null so be cautious
        public List<MedicineUserInterface> GetRecordsInMedicineUserInterface(DataTable table)
        {
            //list to store the results
            List<MedicineUserInterface> medUserInterfaceList = new List<MedicineUserInterface>();

            //1. for each row find row with matching MedicineId
            //and merge them all
            foreach (DataRow row in table.Rows)
            {
                //also check if a row is marked delete then continue to next
                //iteration
                if (row.RowState == DataRowState.Deleted)
                {
                    continue;
                }

                //2. find all the matching records wiht same MedicineId
                Int32 medid = (Int32)row["MedicineId"];
                DataRow[] matchingRows = table.Select("MedicineId=" + medid);

                //3. Make a MedicineUserInterface Object
                MedicineUserInterface medObj = MedicineUserInterface.MedicineFieldInitForMedicineUserInterface(row);


                Boolean flagToCheckForFirstEntry = true;

                //4. iterate over all matching record id records and
                //merge them into one record and delete it afterwards
                foreach (DataRow matchingRow in matchingRows)
                {
                    //in case of first entry no need to append '+'
                    if (flagToCheckForFirstEntry)
                    {
                        flagToCheckForFirstEntry = false;
                        //do nothing
                    }
                    else
                    {
                        //append '+' to all salt attributes
                        medObj.Salt = medObj.Salt + " + ";
                        medObj.SaltAmount = medObj.SaltAmount + " + ";

                    }


                    //and now call function to put values from row to 
                    //object
                    medObj = MedicineUserInterface.SaltFieldInitForMedicineUserInterface(matchingRow, medObj);



                    //5. and remove the row from datatable as well
                    matchingRow.Delete();
                    //table.Rows.Remove(matchingRow);
                }
                //6. append the medObj to list
                medUserInterfaceList.Add(medObj);
            }
            //7. return the resulting list
            return medUserInterfaceList;
        }



    }
}
