﻿using DAL.MedicineAccessDAL;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.MedicineAccessBAL
{
    public class MedicineImageBAL
    {
        public List<MedicineImages> GetImagesForMedicine(Int32 medid)
        {
            MedicineImagesAccess obj = new MedicineImagesAccess();

            return obj.GetImagesForMedicineId(medid);
        }
    }
}
