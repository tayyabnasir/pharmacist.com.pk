﻿using DAL.MedicineAccessDAL;
using Models.MedicineRecord;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.MedicineAccessBAL
{
    //to get recommendations for search help
    public class GetRecommendationsBAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //get seacrh recommendations for salt
        public List<Recommendations> GetRecommendationsForSalt(String salt)
        {
            SearchHelpDAL obj = new SearchHelpDAL();

            DataTable table = obj.GetRecommendationsForSalt(salt);

            if (table == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            return Recommendations.GetRecommendationsForRecord(table.Rows, "SaltName");
        }

        //get seacrh recommendations for brand
        public List<Recommendations> GetRecommendationsForBrand(String brand)
        {
            SearchHelpDAL obj = new SearchHelpDAL();

            DataTable table = obj.GetRecommendationsForBrand(brand);

            if (table == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            return Recommendations.GetRecommendationsForRecord(table.Rows, "BrandName");
        }

        //get seacrh recommendations for Manufacturer
        public List<Recommendations> GetRecommendationsForManufacturer(String manufacturer)
        {
            SearchHelpDAL obj = new SearchHelpDAL();

            DataTable table = obj.GetRecommendationsForManufacturer(manufacturer);

            if (table == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            return Recommendations.GetRecommendationsForRecord(table.Rows, "Manufacturer");
        }
    }
}
