﻿using DAL.MedicineAccessDAL;
using Models;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.MedicineAccessBAL
{
    //handle business logic for getting medicine records from DAL
    //for provided salt and/or brandname
    public class GetMedicineBAL
    {
        //this function is responsible to get a string containing salt(s)
        //and return the medicine records (usder interface only)
        //against that salt(s)
        //may return null so be cautious
        public List<MedicineUserInterface> GetAlternatesForSalt(String salt)
        {
            if (salt == null || salt.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }
            CommonCodeBAL logicObj = new CommonCodeBAL();
            //1. seperate the salts into a list of strings
            List<String> saltsList = logicObj.GetSaltNamesList(salt);

            //2.get datatble with records for matching salt
            GetMedicineDAL dalObj = new GetMedicineDAL();
            DataTable table = dalObj.GetMedicinesForSalt(saltsList);

            //in case no record was found return and save computations
            if (table == null)
            {
                return null;
            }

            //3. for the result set merge records with same MedicineId
            //seperate only user interface information from Medicine
            //records and put them in List
            List<MedicineUserInterface> list = logicObj.GetRecordsInMedicineUserInterface(table);


            //4. return the list
            return list;
        }

        //this function is responsible for getting all alternates for 
        // provided BrandName
        //may return null
        public List<MedicineAmoutPair> GetAlternatesForBrandName(String brandName)
        {
            if (brandName == null || brandName.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }
            //1. Get all matching records with same salt
            GetMedicineDAL dalObj = new GetMedicineDAL();
            Int32 medid = dalObj.GetMedicineIdForBrandName(brandName);
            //in case there was some error
            if (medid == -1)
            {
                return null;
            }

            //getting the salts of current brandname
            List<Salt> salts = dalObj.GetSaltForMedicineId(medid);
            //in case there was some error
            if (salts == null)
            {
                return null;
            }

            //extracting salt names
            List<String> saltName = new List<string>();

            foreach (Salt saltObj in salts)
            {
                saltName.Add(saltObj.SaltName);
            }

            //get records with matching salt names
            DataTable table = dalObj.GetMedicinesForSalt(saltName);

            if (table == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //2. call function to get MedicineAmountPairs for the current 
            //entered brandName
            GetAlternatesForBrandName obj = new GetAlternatesForBrandName();

            return obj.GetMedicineAmountPairs(table, medid);
        }


        //this function is responsible to return a list of simillar brand name
        //Medicines
        //may return null so be cautious
        public  List<MedicineUserInterface> GetSimillarMedicines(String brandName)
        {
            //1. get the Medicine Records
            GetMedicineDAL obj = new GetMedicineDAL();
            DataTable table = obj.GetSimillarBrandNameMedicines(brandName);

            //in case no record was found return and save computations
            if (table == null)
            {
                return null;
            }

            //2. for the result set merge records with same MedicineId
            //seperate only user interface information from Medicine
            //records and put them in List
            List<MedicineUserInterface> list = new CommonCodeBAL().GetRecordsInMedicineUserInterface(table);


            //3. return the list
            return list;

        }


        public MedicineUserInterface GetMedicineRecordForId(Int32 medid)
        {
            
            GetMedicineDAL pObj = new GetMedicineDAL();

            //here we will get all records from our join
            //which we have to seperate to put into Medicine objects
            DataTable table = pObj.GetMedicineForMedId(medid);

            CommonCodeBAL obj = new CommonCodeBAL();

            //now convert the table data to List of data to be sent to client
            List<MedicineUserInterface> MedsList = obj.GetRecordsInMedicineUserInterface(table);


            return MedsList.ElementAt(0);
        }
    }
}
