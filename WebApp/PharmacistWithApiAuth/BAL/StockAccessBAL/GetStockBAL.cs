﻿using BAL.MedicineAccessBAL;
using DAL.StockAccessDAL;
using Models.MedicineRecord;
using Models.Stock;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.StockAccessBAL
{
    //to get stock 
    public class GetStockBAL
    {
        //to parse datatable to MedicineUserInterface objects
        private List<MedicineUserInterface> ParseToMedicineuserInterfaceList(DataTable table)
        {
            return new CommonCodeBAL().GetRecordsInMedicineUserInterface(table); 
        }

        //to recieve datatable and MedicineUserInterface list and merge them 
        //to MedicineStockPair
        private List<MedicineStockPair> MergeMedInterface(List<MedicineUserInterface> list,DataRowCollection rows)
        {
            List<MedicineStockPair> medStockList = new List<MedicineStockPair>();
            
            foreach(DataRow row in rows)
            {
                
                //merge row with matching list item
                MedicineStockPair temp = new MedicineStockPair();
                Int32 medid = (int)row["MedicineId"];
                //check for repeating rows
                if (medStockList.Where(x=>x.Medicine.MedicineId==medid).FirstOrDefault()!=null)
                {
                    continue;
                }

                temp.ItemId = (int)row["ItemId"];
                temp.Availability = (Boolean)row["Available"];
                temp.RetailPrice = (Double)row["RetailPrice"];
                
                temp.Medicine = list.Where(x => x.MedicineId == medid).FirstOrDefault();

                medStockList.Add(temp);
            }

            return medStockList;
        }

        //to retreive and return MedicineStock pair for given pharmacy name
        //may return null
        public List<MedicineStockPair> GetMedicineStockForPharmacyName(String pharmacy)
        {
            if(pharmacy==null || pharmacy.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //1. retrive datatable with matching records
            GetStockDAL obj = new GetStockDAL();

            DataTable table = obj.GetMedicineStockForPharmacyName(pharmacy);

            //2. parse DataTable into the required object
            if (table == null || table.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                //do nothing
            }
            List<MedicineUserInterface> listMU = ParseToMedicineuserInterfaceList(table);

            if(listMU==null || listMU.Count == 0)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //change the states of rows to unchanged
            table.RejectChanges();

            return MergeMedInterface(listMU, table.Rows);
        }

        //may return null so be cautious
        public List<MedicineContainingPharmacies> GetPharmaciesWithMedicine(Int32 medid)
        {
            return new GetStockDAL().GetPharmaciesHavingMedicine(medid);
        }
    }
}
