﻿using DAL.StockAccessDAL;
using Models.Roles;
using Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.StockAccessBAL
{
    public class ModifyStockBAL
    {
        //to check if item belongs to the provided pharmacy
        //must be called before all change operations
        public Boolean CheckForPermission(Int32 itemId,Pharmacy pharm)
        {
            if (pharm == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }

            GetStockDAL obj = new GetStockDAL();

            Int32 pharmacyOfItem = obj.GetOwnerOfItem(itemId);

            if (pharmacyOfItem == -1)
            {
                return false;
            }
            else if(pharmacyOfItem != pharm.PharmacyId)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        //to update Stock
        public Boolean UpdateStock(Stock stock,Pharmacy pharm)
        {
            if (stock == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }
            //1. check if pharmacy is owner of the item
            if (CheckForPermission(stock.ItemId,pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            //2. call prepare object to verify
            if (stock.PrepareStockObject(pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            //3. finally update the product
            return new ModifyStockDAL().UpdateMedicineInStock(stock);
        }

        //to deactivate an item
        public Boolean DeativateItem(Int32 itemId,Pharmacy pharm)
        {
            //1. check if pharmacy is owner of the item
            if (CheckForPermission(itemId, pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return new ModifyStockDAL().RemoveFromStock(itemId);
        }


        //to add to stock Stock
        public Boolean AddStock(Stock stock, Pharmacy pharm)
        {
            //1. check if stock is null
            if (stock == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }
            

            //2. call prepare object to verify
            if (stock.PrepareStockObject(pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            //3. check if MedicineId exist in stock already
            GetStockDAL obj = new GetStockDAL();
            if (obj.CheckForStockEntry(stock.MedicineId, pharm.PharmacyId)!=0)
            {
                //in case medicine is already in stock
                return false;
            }
            else
            {
                //do nohting
            }

            //4. finally add the product
            return new ModifyStockDAL().AddMedicineToStock(stock);
        }
    }
}
