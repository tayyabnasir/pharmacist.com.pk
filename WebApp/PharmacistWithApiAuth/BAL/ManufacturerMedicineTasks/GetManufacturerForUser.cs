﻿using DAL.ManufacturerTasks;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ManufacturerMedicineTasks
{
    public class GetManufacturerForUser
    {
        //get Maufacturer for userid
        //may return null so be cautious
        public Manufacturer GetManufacturerForUserId(Int32 uid)
        {
            ManufacturerAccessDAL obj = new ManufacturerAccessDAL();

            return obj.GetManufacturerForUserId(uid);
        }
    }
}
