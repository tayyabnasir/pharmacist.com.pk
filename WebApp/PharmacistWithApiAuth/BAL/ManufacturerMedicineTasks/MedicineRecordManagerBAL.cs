﻿using DAL.MedicineAccessDAL;
using Models;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ManufacturerMedicineTasks
{
    //provide functionality to help manufacturer to Add, Update and Delete its own records
    public class MedicineRecordManagerBAL
    {
        
        //the responsiblity of this function is to verify if the product 
        //being changed belongs to manufacturer logged in 

        public Boolean CheckPermission(String manufacturer, Int32 medid)
        {
            //first of all get the manufacturer name for the provied medid
            GetMedicineForManufacturerDAL pObj = new GetMedicineForManufacturerDAL();

            String manufacturerName = pObj.GetMedicineManufacturer(medid);

            //next check if the manufacturer is same as that logged in using data from session
            if (manufacturerName == null || manufacturerName.Equals(""))
            {
                return false;
            }

            manufacturerName = manufacturerName.ToLower();

            if (manufacturerName.Equals(manufacturer.ToLower()))
            {
                //that is valid permission for current manufacturer
                return true;
            }
            else
            {
                return false;
            }
        }

        //only user with RemoveMedicine can access this
        //to deactivate a record
        public Boolean DisableMedicineRecord(String manufacturerName, Medicine med)
        {
            if (med==null)
            {
                return false;
            }
            else
            {
                //do nothing
            }

            //first check if the current manifacturer has required permissions to change the medicine
            if (!CheckPermission(manufacturerName, med.MedicineId))
            {
                //in case no permission
                return false;
            }

            //if has permission
            ModifyMedicineDAL pObj = new ModifyMedicineDAL();

            return pObj.DeactiveRecord(med);
        }

        //only user with UpdateMedicine can access this
        //to update a record
        public Boolean UpdateMedicine(String manufacturerName, Medicine med)
        {
            if (med==null)
            {
                return false;
            }
            else
            {
                //do nothing
            }
            //first check if the current manifacturer has required permissions to change the medicine
            if (!CheckPermission(manufacturerName, med.MedicineId))
            {
                //in case no permission
                return false;
            }

            //if has permission

            //set manufacturer for medicine object from session
            med.Manufacturer = manufacturerName;

            //verify medicine object
            PrepareMedicineToModify verifier = new PrepareMedicineToModify();
            if (verifier.VerifyAndFormatMedicineObject(med))
            {
                //do nothing
            }
            else
            {
                //in case medicnie object has irremovable inconsistancies
                return false;
            }

            
            ModifyMedicineDAL pObj = new ModifyMedicineDAL();

            return pObj.UpdateRecord(med);
        }

        //only user with AddMedicine can access this
        //to insert a new record
        public Boolean AddNewMedicine(String manufacturerName, Medicine med)
        {
            if (med == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }
            
            //set manufacturer for medicine object from session
            med.Manufacturer = manufacturerName;

            //verify medicine object
            PrepareMedicineToModify verifier = new PrepareMedicineToModify();
            if (verifier.VerifyAndFormatMedicineObject(med))
            {
                //do nothing
            }
            else
            {
                //in case medicnie object has irremovable inconsistancies
                return false;
            }

            ModifyMedicineDAL pObj = new ModifyMedicineDAL();

            return pObj.InsertRecord(med);
        }


        //null returner
        //to get records for a manufacturer
        public List<Medicine> GetMedicineRecords(String manufacturerName)
        {
            GetMedicineForManufacturerDAL pObj = new GetMedicineForManufacturerDAL();

            //here we will get all records from our join
            //which we have to seperate to put into Medicine objects
            DataTable table = pObj.GetMedicalRecords(manufacturerName);

            //now convert the table data to List of data to be sent to client
            List<Medicine> MedsList = Medicine.GetRecordsInMedicine(table);


            return MedsList;
        }

        //null returner
        //to get record for a manufacturer
        public Medicine GetMedicineRecordForId(String manufacturerName, Int32 medid)
        {
            //check if user has permission to get the medid
            if (!CheckPermission(manufacturerName, medid))
            {
                return null;
            }

            GetMedicineDAL pObj = new GetMedicineDAL();

            //here we will get all records from our join
            //which we have to seperate to put into Medicine objects
            DataTable table = pObj.GetMedicineForMedId(medid);

            //now convert the table data to List of data to be sent to client
            List<Medicine> MedsList = Medicine.GetRecordsInMedicine(table);


            return MedsList.ElementAt(0);
        }

        public Boolean AddMedicineImageBAL(MedicineImages dto,String manufacturerName)
        {
            //check if user has permission to get the medid
            if (!CheckPermission(manufacturerName, dto.medicineId))
            {
                return false;
            }
            else
            {
                //do nothing
            }

            MedicineImagesAccess obj = new MedicineImagesAccess();

            return obj.AddMedicineImage(dto);
        }

        //check if null returned then do not try to delete the file
        public String DeleteMedicineImageBAL(MedicineImages dto, String manufacturerName)
        {
            //check if user has permission to get the medid
            if (!CheckPermission(manufacturerName, dto.medicineId))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            MedicineImagesAccess obj = new MedicineImagesAccess();

            return obj.DeleteMedicineImage(dto);
        }
    }
}
