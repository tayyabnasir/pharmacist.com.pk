﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ManufacturerMedicineTasks
{
    //to provide functions to prepapre medicine objects calculating and verifying 
    //alll the fields to amke them consistent
    public class PrepareMedicineToModify
    {
        public Boolean VerifyAndFormatMedicineObject(Medicine medicine)
        {
            //1. check if pattern of packagin is correct
            if (!medicine.CheckForPackagePattern(medicine.PackSize))
            {
                return false;
            }

            //2. calculate total number of units per pack
            //3. Count salt
            //4. add medid to each salt
            //5. Trim spaces and format the medicine and salt object
            if (!medicine.FormatMedicineObject(medicine))
            {
                return false;
            }
            //6. check for nullity and validity of Medince and Salt object
            if (!medicine.ValidateMedicine(medicine))
            {
                return false;
            }


            return true;
        }
    }
}
