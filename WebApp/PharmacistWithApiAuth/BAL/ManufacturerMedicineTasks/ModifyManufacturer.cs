﻿using DAL.ManufacturerTasks;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ManufacturerMedicineTasks
{
    public class ModifyManufacturer
    {
        public bool UpdateManufacturerBAL(Manufacturer dto)
        {
            if (dto.VerifyManufacturer())
            {
                //do nothing
            }
            else
            {
                return false;
            }
            ManufacturerAccessDAL obj = new ManufacturerAccessDAL();
            return obj.UpdateManufacturer(dto);
        }
    }
}
