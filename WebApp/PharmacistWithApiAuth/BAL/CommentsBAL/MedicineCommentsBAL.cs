﻿using DAL.CommentDAL;
using Models.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.CommentsBAL
{
    public class MedicineCommentsBAL
    {
        public Boolean AddComment(MedicineComment dto)
        {
            if (dto.ValidateComment())
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return new CommentsHandlerDAL().SetComment(dto);
        }

        public List<MedicineComment> GetNextComments(Int32 medid,Int64 offset)
        {
            return new CommentsHandlerDAL().GetCommentsForMedicineId(medid, offset);
        }
    }
}
