﻿using DAL.CommentDAL;
using Models.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.CommentsBAL
{
    public class UserMessageBAL
    {
        public Boolean SaveMessage(UserMessage msg)
        {
            //validate the message
            if (msg.validateMessage())
            {
                //do nohting
            }
            else
            {
                return false;
            }

            //put in database
            UserMessageDAL obj = new UserMessageDAL();

            return obj.InsertUserMessage(msg);
        }
    }
}
