﻿using DAL.PharmacyTasks;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.PharmacyTasksBAL
{
    //to access pharmacy related data
    public class AccessPharmacyBAL
    {
        //for given userid returns the Pharmacy attributes
        //may return null
        public Pharmacy GetPharmacyForUserId(Int32 userId)
        {
            PharmacyAccessDAL obj = new PharmacyAccessDAL();

            return obj.GetPharmacyForUserId(userId);
        }

        public Boolean UpdatePharmacy(Pharmacy obj)
        {
            //verify the Pharmacy object
            if (!obj.VerifyPharmacy())
            {
                return false;
            }
            else
            {
                //do nothing
            }

            PharmacyAccessDAL dalObj = new PharmacyAccessDAL();

            return dalObj.UpdatePharmacy(obj);
        }
    }
}
