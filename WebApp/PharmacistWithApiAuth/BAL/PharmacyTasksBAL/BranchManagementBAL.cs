﻿using DAL.PharmacyTasks;
using Models.BranchManagement;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.PharmacyTasksBAL
{
    //to manage branch related tasks
    public class BranchManagementBAL
    {
        //to check if item belongs to the provided pharmacy
        //must be called before all change operations
        private Boolean CheckForPermission(Int32 branchId, Pharmacy pharm)
        {
            if (pharm == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }

            BranchManagementDAL obj = new BranchManagementDAL();

            Int32 pharmacyOfBranch = obj.GetOwnerOfBranch(branchId);

            if (pharmacyOfBranch == -1)
            {
                return false;
            }
            else if (pharmacyOfBranch != pharm.PharmacyId)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        //to update Stock
        public Boolean UpdateBranch(PharmacyBranch branch, Pharmacy pharm)
        {
            if (branch == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }
            //1. check if pharmacy is owner of the item
            if (CheckForPermission(branch.BranchId, pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            //2. call prepare object to verify
            if (branch.VerifyBranch(pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            //3. finally update the product
            return new BranchManagementDAL().UpdateBranch(branch);
        }

        //to deactivate an item
        public Boolean DeativateBranch(Int32 branchId, Pharmacy pharm)
        {
            //1. check if pharmacy is owner of the item
            if (CheckForPermission(branchId, pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return new BranchManagementDAL().DeleteBranch(branchId);
        }


        //to add to stock Stock
        public Boolean AddBranch(PharmacyBranch branch, Pharmacy pharm)
        {
            //1. check if stock is null
            if (branch == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }


            //2. call prepare object to verify
            if (branch.VerifyBranch(pharm))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            

            //3. finally add the product
            return new BranchManagementDAL().AddBranch(branch);
        }

        //to get branches for Pharmacy name
        //may return null
        public List<PharmacyBranch> GetBranchesForPharmacy(String pharmacy)
        {
            if (pharmacy == null || pharmacy.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            return new BranchManagementDAL().GetBranchesForPharmacyName(pharmacy);
        }
    }
}
