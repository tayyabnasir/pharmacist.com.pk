﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BAL.EmailHandler
{
    //to manage email sending
    public class EmailSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //sends the mail to user
        public Boolean SendMail(String reciver,String body,String sender,String password,String DisplayName,String subject)
        {
            
            try
            {
                MailAddress senderAddress = new MailAddress(sender, DisplayName);


                MailAddress recieverAddress = new MailAddress(reciver);

                SmtpClient smtp = new SmtpClient();

                using (var message = new MailMessage(senderAddress, recieverAddress)
                {
                    Subject = subject,
                    Body = body,


                })
                {
                    message.IsBodyHtml = true;
                    smtp.Send(message);
                }

                return true;
            }
            catch (Exception ex)
            {
                //in case of error
                logger.Warn("Email not sent for :" + reciver + " At :" + DateTime.Now + " Due to " + ex.Message);
                return false;
            }
        }

        
    }
}
