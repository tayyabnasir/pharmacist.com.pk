﻿using DAL.AccountsManagerDAL;
using Models.AccountManagement;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    public class UpdatingUser
    {
        public Boolean UpdateUserInfo(UserSettingsDTO dto)
        {
            if (dto.ValidateAndFormatObject())
            {
                //do nohting
            }
            else
            {
                return false;
            }


            AccountDataAccess obj = new AccountDataAccess();
            return obj.UpdateUserInfo(dto);
        }

        public Boolean UpdatingPassword(LoginDTO dto)
        {
            if (dto.ValidateLogin())
            {
                //do nothing 
            }
            else
            {
                return false;
            }

            AccountDataAccess obj = new AccountDataAccess();

            return obj.UpdateUserPassword(dto);
        }
    }
}
