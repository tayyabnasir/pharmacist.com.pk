﻿using DAL.AccountsManagerDAL;
using DAL.RolesPermissions;
using Models.AccountManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    
    //code reviewed and no error so far
    //to manage email verification tasks
    public class ConfirmEmailForUserBAL
    {
        //logger
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //verifies a given user sign up
        public Boolean VerifiyUser(String email,String token)
        {
            //1. Put the credentials in EmailVerificationDTO
            EmailVerificationDTO dto = new EmailVerificationDTO();
            dto.Email = email;
            dto.Token = token;

            //2. next check if such an entry exists

            EmailVerificationDAL dalObj = new EmailVerificationDAL();

            if (dalObj.CheckIfTokenEmailPairExist(dto))
            {
                //in case of entry existing

                //3. verify user entry in Users
                AccountDataAccess accObj = new AccountDataAccess();

                if (accObj.VerifyUser(dto.Email))
                {
                    //do nothing
                }
                else
                {
                    return false;
                }

                //4. Also put the UserId in RolePermissions table
                Int32 userId = accObj.GetUserIdForEmail(dto.Email);
                if (userId==-1)
                {
                    //put an entry in log for User not verified for confirmed email
                    logger.Fatal("User not verified completely for Email confirmation at: " + DateTime.Now + " Given Email: " + dto.Email);
                    

                    return false;
                }
                else
                {
                    //do nothing
                }
                ModifyRolePermissions permRole = new ModifyRolePermissions();
                if (permRole.DefineUserRole(userId))
                {
                    //do nothing
                }
                else
                {
                    return false;
                }

                //5. remove entry from the EmailVerification
                if (dalObj.RemoveFromEmailVErification(dto.Email))
                {
                    //do nothing
                }
                else
                {
                    //in case of error 
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
