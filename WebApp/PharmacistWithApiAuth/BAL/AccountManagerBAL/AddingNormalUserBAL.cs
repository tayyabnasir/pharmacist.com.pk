﻿using DAL.AccountsManagerDAL;
using Models.AccountManagement;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    //reviewed and no error so far

    //functions to verifiy and ad up a new User
    public class AddingNormalUserBAL
    {
        //generates a token for email to be verified
        private String GetToken()
        {
            return Guid.NewGuid().ToString();
        } 

        //makes an entry for a user to be confirmed via email
        //may return null so be cautiuos
        private EmailVerificationDTO PutUserInToBeVerifiedList(String email)
        {
            //1. Get a token for the User
            String token = GetToken();

            EmailVerificationDTO dto = new EmailVerificationDTO();

            dto.Token = token;
            dto.Email = email;

            //verify object
            if (dto.VerifyEmail())
            {
                //do nothing
            }
            else
            {
                //in case email not correct
                return null;
            }

            //2. make an entery in the Verification Table
            EmailVerificationDAL dalObj = new EmailVerificationDAL();

            if (dalObj.PutUpAnEnteryInVerificationTable(dto))
            {
                return dto;
            }
            else
            {
                //in case of error
                return null;
            }
        }


        //to check if email already has been registered
        public Boolean EmailExists(String email)
        {
            AccountDataAccess dObj = new AccountDataAccess();

            if (dObj.IsExistingEmail(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        

        //to verify and add a new user
        public EmailVerificationDTO AddNewNormalUser(SignUpDTO signUpDTO)
        {
            //1. validate and format the Object
            if (signUpDTO==null || !signUpDTO.ValidateAndFormatObject())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //2. Get User for SignUp
            User user = User.GetUserForSignUpDTO(signUpDTO);

            //3. make entry in Users
            AccountDataAccess dObj = new AccountDataAccess();

            if (!dObj.AddNewUser(user))
            {
                //in case of unsuccessful insertion
                return null;
            }
            else
            {
                //do nothing
            }

            //4. Make an entry in Verification table
            EmailVerificationDTO emailDTO = PutUserInToBeVerifiedList(user.Login);
            if (emailDTO!=null)
            {
                //login complete success

                return emailDTO;
            }
            else
            {
                return null;
            }
        }
    }
}
