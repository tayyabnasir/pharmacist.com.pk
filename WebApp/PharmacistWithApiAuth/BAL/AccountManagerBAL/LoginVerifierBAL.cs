﻿using DAL.AccountsManagerDAL;
using DAL.RolesPermissions;
using Models.AccountManagement;
using Models.Others;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    //code reviewed and no error so far

    //contain functions to verify user login
    public class LoginVerifierBAL
    {
        //Verifiesthe provided login credentials
        //may return null so be cautious
        public SessionManager IsLoginValid(LoginDTO login)
        {
            //1.first check if the credentials were in correct form
            if (login==null || !login.ValidateLogin())
            {
                //in case login dto was invalid
                return null;
            }
            else
            {
                //do nothing
            }
            //2.check if login exist
            AccountDataAccess obj = new AccountDataAccess();

            User userDTO = obj.IsLoginValid(login);

            //3.if login fails
            if (userDTO == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //get permissions for the user

            GetRolePermissionsDAL permsObj = new GetRolePermissionsDAL();

            userDTO.Permissions = permsObj.GetPermissionsOfUser(userDTO.UserId);

            //still if permissions is null set it to empty list
            if (userDTO.Permissions == null)
            {
                userDTO.Permissions = new List<string>();
            }
            else
            {
                //do nothing
            }

            //get roles for the user
            userDTO.Roles = permsObj.GetRolesOfUser(userDTO.UserId);
            //in case of o role for a user return null
            if (userDTO.Roles == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //finally create session object and return

            SessionManager session = new SessionManager(userDTO);

            return session;
        }
    }
}
