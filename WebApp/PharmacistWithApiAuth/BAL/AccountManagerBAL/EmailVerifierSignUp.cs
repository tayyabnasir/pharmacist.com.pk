﻿using BAL.EmailHandler;
using DAL.AccountsManagerDAL;
using Models.AccountManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    //reviewed and no error so far

    //to generate content  for email verification for sign ups
    public class EmailVerifierSignUp
    {
        //generates a link which will be sent to user for verification of email
        private String GetLinkToSend(String email, String token)
        {
            String link = "http://pharmacist.com.pk/Accounts/EmailVerify?email=" + email + "&token=" + token;

            return link;
        }

        private String GenerateBodyForEmailVerify(String link)
        {
            String body= " <html> <head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> <title>Single-Column Responsive Email Template</title> <style> @media only screen and (min-device-width: 541px) { .content { width: 540px !important; } } .center { display: block; margin: 0 auto; } </style> </head> <body> <!--[if (gte mso 9)|(IE)]> <table width='540' align='center' cellpadding='0' cellspacing='0' border='0'> <tr> <td> <![endif]--> <table class='content' align='center' cellpadding='0' cellspacing='0' border='0' style='width: 100%; max-width: 540px;'> <tr> <td> <img src='http://i68.tinypic.com/2929u1c.jpg' width='40%' class='center'/> </td> </tr> <tr> <td><hr/></td> </tr> <tr> <td> <h3>Hi, <span class='name'></span></h3> <br/> </td> </tr> <tr> <td style='font-family:Helvetica;font-size:large'> <p>We welcome you to <a href='http://pharmacist.com.pk'>pharmacist.com.pk</a></p> <p>Please Click on the following link to verify your Email:</p> <p><a href='" + link + "'>" + link + "</a></p> <p><br/></p> <p>If the does not open copy and paste it into your browser's address bar</p> </td> </tr> <tr> <td><br/><br/></td> </tr> <tr> <td style='font-family:Helvetica;font-size:large;text-align:right'> <p>Regards,</p> <p>Team pharmacist.com.pk</p> </td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </body> </html>";

            return body;
        }

        //to send the verification Email
        public Boolean SendEmailToVerifySignUp(EmailVerificationDTO dto)
        {
            //1. get link to send
            String link = GetLinkToSend(dto.Email, dto.Token);
            //2. get body
            String body = GenerateBodyForEmailVerify(link);
            //3. subject
            String subject = "Email Verification for pharmacist.com.pk Account";

            //4. Send the email
            EmailSender emailObj = new EmailSender();

            Boolean ret=emailObj.SendMail(dto.Email, body, "support@pharmacist.com.pk", "mnosecure", "Pharmacist.com.pk", subject);

            //for now no need to verify if email was sent
            return ret;
        }

        //manages request for email resend
        public Boolean ResendRequestManager(String email)
        {
            //1. check if email exist in Verification table
            EmailVerificationDAL emVer = new EmailVerificationDAL();
            EmailVerificationDTO dto = emVer.GetEmailToken(email);

            if (dto == null)
            {
                //in case email does not exist
                return false;
            }
            else
            {
                return SendEmailToVerifySignUp(dto);
            }
        }
    }
}
