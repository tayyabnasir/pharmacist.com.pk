﻿using BAL.EmailHandler;
using DAL.AccountsManagerDAL;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.AccountManagerBAL
{
    //manages taks related to recovery to password
    public class PasswordRecoveryBAL
    {
        //to prepare email to be sent for email recovery
        private String GenerateBodyForEmailVerify(User info)
        {
            String body = " <html> <head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> <title>Single-Column Responsive Email Template</title> <style> @media only screen and (min-device-width: 541px) { .content { width: 540px !important; } } .center { display: block; margin: 0 auto; } </style> </head> <body> <!--[if (gte mso 9)|(IE)]> <table width='540' align='center' cellpadding='0' cellspacing='0' border='0'> <tr> <td> <![endif]--> <table class='content' align='center' cellpadding='0' cellspacing='0' border='0' style='width: 100%; max-width: 540px;'> <tr> <td> <img src='http://i68.tinypic.com/2929u1c.jpg' width='40%' class='center'/> </td> </tr> <tr> <td><hr/></td> </tr> <tr> <td> <h3>Hi, "+info.FirstName+" "+info.LastName+" <span class='name'></span></h3> <br/> </td> </tr> <tr> <td style='font-family:Helvetica;font-size:large'> <p>Here is your password for your account "+info.Login+" which was requested by you to be recovered: </p> <p>"+info.Password+"</p> <p><br/></p> <p>If you did not request for a password recovery than simply ignore this message.</p> </td> </tr> <tr> <td><br/><br/></td> </tr> <tr> <td style='font-family:Helvetica;font-size:large;text-align:right'> <p>Regards,</p> <p>Team pharmacist.com.pk</p> </td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </body> </html>";

            return body;
        }

        private Boolean PrepareAndSendEmail(User info)
        {
            String reciever = info.Email;
            String subject = "Password recovery for Account: " + info.Login;
            String body = GenerateBodyForEmailVerify(info);


            EmailSender sender = new EmailSender();

            if(sender.SendMail(reciever,body, "support@pharmacist.com.pk", "mnosecure", "Pharmacist.com.pk", subject))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean SendPasswordToEmail(String email)
        {
            //1. check if user is existing
            AccountDataAccess dataAccess = new AccountDataAccess();

            if (dataAccess.IsExistingEmail(email))
            {
                //do nothing
            }
            else
            {
                //no email exists
                return false;
            }

            //2.get the contact email in case if Role is Manufacturer or Pharmacy
            User u = dataAccess.GetUserForLogin(email);

            if (u == null)
            {
                return false;
            }
            else
            {
                //do nothing
            }

            String contactEmail = u.Email;


            //3. if email is not null then send mail with password
            //also if user is not verified
            if(contactEmail==null || contactEmail.Equals("") || !u.IsVerified)
            {
                return false;
            }
            else
            {

                //do nothing
            }

            //prepare and send email for password recovery
            if (PrepareAndSendEmail(u))
            {
                return true;
            }
            else
            {
                //in case email not sent
                return false;
            }

        }
    }
}
