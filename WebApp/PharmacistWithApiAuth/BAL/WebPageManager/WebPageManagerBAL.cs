﻿using DAL.WePageManager;
using Models.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.WebPageManager
{
    //provide functions to edit and access web page for given user
    public class WebPageManagerBAL
    {
        //may return null so be cautious
        public WebPage GetWebPageForUser(Int32 userID)
        {
            WebPageHandlerDAL obj = new WebPageHandlerDAL();
            return obj.GetPageForUserId(userID);
        }

        //to update html for webpage
        public Boolean UpdateWebPage(String html,Int32 userId)
        {
            //first check for lenght of html must not be more than 1490
            
            if (html.Length > 1499 || html.Length < 0)
            {
                return false;
            }
            else
            {
                //do nothing
            }

            WebPageHandlerDAL obj = new WebPageHandlerDAL();

            return obj.UpdateHTMLForUser(html, userId);
        }

        //to get page of a certain manufacturer
        public String GetPageForName(String name,String Role)
        {
            return new WebPageHandlerDAL().GetPageForManufacturer(name,Role);
            
        }
    }
}
