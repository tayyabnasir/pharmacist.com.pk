﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Pharmacist.HelperClasses
{
    public class ImageStoreHelper
    {
        //get extension
        public static String GetFileExtension(HttpPostedFileBase upload)
        {
            string extension = Path.GetExtension(upload.FileName);
            return extension;
        }

        public Boolean StoreFile(HttpPostedFileBase file,String guid,String serverPath)
        {
            
            String name = guid;

            String path= Path.Combine(serverPath, name);

            try
            {
                file.SaveAs(path);
            }
            catch(Exception ex)
            {
                return false;
            }

            return true;
        }

        public Boolean DeleteFile(String url, String serverPath)
        {
            
            String path = Path.Combine(serverPath, url);

            try
            {
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}