﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pharmacist.HelperClasses
{
    public class ImageVerifier
    {
        //check if file actually is image
        public bool IsImage(HttpPostedFileBase file)
        {
            try
            {
                using (var bitmap = new System.Drawing.Bitmap(file.InputStream))
                {
                    return !bitmap.Size.IsEmpty;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        //check if less than 1 mb
        public Boolean IsLessThanOneMb(HttpPostedFileBase file)
        {
            int fileSize = file.ContentLength;
            if (fileSize > (1024 * 1024))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}