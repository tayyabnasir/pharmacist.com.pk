﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using BAL.AccountManagerBAL;
using Models.Others;
using Models.AccountManagement;
using System.Security.Claims;

namespace Pharmacist.AuthServer
{
    //the server to validate user and give back the token
    public class AuthServerProvider:OAuthAuthorizationServerProvider
    {
        //as no need for validating the client
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        //the authentication process
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            LoginDTO userLogin = new LoginDTO();
            userLogin.Login = context.UserName;
            userLogin.Password = context.Password;

            //Verify the credentials
            LoginVerifierBAL obj = new LoginVerifierBAL();
            //validate login
            SessionManager session = obj.IsLoginValid(userLogin);

            //in case no user exist
            if (session == null)
            {
                context.SetError("invalid_grant", "No Such User Exists");
            }
            //check if email is verified or not
            else if (session.user.IsVerified == true)
            {
                identity.AddClaim(new Claim("user_name", session.user.FirstName));
                identity.AddClaim(new Claim("email", session.user.Email));

                //validate context and generate token
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Email not Verified");
            }
        }
    }
}