﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Pharmacist.AuthServer;
using System;
using System.Web.Http;

[assembly: OwinStartupAttribute(typeof(PharmacistWithApiAuth.Startup))]
namespace PharmacistWithApiAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            //enabling cors
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }

        //to configure the server provider
        private void ConfigureAuth(IAppBuilder app)
        {
            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new AuthServerProvider(),
                AccessTokenExpireTimeSpan=TimeSpan.FromMinutes(55),
                AllowInsecureHttp=true
            };

            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
