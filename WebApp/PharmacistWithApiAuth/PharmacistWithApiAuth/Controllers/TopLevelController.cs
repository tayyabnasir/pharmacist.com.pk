﻿using BAL.ManufacturerMedicineTasks;
using BAL.PharmacyTasksBAL;
using Models.Others;
using Models.Roles;
using Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers
{
    // all web controllers must be inherited from this controller
    public class TopLevelController : Controller
    {
        //will contain some code common to all controllers like session mangement
        protected Boolean IsSessionSet()
        {
            SessionManager session = (SessionManager)Session["user"];
            if (session != null)
            {
                //in case of session is set put the current session object
                //in View data so that it can be accessed in views to 
                //render them according to details of User
                ViewData["userDTO"] = session;

                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean CheckForPermission(User dto,String Permission)
        {
            return new CheckPermission().HasPermission(dto, Permission);
        }

        protected Boolean CheckForRole(User dto,String Role)
        {
            return new CheckRole().HasRole(dto, Role);
        }

        //sets manufacturer in session if not set already
        protected Boolean SetManufacturerInSession(User user)
        {
            if (Session["manufacturer"] != null)
            {
                return true;
            }
            else
            {
                //1. check if user is manufacturer
                if (CheckForRole(user, "Manufacturer"))
                {
                    // do nothing
                }
                else
                {
                    return false;
                }
                GetManufacturerForUser obj = new GetManufacturerForUser();
                Manufacturer manObj = obj.GetManufacturerForUserId(user.UserId);

                if (manObj == null)
                {
                    return false;
                }
                else
                {
                    Session["manufacturer"] = manObj;
                    return true;
                }
            }
        }

        //must be called before accessing a manufacturer page
        protected Boolean ChecksForManufacturerPages()
        {
            //1. check if session is set
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return false;
            }

            SessionManager session = (SessionManager)Session["user"];

            //2. check is role is manufacturer
            if (CheckForRole(session.user, "Manufacturer"))
            {
                //do nothing
            }
            else
            {
                //in case not a manufacturer
                return false;
            }
            //3. Get Manufacturer name for userid
            if (SetManufacturerInSession(session.user))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return true;
        }



        //sets pharmacy in session if not already set
        protected Boolean SetPharmacyInSession(User user)
        {
            if (Session["pharmacy"] != null)
            {
                return true;
            }
            else
            {
                //1. check if user is manufacturer
                if (CheckForRole(user, "Pharmacy"))
                {
                    // do nothing
                }
                else
                {
                    return false;
                }
                AccessPharmacyBAL obj = new AccessPharmacyBAL();
                Pharmacy pharmObj = obj.GetPharmacyForUserId(user.UserId);

                if (pharmObj == null)
                {
                    return false;
                }
                else
                {
                    Session["pharmacy"] = pharmObj;
                    return true;
                }
            }
        }

        //must be called before accessing a pharmacy page
        protected Boolean ChecksForPharmacyPages()
        {
            //1. check if session is set
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return false;
            }

            SessionManager session = (SessionManager)Session["user"];

            //2. check is role is manufacturer
            if (CheckForRole(session.user, "Pharmacy"))
            {
                //do nothing
            }
            else
            {
                //in case not a manufacturer
                return false;
            }
            //3. Get Pharmacy name for userid
            if (SetPharmacyInSession(session.user))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}