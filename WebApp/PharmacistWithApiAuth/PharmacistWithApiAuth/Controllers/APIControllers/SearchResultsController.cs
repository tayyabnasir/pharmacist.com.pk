﻿using BAL.ManufacturerMedicineTasks;
using BAL.MedicineAccessBAL;
using Models;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PharmacistWithApiAuth.Controllers.APIControllers
{
    public class SearchResultsController : ApiController
    {
        //for a certain salt return all brandnames along with their different amounts and details in json format
        [Authorize]
        [HttpGet]
        public List<MedicineUserInterface> GetAlternatesForSalt(String Salt)
        {
            GetMedicineBAL obj = new GetMedicineBAL();

            //calling null returner
            List<MedicineUserInterface> list = obj.GetAlternatesForSalt(Salt);

            //check on client if null is returned than display error message
            return list;

        }


        [Authorize]
        [HttpGet]
        public List<MedicineAmoutPair> GetAlternatesForBrand(String BrandName)
        {
            
            GetMedicineBAL obj = new GetMedicineBAL();

            //calling null returner
            List<MedicineAmoutPair> list = obj.GetAlternatesForBrandName(BrandName);

            //check on client if null is returned then display error
            return list;
        }

        //recommendations for brand search
        [Authorize]
        [HttpGet]
        public List<Recommendations> GetRecommendationsForBrand(String prefixBrand)
        {
            
            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return obj.GetRecommendationsForBrand(prefixBrand);
        }

        //recommendation for salt search
        [Authorize]
        [HttpGet]
        public List<Recommendations> GetRecommendationsForSalt(String prefixSalt)
        {
            

            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return obj.GetRecommendationsForSalt(prefixSalt);
        }

        //recommendation for Manufacuturer
        [Authorize]
        [HttpGet]
        public List<Recommendations> GetRecommendationsForManufacturer(String prefixManufacturer)
        {
            
            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return obj.GetRecommendationsForManufacturer(prefixManufacturer);
        }

        
        //to get medicines for given maufacturer but in MedicineUserInterface Form
        //to get medicines of the given manufacturer
        [HttpGet]
        [Authorize]
        public List<MedicineUserInterface> GetMedicinesUserInterfaceForManufacturer(String manufacturer)
        {
            
            //check if manufacturer is not null
            if (manufacturer == null || manufacturer.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            MedicineRecordManagerBAL obj = new MedicineRecordManagerBAL();

            List<Medicine> med = obj.GetMedicineRecords(manufacturer);

            //convert the Medicine list to MedicineUserInterface

            return MedicineUserInterface.GetMedUserInterfaceForMedicines(med);
        }
    }
}
