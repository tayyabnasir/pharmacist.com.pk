﻿using BAL.AccountManagerBAL;
using Models.AccountManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace PharmacistWithApiAuth.Controllers.APIControllers
{
    public class AccountsAPIController : ApiController
    {
        //to recover password
        [HttpGet]
        public IHttpActionResult RecoverPassword(String email)
        {
            if (email == null || email.Equals(""))
            {
                return null;
            }
            else
            {
                //dp nothing
            }

            PasswordRecoveryBAL obj = new PasswordRecoveryBAL();


            if (obj.SendPasswordToEmail(email))
            {
                return Ok("Email Sent");
            }
            else
            {
                //in case email could not be sent
                return null;
            }

            
        }

        [HttpPost]
        public IHttpActionResult SignUp(SignUpDTO dto)
        {
            //1. Try to add new User
            AddingNormalUserBAL balObj = new AddingNormalUserBAL();
            EmailVerificationDTO emailObj = balObj.AddNewNormalUser(dto);
            if (emailObj != null)
            {
                //in case of success send an email to user and return success
                //currently email goes sychronously still no need to check if email was sent successfully
                EmailVerifierSignUp obj = new EmailVerifierSignUp();

                //no need to check if email was sent successfully as can be requested
                //to be sent again
                obj.SendEmailToVerifySignUp(emailObj);

                return Ok("Sign Up Added. Verify your Email");

            }
            else
            {
                // in case some error was encountered
                return null;
            }

        }



        //to get details of the user logged in
        //also serve as the login verifier
        [HttpGet]
        [Authorize]
        public UserEmailName GetLoggedInUserDTO()
        {
            var identity = User.Identity as ClaimsIdentity;

            UserEmailName dto = new UserEmailName();
            dto.Name = identity.Claims.Where(c => c.Type == "user_name").Single().Value;
            dto.Email = identity.Claims.Where(c => c.Type == "email").Single().Value;

            return dto;
        }

        //to check if user is logged in or not
        [HttpGet]
        [Authorize]
        public IHttpActionResult IsLoggedIn()
        {
            var identity = User.Identity as ClaimsIdentity;

            return Ok("You Are Logged in " + identity.Claims.Where(c => c.Type == "user_name").Single().Value);
        }
    }
}
