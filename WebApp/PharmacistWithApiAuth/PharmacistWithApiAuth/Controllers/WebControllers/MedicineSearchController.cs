﻿using Models.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers
{
    public class MedicineSearchController : TopLevelController
    {
        

        // returns the main form for medicine search form
        [HttpGet]
        public ActionResult Index()
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                // if so redirect to login page
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            return View();
        }

        //salt search individual form
        public ActionResult SaltSearch()
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                // if so redirect to login page
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            return View();
        }

        //brandSearch individual form
        public ActionResult BrandSearch()
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                // if so redirect to login page
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            return View();
        }

        // Salt SearchResults
        [HttpGet]
        public ActionResult SaltSearchResult(String salt)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                // if so redirect to login page
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            ViewBag.Salt = salt;
            return View();
        }

        //brand results
        [HttpGet]
        public ActionResult BrandSearchResult(String brandName)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                // if so redirect to home page
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            ViewBag.BrandName = brandName;
            return View();
        }
    }
}