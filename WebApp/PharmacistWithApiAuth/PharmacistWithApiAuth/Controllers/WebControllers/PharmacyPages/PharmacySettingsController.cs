﻿using BAL.PharmacyTasksBAL;
using Models.Others;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages
{
    public class PharmacySettingsController : TopLevelController
    {
        // GET: PharmacySettings
        public ActionResult Settings()
        {
            //check for session and permissions
            if (ChecksForPharmacyPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            Pharmacy obj = (Pharmacy)Session["pharmacy"];

            return View(obj);
        }

        [HttpPost]
        public JsonResult UpdateAccount(Pharmacy dto)
        {
            //1. session check
            if (ChecksForPharmacyPages())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "UpdatePharmacy"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            dto.PharmacyId = pharm.PharmacyId;

            AccessPharmacyBAL balObj = new AccessPharmacyBAL();

            if (balObj.UpdatePharmacy(dto))
            {
                //update session
                pharm.PharmacyName = dto.PharmacyName;
                pharm.Website = dto.Website;
                Session["pharmacy"] = pharm;

                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }
    }
}