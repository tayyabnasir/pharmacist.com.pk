﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages
{
    public class PharmacyPanelController : TopLevelController
    {
        // get the main panel for pharmacy displaying all the products added to 
        // stock by pharmacy
        public ActionResult Index()
        {
            //check for session and permissions
            if (ChecksForPharmacyPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            //to get Pharmacy information in view
            Pharmacy obj = (Pharmacy)Session["pharmacy"];
            ViewData["Pharmacy"] = obj;

            return View();
        }

        //panel to display products that can be added to stock
        public ActionResult AddProductToStock()
        {
            //check for session and permissions
            if (ChecksForPharmacyPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            //to get Pharmacy information in view
            Pharmacy obj = (Pharmacy)Session["pharmacy"];
            ViewData["Pharmacy"] = obj;

            return View();
        }

    }
}