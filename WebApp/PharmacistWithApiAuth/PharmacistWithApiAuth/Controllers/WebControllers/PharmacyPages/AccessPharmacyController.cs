﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages
{
    public class AccessPharmacyController : TopLevelController
    {
        // GET: AccessPharmacy
        public ActionResult GetPharmacyStock(String pharmacy)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }
            ViewData["pharmacy"] = pharmacy;

            return View();
        }

        public ActionResult GetPharamcyBranches(String pharmacy)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }
            ViewData["pharmacy"] = pharmacy;

            return View();
        }
    }
}