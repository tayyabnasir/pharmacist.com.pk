﻿using BAL.StockAccessBAL;
using Models.Others;
using Models.Roles;
using Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages
{
    public class StockManageController : TopLevelController
    {
        // to remove an item from stock
        [HttpPost]
        public JsonResult RemoveFromStock(Int32 itemId)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "RemoveFromStock"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }
            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            //4. try to remove
            ModifyStockBAL obj = new ModifyStockBAL();

            if (obj.DeativateItem(itemId, pharm))
            {
                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                //do nothing
            }

            return null;
        }

        //to update an item
        [HttpPost]
        public JsonResult UpdateStock(Stock stock)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "UpdateStock"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to remove
            ModifyStockBAL obj = new ModifyStockBAL();

            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            if (obj.UpdateStock(stock, pharm))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }


        //to add an item
        [HttpPost]
        public JsonResult AddStock(Stock stock)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "AddToStock"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }
            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            //4. try to remove
            ModifyStockBAL obj = new ModifyStockBAL();

            if (obj.AddStock(stock, pharm))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }
    }
}