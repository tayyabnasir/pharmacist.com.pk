﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages.BranchManagement
{
    public class BranchPanelController : TopLevelController
    {
        // GET: BranchPanel
        public ActionResult Index()
        {
            //check for session and permissions
            if (ChecksForPharmacyPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            //to get Pharmacy information in view
            Pharmacy obj = (Pharmacy)Session["pharmacy"];
            ViewData["Pharmacy"] = obj;

            return View();
        }
    }
}