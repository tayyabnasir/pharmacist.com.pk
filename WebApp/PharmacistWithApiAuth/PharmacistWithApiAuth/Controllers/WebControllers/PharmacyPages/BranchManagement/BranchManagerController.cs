﻿using BAL.PharmacyTasksBAL;
using Models.BranchManagement;
using Models.Others;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.PharmacyPages.BranchManagement
{
    public class BranchManagerController : TopLevelController
    {
        //get all branches for proivded Pharmacy name
        [HttpPost]
        public JsonResult GetBranchesOfPharmacy()
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }
            String pharmacy = Request["pharmacy"];
            if (pharmacy == null || pharmacy.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            BranchManagementBAL obj = new BranchManagementBAL();

            return Json(obj.GetBranchesForPharmacy(pharmacy));
        }


        //Add branch
        [HttpPost]
        public JsonResult AddBranch(PharmacyBranch branch)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "AddBranch"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to remove
            BranchManagementBAL obj = new BranchManagementBAL();

            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            if (obj.AddBranch(branch, pharm))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }


        // to remove an item from stock
        [HttpPost]
        public JsonResult DeleteBranch(Int32 branchId)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "RemoveBranch"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }
            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            //4. try to remove
            BranchManagementBAL obj = new BranchManagementBAL();

            if (obj.DeativateBranch(branchId, pharm))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }


        }

        //to update an item
        [HttpPost]
        public JsonResult UpdateBranch(PharmacyBranch branch)
        {
            //1. session check
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "ChangeBranchDetails"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to remove
            BranchManagementBAL obj = new BranchManagementBAL();

            //get the pharmacy object from session
            Pharmacy pharm = (Pharmacy)Session["pharmacy"];
            if (pharm == null)
            {
                //in case pharmacy was not set
                return null;
            }
            else
            {
                //do nothing
            }

            if (obj.UpdateBranch(branch, pharm))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }
    }
}