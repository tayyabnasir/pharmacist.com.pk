﻿using BAL.StockAccessBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers
{
    public class StockReadController : TopLevelController
    {
        //get stock for pharmacy name
        [HttpPost]
        public JsonResult GetStock()
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }
            String pharmacy = Request["pharmacy"];
            if (pharmacy == null || pharmacy.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetStockBAL obj = new GetStockBAL();

            return Json(obj.GetMedicineStockForPharmacyName(pharmacy));
        }

    }
}