﻿using BAL.AccountManagerBAL;
using Models.AccountManagement;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//code reviwed and no error detected so far
namespace Pharmacist.Controllers.WebControllers
{
    public class AccountsController : TopLevelController
    {
        //Login Controllers
        [HttpGet]
        public ActionResult Login()
        {
            //check if session is already set
            if (IsSessionSet())
            {
                // if so redirect to home page
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }

            //return the login page
            return View();
        }

        //to verify via ajax call if login exists
        [HttpPost]
        public JsonResult VerifyLogin(LoginDTO userLogin)
        {
            if (IsSessionSet())
            {//in case session is already set clear it
                Session.Remove("user");
            }
            else
            {
                //do nothing
            }

            LoginVerifierBAL obj = new LoginVerifierBAL();
            //validate login
            SessionManager session = obj.IsLoginValid(userLogin);

            //in case no user exist
            if (session == null)
            {
                return null;
            }
            //check if email is verified or not
            else if (session.user.IsVerified == true)
            {
                //store the session manger in Session
                Session["user"] = session;
                Object objRet = new
                {
                    isVerified = true,

                };


                //session is set with the user along with all permissions
                return Json(objRet);

            }
            else
            {
                //in case of not verified user redirect to email verify page
                //from client
                Object objRet = new
                {
                    isVerified = false,

                };

                return Json(objRet);
            }

        }

        //sign up controllers
        [HttpGet]
        public ActionResult SignUp()
        {
            //check if session is already set
            if (IsSessionSet())
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }
            //return the sign up form page
            return View();
        }

        //to add a user account
        [HttpPost]
        public JsonResult AddUserAccount(SignUpDTO userSignUp)
        {
            if (IsSessionSet())
            {//in case session is already set clear it
                Session.Remove("user");
            }
            else
            {
                //do nothing
            }

            //1. Try to add new User
            AddingNormalUserBAL balObj = new AddingNormalUserBAL();
            EmailVerificationDTO emailObj = balObj.AddNewNormalUser(userSignUp);
            if (emailObj!=null)
            {
                //in case of success send an email to user and return success
                //currently email goes sychronously still no need to check if email was sent successfully
                EmailVerifierSignUp obj = new EmailVerifierSignUp();
                
                //no need to check if email was sent successfully as can be requested
                //to be sent again
                obj.SendEmailToVerifySignUp(emailObj);

                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
                
            }
            else
            {
                // in case some error was encountered
                return null;
            }

            
        }


        //to check if the added email already exists
        //return true if email not exists
        [HttpPost]
        public JsonResult CheckForEmailExistance(String email)
        {
            if (IsSessionSet())
            {//in case session is already set clear it
                Session.Remove("user");
            }
            else
            {
                //do nothing
            }
            //check if email entered was not null or empty
            if (email == null || email.Equals(""))
            {
                return null;
            }
            else
            {
                AddingNormalUserBAL bObj = new AddingNormalUserBAL();
                //check if email is already been added
                if (!bObj.EmailExists(email))
                {
                    //case where email not exists email is unique
                    Object objRet = new
                    {
                        success = true,

                    };

                    return Json(objRet);
                }
                else
                {
                    // in case email exist
                    return null;
                }
            }

        }


        //email verification controllers
        [HttpGet]
        public ActionResult EmailVerify(String email,String token)
        {
            if (IsSessionSet())
            {//in case session is already set redirect
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }
            ConfirmEmailForUserBAL obj = new ConfirmEmailForUserBAL();

            //try to verify email.
            if (obj.VerifiyUser(email, token))
            {
                //display success message and Show Login Screen
                return View();
            }
            else
            {
                //in case email could not be verified open up Sign up Page
                return View("SignUp");
            }
            
        }

        //resend verification email
        [HttpPost]
        public JsonResult ResendVerificationEmail(String email)
        {
            
            if (IsSessionSet())
            {//in case session is already set clear it
                Session.Remove("user");
            }
            else
            {
                //do nothing
            }
            //checkif provided email is valid
            if (email == null || email.Equals(""))
            {
                return null;
            }
            else
            {
                EmailVerifierSignUp obj = new EmailVerifierSignUp();

                //try to resend email 
                if (obj.ResendRequestManager(email))
                {
                    Object objRet = new
                    {
                        success = true,

                    };

                    return Json(objRet);
                }
                else
                {
                    // in case email could not be sent
                    return null;
                }
            }
        }

        //password recovery via password
        [HttpGet]
        public ActionResult RecoverPassword(String email)
        {
            if (IsSessionSet())
            {//in case session is already set redirect
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }

            if (email == null || email.Equals(""))
            {
                ViewData["Success"] = false;
                ViewData["Login"] = "Please Provide Valid Email";

                return View();
            }
            else
            {
                //dp nothing
            }
            
            PasswordRecoveryBAL obj = new PasswordRecoveryBAL();

            ViewData["Login"] = email;

            

            if (obj.SendPasswordToEmail(email))
            {
                ViewData["Success"] = true;
            }
            else
            {
                //in case email could not be sent
                ViewData["Success"] = false;
            }

            return View();
        }


        //logout the current User
        [HttpGet]
        public ActionResult Logout()
        {
            if (IsSessionSet())
            {//in case session is set remove it
                Session.RemoveAll(); //Removes all session variables
            }
            else
            {
                //do nothing
            }

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Settings()
        {
            if (!IsSessionSet())
            {
                return RedirectToAction("Login", "Accounts");
            }
            else
            {
                //do nothing
            }

            SessionManager session = (SessionManager)Session["user"];

            return View(session.user);
        }

        [HttpPost]
        public JsonResult UpdateAccount(UserSettingsDTO dto)
        {
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nohting
            }
            SessionManager session = (SessionManager)Session["user"];

            dto.Email = session.user.Login;

            UpdatingUser obj = new UpdatingUser();

            if (obj.UpdateUserInfo(dto))
            {
                //update session
                session.user.FirstName = dto.FirstName;
                session.user.LastName = dto.LastName;
                Session["user"] = session;

                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
            
        }

        [HttpPost]
        public JsonResult UpdatePassword(String oldPassword,String newPassword)
        {
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nohting
            }
            SessionManager session = (SessionManager)Session["user"];

            if (session.user.Password.Equals(oldPassword))
            {
                //do nothing
            }
            else
            {
                return null;
            }

            LoginDTO dto = new LoginDTO();
            dto.Login = session.user.Login;
            dto.Password = newPassword;

            UpdatingUser obj = new UpdatingUser();

            if (obj.UpdatingPassword(dto))
            {
                session.user.Password = dto.Password;
                Session["user"] = session;

                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }

            
        }
    }
}