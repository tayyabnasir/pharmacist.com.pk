﻿using BAL.WebPageManager;
using Models.Others;
using Pharmacist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers
{
    public class WebPageController : TopLevelController
    {
        // get the web page Editor
        public ActionResult GetEditor()
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            SessionManager session = (SessionManager)Session["user"];
            //check if user has permission to access the page
            if(CheckForPermission(session.user, "EditPage"))
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            //now get the web page of the user and show it 
            WebPageManagerBAL obj = new WebPageManagerBAL();

            WebPage page = obj.GetWebPageForUser(session.user.UserId);

            if (page == null)
            {
                return RedirectToAction("Index","Home");
            }
            else
            {
                //do nothing
            }
            // Decode the content for showing on Web page.
            page.PageHtml = WebUtility.HtmlDecode(page.PageHtml);
            return View(page);
        }

        //to update web page for manufacturer
        [HttpPost]
        public JsonResult UpdatePage(PageHtml htmlPage)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            SessionManager session = (SessionManager)Session["user"];
            //check if user has permission to access the page
            if (CheckForPermission(session.user, "EditPage"))
            {
                //do nothing
            }
            else
            {
                return null;
            }

            if (htmlPage == null || htmlPage.html==null)
            {
                return null;
            }
            else
            {
                //do nothing
            }
            //to encode to text
            htmlPage.html = WebUtility.HtmlEncode(htmlPage.html);
            WebPageManagerBAL obj = new WebPageManagerBAL();

            if (obj.UpdateWebPage(htmlPage.html, session.user.UserId))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }

        //to get page for a manufacturer
        [HttpGet]
        public ActionResult GetManufacturerPage(String manufacturer)
        {
            if(manufacturer==null || manufacturer.Equals(""))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }
            WebPageManagerBAL obj = new WebPageManagerBAL();

            String html = obj.GetPageForName(manufacturer,"Manufacturer");

            // Decode the content for showing on Web page.
            html = WebUtility.HtmlDecode(html);

            ViewData["html"] = html;
            return View();
        }

        //to get page for a pharmacy
        [HttpGet]
        public ActionResult GetPharmacyPage(String pharmacy)
        {
            if (pharmacy == null || pharmacy.Equals(""))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //do nothing
            }
            WebPageManagerBAL obj = new WebPageManagerBAL();

            String html = obj.GetPageForName(pharmacy,"Pharmacy");

            // Decode the content for showing on Web page.
            html = WebUtility.HtmlDecode(html);

            ViewData["html"] = html;
            return View();
        }
    }
}