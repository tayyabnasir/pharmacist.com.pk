﻿using BAL.MedicineAccessBAL;
using BAL.StockAccessBAL;
using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers
{
    public class MedicineDetailsController : TopLevelController
    {
        // GET: MedicineDetails
        public ActionResult Details(Int32 medid)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            GetMedicineBAL obj = new GetMedicineBAL();
            MedicineImagePair medObj = new MedicineImagePair();
            medObj.medicine = obj.GetMedicineRecordForId(medid);

            MedicineImageBAL imgObj = new MedicineImageBAL();

            medObj.Images = imgObj.GetImagesForMedicine(medid);


            return View(medObj);
        }

        [HttpPost]
        public JsonResult GetPharmaciesWithMedicine(Int32 medid)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            GetStockBAL obj = new GetStockBAL();

            return Json(obj.GetPharmaciesWithMedicine(medid));
        }

        [HttpPost]
        public JsonResult GetImagesForMedicine(Int32 medid)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            MedicineImageBAL obj = new MedicineImageBAL();

            return Json(obj.GetImagesForMedicine(medid));
            
        }
    }
}