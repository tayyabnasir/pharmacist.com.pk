﻿using BAL.ManufacturerMedicineTasks;
using Models.Others;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.ManufacturerPages
{
    public class ManufacturerSettingsController : TopLevelController
    {
        // GET: ManufacturerSettings
        public ActionResult Settings()
        {
            //check for session and permissions
            if (ChecksForManufacturerPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            Manufacturer obj = (Manufacturer)Session["manufacturer"];

            return View(obj);
        }

        [HttpPost]
        public JsonResult UpdateAccount(Manufacturer dto)
        {
            //1. session check
            if (ChecksForManufacturerPages())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.Get user object from session
            SessionManager session = (SessionManager)Session["user"];
            Manufacturer manuf = (Manufacturer)Session["manufacturer"];

            //3. checki if user has permission to remove
            if (CheckForPermission(session.user, "UpdateManufacturer"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            dto.ManufacturerId = manuf.ManufacturerId;

            ModifyManufacturer balObj = new ModifyManufacturer();

            if (balObj.UpdateManufacturerBAL(dto))
            {
                //update session
                manuf.ManufacturerName = dto.ManufacturerName;
                manuf.Phone = dto.Phone;
                manuf.Website = dto.Website;
                Session["manufacturer"] = manuf;

                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }
    }
}