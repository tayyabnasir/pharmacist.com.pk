﻿using BAL.ManufacturerMedicineTasks;
using BAL.MedicineAccessBAL;
using Models;
using Models.MedicineRecord;
using Models.Others;
using Models.Roles;
using Pharmacist.HelperClasses;
using Pharmacist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.ManufacturerPages
{
    public class MedicineDataController : TopLevelController
    {
        
        //responsible for getting all forms of medcine
        [HttpGet]
        public JsonResult GetMedicineForms()
        {
            List<String> str = new List<string>();
            str.Add("tab");
            str.Add("cap");
            str.Add("inj");
            str.Add("inf");

            return Json(str, JsonRequestBehavior.AllowGet);
        }


        //responsible to get all availible medicine SI Units
        [HttpGet]
        public JsonResult GetMedicineSIUnits()
        {
            List<String> str = new List<string>();
            str.Add("ml");
            str.Add("mg");
            str.Add("ml/mg");
            str.Add("IU");

            return Json(str, JsonRequestBehavior.AllowGet);
        }

        //to update a Medicine Record
        [HttpPost]
        public JsonResult UpdateMedicine(Medicine med)
        {
            //1.check for session
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.get user and manufacturer from session
            SessionManager session = (SessionManager)Session["user"];
            Manufacturer manufacturer = (Manufacturer)Session["manufacturer"];
            if (manufacturer!=null)
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //3. check for Permissions
            if (CheckForPermission(session.user, "UpdateMedicine"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to update Medicine
            MedicineRecordManagerBAL balObj = new MedicineRecordManagerBAL();

            if(balObj.UpdateMedicine(manufacturer.ManufacturerName, med))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                //in case of error
                return null;
            }
            
        }

        [HttpPost]
        public JsonResult AddMedicine(Medicine med)
        {
            //1.check for session
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.get user and manufacturer from session
            SessionManager session = (SessionManager)Session["user"];
            Manufacturer manufacturer = (Manufacturer)Session["manufacturer"];
            if (manufacturer != null)
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //3. check for Permissions
            if (CheckForPermission(session.user, "AddMedicine"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to update Medicine
            MedicineRecordManagerBAL balObj = new MedicineRecordManagerBAL();

            if (balObj.AddNewMedicine(manufacturer.ManufacturerName, med))
            {
                Object objRet = new
                {
                    success = true,

                };

                return Json(objRet);
            }
            else
            {
                //in case of error
                return null;
            }
        }

        [HttpPost]
        public JsonResult DeleteMedicine(MedicineImages dto)
        {
            //1.check for session
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.get user and manufacturer from session
            SessionManager session = (SessionManager)Session["user"];
            Manufacturer manufacturer = (Manufacturer)Session["manufacturer"];
            if (manufacturer != null)
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //3. check for Permissions
            if (CheckForPermission(session.user, "DeleteMedicineImage"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }

            //4. try to delete Medicine
            MedicineRecordManagerBAL balObj = new MedicineRecordManagerBAL();
            String url = balObj.DeleteMedicineImageBAL(dto,manufacturer.ManufacturerName);
            if (url==null || url.Equals(""))
            {
                return null;
            }
            else
            {
                //try to delete the image from server as well
                if (DeleteImage(url))
                {
                    //do nothing
                }
                else
                {
                    //make entry in log<+++++++++++++++++++
                }

                //in case of error
                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
        }

        [HttpPost]
        public JsonResult AddMedicineImages()
        {
            //1.check for session
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //2.get user and manufacturer from session
            SessionManager session = (SessionManager)Session["user"];
            Manufacturer manufacturer = (Manufacturer)Session["manufacturer"];
            if (manufacturer != null)
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //3. check for Permissions
            if (CheckForPermission(session.user, "AddMedicineImage"))
            {
                //do nothing
            }
            else
            {
                //in case user does not have permission to update
                return null;
            }


            //try to extract the image passed
            ImageNamePair dto = new ImageNamePair();
            if (GetFiles(dto))
            {
                //do nothing
            }
            else
            {
                return null;
            }
            //make entry in database
            MedicineImages medImg = new MedicineImages();
            medImg.medicineId = dto.MedicineId;
            medImg.url = dto.Name;

            MedicineRecordManagerBAL balObj = new MedicineRecordManagerBAL();
            if (balObj.AddMedicineImageBAL(medImg,manufacturer.ManufacturerName))
            {
                //do nothing
            }
            else
            {
                return null;
            }

            //store file on server
            if (VerifyAndStoreImage(dto))
            {
                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }

        }


        private Boolean GetFiles(ImageNamePair dto)
        {
            if(Request.Files!=null && Request.Files.Count > 0 )
            {
                dto.Image = Request.Files.Get(0);
                String medid= Request.Form["MedicineId"];
                int medidInt = -1;
                bool converted=Int32.TryParse(medid,out medidInt);
                if (converted)
                {
                    dto.MedicineId = medidInt;
                }
                else
                {
                    return false;
                }
                //get file extension
                String ext = ImageStoreHelper.GetFileExtension(dto.Image);
                dto.Name = Guid.NewGuid().ToString() + ext;
                return true;
            }
            else
            {
                return false;
            }
        }

        private Boolean VerifyAndStoreImage(ImageNamePair dto)
        {
            ImageVerifier verObj = new ImageVerifier();
            if(verObj.IsLessThanOneMb(dto.Image) && verObj.IsImage(dto.Image))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            ImageStoreHelper storeObj = new ImageStoreHelper();

            if (storeObj.StoreFile(dto.Image, dto.Name, Server.MapPath("~/Content/MedicineImages")))
            {
                //do nothing
            }
            else
            {
                return false;
            }

            return true;
        }

        private Boolean DeleteImage(String url)
        {
            String path = Server.MapPath("~/Content/MedicineImages");

            ImageStoreHelper obj = new ImageStoreHelper();
            if (obj.DeleteFile(url, path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
