﻿using Models.Others;
using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers.ManufacturerPages
{
    //provides functionality related to Medicine management for manufacturer
    public class MedicineManagementController : TopLevelController
    {
        public ActionResult Index()
        {
            //check if user can access manufacturer page
            if (ChecksForManufacturerPages())
            {
                //do nothing
            }
            else
            {
                return RedirectToAction("Login", "Accounts");
            }

            ViewData["Manufacturer"] = (Manufacturer)Session["manufacturer"];

            return View();
        }
    }
}