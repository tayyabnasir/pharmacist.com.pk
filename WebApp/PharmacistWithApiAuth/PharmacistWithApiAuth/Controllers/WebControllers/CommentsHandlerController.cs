﻿using BAL.CommentsBAL;
using Models.Comments;
using Models.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Controllers.WebControllers
{
    public class CommentsHandlerController : TopLevelController
    {
        [HttpPost]
        public JsonResult UserMessage(UserMessage msg)
        {
            UserMessageBAL obj = new UserMessageBAL();

            if (obj.SaveMessage(msg))
            {
                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult GetNextComments(Int32 medid,Int64 start)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            MedicineCommentsBAL obj = new MedicineCommentsBAL();

            return Json(obj.GetNextComments(medid, start));
        }

        [HttpPost]
        public JsonResult AddNewComment(MedicineComment dto)
        {
            if (IsSessionSet())
            {
                //do nothing
            }
            else
            {
                return null;
            }

            SessionManager session = (SessionManager)Session["user"];

            dto.UserId = session.user.UserId;

            //and now to store comment
            MedicineCommentsBAL obj = new MedicineCommentsBAL();
            if (obj.AddComment(dto))
            {
                Object objRet = new
                {
                    success = true

                };

                return Json(objRet);
            }
            else
            {
                return null;
            }
        }
    }
}