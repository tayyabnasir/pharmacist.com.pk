﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.MedicineAccessBAL;
using Models.MedicineRecord;
using BAL.ManufacturerMedicineTasks;
using Models;

namespace Pharmacist.Controllers.WebControllers
{
    //controller to return medicine alternates for web application
    public class MedicineSearchResultsController : TopLevelController
    {
        //for a certain salt return all brandnames along with their different amounts and details in json format
        [HttpGet]
        public JsonResult GetAlternatesForSalt(String Salt)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetMedicineBAL obj = new GetMedicineBAL();

            //calling null returner
            List<MedicineUserInterface> list = obj.GetAlternatesForSalt(Salt);

            //check on client if null is returned than display error message
            return Json(list, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public JsonResult GetAlternatesForBrand(String BrandName)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetMedicineBAL obj = new GetMedicineBAL();

            //calling null returner
            List<MedicineAmoutPair> list = obj.GetAlternatesForBrandName(BrandName);

            //check on client if null is returned then display error
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //recommendations for brand search
        [HttpPost]
        public JsonResult GetRecommendationsForBrand(String prefix)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return Json(obj.GetRecommendationsForBrand(prefix));
        }

        //recommendation for salt search
        [HttpPost]
        public JsonResult GetRecommendationsForSalt(String prefix)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return Json(obj.GetRecommendationsForSalt(prefix));
        }

        //recommendation for Manufacuturer
        [HttpPost]
        public JsonResult GetRecommendationsForManufacturer(String prefix)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetRecommendationsBAL obj = new GetRecommendationsBAL();

            return Json(obj.GetRecommendationsForManufacturer(prefix));
        }

        //to get medicines of the given manufacturer
        [HttpPost]
        public JsonResult GetMedicinesForManufacturer(String manufacturer)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //check if manufacturer is not null
            if(manufacturer==null || manufacturer.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            MedicineRecordManagerBAL obj = new MedicineRecordManagerBAL();

            return Json(obj.GetMedicineRecords(manufacturer));
        }


        //to get medicines for given maufacturer but in MedicineUserInterface Form
        //to get medicines of the given manufacturer
        [HttpPost]
        public JsonResult GetMedicinesUserInterfaceForManufacturer(String manufacturer)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //check if manufacturer is not null
            if (manufacturer == null || manufacturer.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            MedicineRecordManagerBAL obj = new MedicineRecordManagerBAL();

            List<Medicine> med = obj.GetMedicineRecords(manufacturer);

            //convert the Medicine list to MedicineUserInterface

            return Json(MedicineUserInterface.GetMedUserInterfaceForMedicines(med));
        }

        //to get medicines for given brand but in MedicineUserInterface Form
        //to get simillar name brands (not alternates)
        [HttpPost]
        public JsonResult GetResultForSimillarBrandName(String brand)
        {
            //check if session is not set
            if (!IsSessionSet())
            {
                return null;
            }
            else
            {
                //do nothing
            }

            //check if manufacturer is not null
            if (brand == null || brand.Equals(""))
            {
                return null;
            }
            else
            {
                //do nothing
            }

            GetMedicineBAL obj = new GetMedicineBAL();

            List<MedicineUserInterface> med = obj.GetSimillarMedicines(brand);

            
            return Json(med);
        }
    }
}