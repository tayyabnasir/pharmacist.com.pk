﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pharmacist.Models
{
    public class ImageNamePair
    {
        public Int32 MedicineId { get; set; }
        public String Name { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }

}