﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacist.Models
{
    public class PageHtml
    {
        [AllowHtml]
        public String html { get; set; }
    }
}