﻿//to delete a certain rpoduct
function Delete() {
    //ask for confirmation
    if (ConfirmationBox("Are you sure you want to delete the Medicine?")) {
        //do nohitng
    }
    else {
        return;
    }

    //get the row for which delete button was pressed
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the medid of the product
    var medicineId = parseInt(record[0].innerText);

    //validate Medicine Id
    if (isNaN(medicineId) || medicineId < 1) {
        MessageModalPopUp("Delete Error", "Please refresh page and try again");
        return;
    }

    //try to delete the product

    var packet = {
        data: {
            "MedicineId": medicineId
        },
        url: "/MedicineData/DeleteMedicine",
        type: "POST",
        dataType: "json",
        success: function (response) {
            OnSuccess(response,"Medicine Delete");
        },
        error: function (response) {
            OnError("Medicine Delete Failed.");
        }
    };

    $.ajax(packet);
    //show loader 
    $("#loader").show();
}