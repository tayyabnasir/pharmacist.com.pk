﻿function ResetImageModal() {
    var modal = $("#picModal");
    //remove medicine fields
    modal.find("input").each(function () {
        $(this).val("");
    });

    $("#imgPreview").attr("src", imgPath + "default.png");

    //close the modal
    $("#picModal").modal("toggle");
}