﻿function updatePackaging() {
    var TotalUnitsPerPack = $("#totalUnitsPerPack");
    TotalUnitsPerPack.text("1");

    var PackSize = $("#packSize");
    PackSize.text("");

    var pack1=$("#pack1").val();
    var pack2=$("#pack2").val();
    var pack3=$("#pack3").val();
    var pack4=$("#pack4").val();

    var packSizeVal = PackSize.text();
    var totalUnitsPerPackVal = 0;

    if (pack1 != null && pack1 != "" && !isNaN(pack1)) {
        packSizeVal=(pack1 + "x");
        totalUnitsPerPackVal += parseInt(pack1);
        TotalUnitsPerPack.text(totalUnitsPerPackVal);
    }
    if (pack2 != null && pack2 != "" && !isNaN(pack2)) {
        packSizeVal+=(pack2 + "x");
        totalUnitsPerPackVal *= parseInt(pack2);
        TotalUnitsPerPack.text(totalUnitsPerPackVal);
    }
    if (pack3 != null && pack3 != "" && !isNaN(pack3)) {
        packSizeVal+=(pack3 + "x");
        totalUnitsPerPackVal *= parseInt(pack3);
        TotalUnitsPerPack.text(totalUnitsPerPackVal);
    }
    if (pack4 != null && pack4 != "" && !isNaN(pack4)) {
        packSizeVal+=(pack4 + "x");
        totalUnitsPerPackVal *= parseInt(pack4);
        TotalUnitsPerPack.text(totalUnitsPerPackVal);
    }
    var pTxt = packSizeVal;
    PackSize.text(pTxt.substr(0, pTxt.length - 1));
}