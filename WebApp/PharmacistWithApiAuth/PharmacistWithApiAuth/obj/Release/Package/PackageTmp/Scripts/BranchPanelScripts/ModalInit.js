﻿//display Modal incase add button pressed
function DisplayModalForAdd() {
    //set branch id 0 which is used as a check while sending data to server
    branchId = 0;
    //show the modal
    $("#addModal").modal();
}

//display modal after initializing it in case of update button pressed
function UpdateModal() {
    //get the row for which update button was pressed
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the branchid of the product
    branchId = record[0].innerText;

    //get the address and phone for the branch
    var address = record[1].innerText;
    var phone = record[2].innerText;

    //in case address or phone cannot be get means something is wrong so prompt
    //to refresh the page
    if (address == null || phone == null) {
        MessageModalPopUp("Internal Error","Please refresh the page");
        return;
    }

    $("#address").val(address);
    $("#phone").val(phone);
    //show the modal
    $("#addModal").modal();
}


//to clear fields of modal and close it
function ResetModal() {
    $("#address").val("");
    $("#phone").val("");
    $("#updateModal").modal("toggle");
}