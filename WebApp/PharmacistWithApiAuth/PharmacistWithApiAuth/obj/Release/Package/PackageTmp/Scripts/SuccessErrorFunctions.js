﻿//triggered on success of ajax request For Stock items
function OnSuccess(response, msg) {
    //hide loader
    $("#loader").hide();
    if (response.success == true) {
        MessageModalPopUp("Error", msg + " Successful");
        //reload the page
        document.location.reload(true);
    }
    else {
        MessageModalPopUp("Error", msg + " Failed");
    }
}

//triggered on error of ajax request
function OnError(msg) {
    //hide loader
    $("#loader").hide();
    MessageModalPopUp("Error", msg + " Server Down. Try Later!!");
}