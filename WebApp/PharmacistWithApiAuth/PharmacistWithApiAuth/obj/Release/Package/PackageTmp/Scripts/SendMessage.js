﻿function SendMessageToServer() {
    var email = $("#msgEmail").val();
    var message = $("#comment").val();

    if (email == null || email == "") {
        MessageModalPopUp("Message Sending Error", "Please Enter an Email.");
        return;
    }
    if (message == null || message == "") {
        MessageModalPopUp("Message Sending Error", "Please Enter your Message.");
        return;
    }
    if (message.length >= 1500) {
        MessageModalPopUp("Message Sending Error", "Message can be only 1500 characters long.");
        return;
    }

    var data = {
        "message": message,
        "email": email
    };

    var packet = {
        data: data,
        url: "/CommentsHandler/UserMessage",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            $("#loader").hide();
            if (response.success == true) {
                MessageModalPopUp("Success", "Message has been sent. Thank you for your feedback.");
            }
            else {
                MessageModalPopUp("Message Fail", "Message sending failed. Please refresh and try again.");
            }
        },
        error: function (response) {
            $("#loader").hide();
            MessageModalPopUp("Message Fail", "Message sending failed. Please refresh and try again.");
        }
    };

    $.ajax(packet);
    $("#loader").show();
}