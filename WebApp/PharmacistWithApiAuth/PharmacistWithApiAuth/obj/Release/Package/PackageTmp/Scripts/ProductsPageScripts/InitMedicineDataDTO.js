﻿var medicineDTO = [];//will hold Medicine records for the manufacturer
var units = [];//will hold units for medicine
var forms = [];//will hold medicine forms


//function to get units from server via ajax call
function InitUnits() {
    //create the packet for ajax request
    var packet = {
        data: '',
        url: "/MedicineData/GetMedicineSIUnits",
        type: "GET",
        datatype: "JSON",
        success: function (response) {
            units = response;
            return;
        },
        error: function (response) {
            MessageModalPopUp("Load Error", "Server is down");
            return;
        }
    };

    $.ajax(packet);
}

//function to get drug forms from server
function InitForms() {
    var packet = {
        data: '',
        url: "/MedicineData/GetMedicineForms",
        type: "GET",
        datatype: "JSON",
        success: function (response) {
            forms = response;
            return;
        },
        error: function (response) {
            MessageModalPopUp("Load Error", "Server is down");
            return;
        }
    };

    $.ajax(packet);
}





function InitMedicineDTO() {
    

    //1. make an ajax call and get the medicine data
    
    var packet = {
        data: data,//data must contain current manufacturer name (this is done inside View)
        url: "/MedicineSearchResults/GetMedicinesForManufacturer",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            medicineDTO=response;

            convertTomedicineUserInterfaceDTO(medicineDTO);//1.
        },
        error: function (response) {
            MessageModalPopUp("Data Get Error", "Server is down");
            return;
        }
    };

    $.ajax(packet);
}

//1.-------------------------------------------------------------
//function to convert the medicineDTO to medicineUserInterfaceDTO
function convertTomedicineUserInterfaceDTO(medDTO) {
    

    var medicineUserInterfaceDTO = [];
    for (var i in medDTO) {
        var tempObj = medDTO[i];

        //create a meduser interface object and put the required fields from Medicine
        //object
        var medicineUserInterfaceDTOObj = {};
        medicineUserInterfaceDTOObj.MedicineId = tempObj["MedicineId"];
        medicineUserInterfaceDTOObj.BrandName = tempObj["BrandName"];
        medicineUserInterfaceDTOObj.DrugForm = tempObj["DrugForm"];
        medicineUserInterfaceDTOObj.PackSize = tempObj["PackSize"];
        medicineUserInterfaceDTOObj.TradePrice = tempObj["TradePrice"];

        //and now making up salt fields
        medicineUserInterfaceDTOObj.SaltName = "";
        medicineUserInterfaceDTOObj.SaltAmount = "";
        var salt = tempObj["Salt"];

        for (var j in salt) {
            medicineUserInterfaceDTOObj.SaltName += salt[j]["SaltName"]+"+";
            medicineUserInterfaceDTOObj.SaltAmount += salt[j]["Amount"] + salt[j]["Unit"] + "+";
        }

        //remove any trailing + signs
        medicineUserInterfaceDTOObj.SaltName = medicineUserInterfaceDTOObj.SaltName.replace(/\+$/, '');
        medicineUserInterfaceDTOObj.SaltAmount = medicineUserInterfaceDTOObj.SaltAmount.replace(/\+$/, '');

        //finally append the object to array
        medicineUserInterfaceDTO.push(medicineUserInterfaceDTOObj);
    }

    //display the enteris in  datatable
    InitDataTable(medicineUserInterfaceDTO);//2.
}