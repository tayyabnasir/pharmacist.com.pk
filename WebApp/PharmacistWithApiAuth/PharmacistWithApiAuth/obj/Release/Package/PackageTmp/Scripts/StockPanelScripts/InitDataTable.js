﻿//for the given data init the datatable
function InitDataTable(stockData) {

    var table = $("#resultsTable").DataTable({
        data: stockData,
        columns: [
            { data: "ItemId" },
            { data: "MedicineId" },
            { data: "BrandName" },
            { data: "Manufacturer" },
            { data: "DrugForm" },
            { data: "PackSize" },
            { data: "TradePrice" },
            { data: "Salt" },
            { data: "SaltAmount" },
            { data: "RetailPrice" },
            { data: "Available" },
            { data: 0 }//added extara column for displaying buttons
        ],
        columnDefs: [{//content for the default column
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu"><li><a href="#"  class="updateClass">Update</a></li><li class="divider"></li><li><a href="#" class="deleteClass">Delete</a></li></ul></div>'
        }]
    });
    

    //adding click event to all update buttons which opens up the editing modal
    $("#resultsTable tbody").on("click", ".updateClass", openModal);
    //adding click event to all delete buttons
    $("#resultsTable tbody").on("click", ".deleteClass", RemoveFormStock);
}