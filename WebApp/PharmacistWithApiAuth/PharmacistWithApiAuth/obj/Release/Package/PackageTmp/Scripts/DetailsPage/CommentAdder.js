﻿function AddComment() {
    var comment = $("#commentBoxTxt").val();
    if (comment == null || comment == "") {
        MessageModalPopUp("Comment Error", "Please Enter a Comment First.");
        return;
    }
    else if (comment.length > 500) {
        MessageModalPopUp("Comment Error", "Please Enter a Comment Less Than 500 Characters.");
        return;
    }

    var data = {
        "MedicineId": medid,
        "Comment": comment
    };

    var packet = {
        data: data,
        url: "/CommentsHandler/AddNewComment",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            if (response.success == true) {
                Refresh();
            }
            else {
                MessageModalPopUp("Comment Error", "Comment cannot be made at this time. Try again later.");
            }
        },
        error: function () {
            MessageModalPopUp("Comment Error", "Comment cannot be made at this time. Try again later.");
        }
    };

    $.ajax(packet);

    $("#commentBoxTxt").val("");
}

function Refresh() {
    offset = 0;
    GetComments();
    $("#commentContainer").empty();
}