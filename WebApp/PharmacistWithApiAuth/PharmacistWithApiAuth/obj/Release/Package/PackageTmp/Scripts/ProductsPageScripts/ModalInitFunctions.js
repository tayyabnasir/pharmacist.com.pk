﻿var medid = -1;

//open model for Update button pressed for product
function openModal() {
    //get the row for which update button was pressed
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the medid of the product
    medid = record[0].innerText;

    

    //in case a previous script could not load
    if (medicineDTO == null) {
        MessageModalPopUp("Error", "Please refresh the page");
        return;
    }

    var medObj = null;
    //get the Medicine Object for the medicineid
    for (var i in medicineDTO) {
        if (medicineDTO[i]["MedicineId"] == medid) {
            medObj = medicineDTO[i];
            break;
        }
    }

    //in case of tempered medicineid
    if (medObj == null) {
        return;
    }

    BindToModal(medObj);
    currentMedId = medid;
    $("#updateModal").modal();
    
}

//fill in the fields of modal with the given medObj
function BindToModal(medObj) {
    //1. set values for basic attributes

    $("#brandName").val(medObj["BrandName"]);
    $("#tradePrice").val(medObj["TradePrice"]);

    //2. init the DrugForm dropdown

    var dropDown = $("#drugForm");
    for (var i in forms) {
        if (forms[i] == medObj["DrugForm"]) {
            dropDown.append("<option selected='selected'>" + forms[i] + "</option>");
        }
        else {
            dropDown.append("<option>" + forms[i] + "</option>");
        }
    }

    //3. init the Packaging Part
    
    $("#packSize").text(medObj["PackSize"]);
    $("#totalUnitsPerPack").text(medObj["TotalUnitsPerPack"]);

    //4. salt part
    for (var i in medObj["Salt"]) {
        $("#addSaltFieldBtn").click();
        var saltFields = $("#saltContainer fieldset:last");

        saltFields.find("#saltName").val(medObj["Salt"][i]["SaltName"]);
        saltFields.find("#saltAmount").val(medObj["Salt"][i]["Amount"]);

        //correct this
        //and adding select to matching units
        saltFields.find("#saltUnit option").filter(function () {
                
            return $(this).text() == medObj["Salt"][i]["Unit"];
        }).prop('selected', true);
        
    }
}

//call this to reset each field of Modal and close the modal
function ResetModal() {
    var modal = $("#updateModal");
    //remove medicine fields
    modal.find("input").each(function () {
        $(this).val("");
    });

    //remove salt fields
    modal.find("fieldset").each(function () {
        $(this).remove();
    });

    modal.find("#packSize").text("");
    modal.find("#totalUnitsPerPack").text("");

    //close the modal
    $("#updateModal").modal("toggle");
}