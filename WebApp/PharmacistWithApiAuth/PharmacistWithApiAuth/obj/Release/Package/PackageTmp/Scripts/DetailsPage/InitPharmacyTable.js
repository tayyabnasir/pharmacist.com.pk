﻿function GetPharmacies(medid){
    var data = { "medid": medid };

    var packet = {
        data: data,
        url: "/MedicineDetails/GetPharmaciesWithMedicine",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            InitDataTable(response);
        },
        error: function (response) {
            alert("No Pharmacies fouind");
        }
    };

    $.ajax(packet);
}

function InitDataTable(data) {
    var table = $("#resultsTable").DataTable({
        data: data,
        columns: [
            { data: "PharmacyId" },
            { data: "PharmacyName" },
            { data: "RetailPrice" },
            { data: "PharmacyName" },
            { data: "PharmacyName" },
            { data: "PharmacyName" }
        ],
        "initComplete": function () {
                    $("#loader").hide();
        },
        columnDefs: [{
            targets: 3,
            render: function (data, type, row, meta) {
                if (type === 'display') {
                    data = '<a href="/AccessPharmacy/GetPharamcyBranches?pharmacy=' + data + '">' + "Click For Branches" + '</a>';
                }

                return data;
            }
        },
        {
            targets: 4,
            render: function (data, type, row, meta) {
                if (type === 'display') {
                    data = '<a href="/AccessPharmacy/GetPharmacyStock?pharmacy=' + data + '">' + "Click To View Pharmacy Stock" + '</a>';
                }

                return data;
            }
        },
        {
            targets: 5,
            render: function (data, type, row, meta) {
                if (type === 'display') {
                    data = '<a href="/WebPage/GetPharmacyPage?pharmacy=' + data + '">' + "Click for Pharmacy Page" + '</a>';
                }

                return data;
            }
        }
        ]
    });
}