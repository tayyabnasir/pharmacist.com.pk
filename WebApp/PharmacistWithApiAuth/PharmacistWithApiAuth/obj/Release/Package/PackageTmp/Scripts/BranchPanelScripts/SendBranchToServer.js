﻿//to prepare and validate the branch object before sending to server
function SendToServer() {
    //in case no id was extracted
    if (branchId == -1) {
        MessageModalPopUp("Error", "Please Select a Product to Update");

        return;
    }
    //extract data from the modal
    var address = $("#address").val();
    var phone = $("#phone").val();

    //check for validity of address and phone
    if (address == null || address == "") {
        MessageModalPopUp("Error", "Please Enter a valid Address");
        return;
    }
    if (phone == null || phone == "") {
        MessageModalPopUp("Error", "Please Enter a valid Phone Number");
        return;
    }

    //create the branch object to be sent to server
    var branch = {
        "BranchId": branchId,
        "Address": address,
        "Phone": phone
    };

    //check if medid is zero than make request to add product else update product
    if (branchId == 0) {
        SendProductToServerForAdd(branch);
    }
    else {
        SendProductToServerForUpdate(branch);
    }
}

//send to controller to add product
function SendProductToServerForAdd(branch) {
    var packet = {
        data: branch,
        url: "/BranchManager/AddBranch",
        type: "POST",
        datatype: "JSON",
        success: function (d) {
            OnSuccess(d, "Branch Adding");
        },
        error: function (d) {
            OnError("Branch Adding Failed.");
        }
    };

    $.ajax(packet);
    //reset modal and show loader
    ResetModal();
    $("#loader").show();
}

//send branch to send to update controller
function SendProductToServerForUpdate(branch) {
    var packet = {
        data: branch,
        url: "/BranchManager/UpdateBranch",
        type: "POST",
        datatype: "JSON",
        success: function (d) {
            OnSuccess(d, "Branch Update");
        },
        error: function (d) {
            OnError("Branch Update Failed.");
        }
    };

    $.ajax(packet);
    //reset modal and show loader
    ResetModal();
    $("#loader").show();
}


function Delete() {
    //ask for confirmation
    if (ConfirmationBox("Are you sure you want to delete the Branch?")) {
        //do nohitng
    }
    else {
        return;
    }

    //get the row for which update button was pressed
    var row = $(this).closest("tr");

    var record = row.find("td");
    
    //find the medid of the product
    branchId = parseInt(record[0].innerText);

    //check if branch id is valid
    if (isNaN(branchId) || branchId == -1 || branchId == 0) {
        MessageModalPopUp("Error", "Please refresh the Page");
        return;
    }

    var packet = {
        data: { "branchId": branchId },
        url: "/BranchManager/DeleteBranch",
        type: "POST",
        datatype: "JSON",
        success: function (d) {
            OnSuccess(d, "Branch Delete");
        },
        error: function (d) {
            OnError("Branch Delete Failed.");
        }
    };

    $.ajax(packet);
    //reset modal and show loader
    ResetModal();
    $("#loader").show();
}