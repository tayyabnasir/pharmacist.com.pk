﻿//IS used to extract fields from the modal and send them to server to be updated
//also will be used to add medicine but onliy medid will be in that case 0
function UpdateMedProd() {
    //1. check if medicienId is set or not
    if (medid == -1) {
        MessageModalPopUp("Error", "Please Select a Product to Update");

        return;
    }

    //2. Get data from each Field of modal in medicine DTO

    var medObj = {};

    medObj["MedicineId"] = parseInt(medid);
    medObj["BrandName"] = $("#brandName").val();
    medObj["DrugForm"] = $("#drugForm").val();
    medObj["TradePrice"] = parseInt($("#tradePrice").val());
    medObj["PackSize"] = $("#packSize").text();
    medObj["TotalUnitsPerPack"] = parseInt($("#totalUnitsPerPack").text());
    medObj["Salt"] = [];

    //and for salts

    var saltFields = $("#saltContainer fieldset");


    saltFields.each(function () {
        var saltObj = {};
        saltObj["SaltName"] = $(this).find("#saltName").val();
        saltObj["Amount"] = parseInt($(this).find("#saltAmount").val());
        saltObj["Unit"] = $(this).find("#saltUnit").val();
        medObj["Salt"].push(saltObj);
    });

    ValidateAllFields(medObj);
}

//checks if any data entered is inconsistant or incorrect
function ValidateAllFields(medObj) {
    
    if (medObj["BrandName"] == "" || medObj["BrandName"] == null) {
        MessageModalPopUp("Error", "Please Provide Name for Your Product");
        return;
    }
    if (medObj["PackSize"] == "" || medObj["PackSize"] == null) {
        MessageModalPopUp("Error", "Please Enter Packaging Details for Your Product");
        return;
    }
    if (medObj["TotalUnitsPerPack"] == "" || medObj["TotalUnitsPerPack"]==null || isNaN(medObj["TotalUnitsPerPack"])) {
        MessageModalPopUp("Error", "Please Provide Units Details Name for Your Product");
        return;
    }
    if (medObj["TradePrice"] == "" || medObj["TradePrice"] == null || isNaN(medObj["TradePrice"])) {
        MessageModalPopUp("Error", "Please Provide Trade Price for Your Product");
        return;
    }

    //check if salt count is 0
    if (medObj["Salt"].length < 1) {
        MessageModalPopUp("Error", "Please Provide Atleast on Salt Details");
        return;
    }


    for (var salt in medObj["Salt"]) {
        if (medObj["Salt"][salt]["Amount"] == "" || medObj["Salt"][salt]["Amount"] == null || isNaN(medObj["Salt"][salt]["Amount"])) {
            MessageModalPopUp("Error", "Please Fill in All Salt Amounts");
            return;
        }
        if (medObj["Salt"][salt]["SaltName"] == "" || medObj["Salt"][salt]["SaltName"] == null) {
            MessageModalPopUp("Error", "Please Fill in All Salt Names");
            return;
        }
        if (medObj["Salt"][salt]["Unit"] == "" || medObj["Salt"][salt]["Unit"] == null) {
            MessageModalPopUp("Error", "Please Select Valid Units For Salt");
            return;
        }
    }

    //check if medid is zero than make request to add product else update product
    if (medid == 0) {
        SendProductToServerForAdd(medObj);
    }
    else {
        SendProductToServerForUpdate(medObj);
    }
    
}

//send the verified data to server
function SendProductToServerForUpdate(data) {
    var packet = {
        data: data,
        url: "/MedicineData/UpdateMedicine",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            OnSuccess(response, "Medicine Update");
        },
        error: function (response) {
            OnError("Medicine Update failed.");
        }
    };

    //show loader and hide modal
    $.ajax(packet);
    ResetModal();
    $("#loader").show();
}

//send the verified data to server
function SendProductToServerForAdd(data) {
    var packet = {
        data: data,
        url: "/MedicineData/AddMedicine",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            OnSuccess(response, "Medicine Add");
        },
        error: function (response) {
            OnError("Medicine Add failed.");
        }
    };

    $.ajax(packet);
    ResetModal();
    $("#loader").show();
}