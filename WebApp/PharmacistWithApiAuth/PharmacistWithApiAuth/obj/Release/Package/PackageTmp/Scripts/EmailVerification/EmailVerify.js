﻿//Email Verification script
//takes an email as argument and make an ajax request to send Verification for email
function VerifyEmail(email) {
    var data = {};
    //check if provided e
    data.email = email;
    if (data.email.length == 0) {
        MessageModalPopUp("Email Invalid","Please Enter an Email to Verify.");
        return;
    }
    else {
        //do nothing
    }

    //create packet to send to server
    var settings = {
        type: "POST",
        data: data,
        dataType: "json",
        url: '/Accounts/ResendVerificationEmail',
        success: function (d) {
            OnSuccess(d, "");
        },
        error: function (d) {
            OnError("Email Sending Failed.")
        }
    };
    $.ajax(settings);
    //show loader
    $("#loader").show();
}
