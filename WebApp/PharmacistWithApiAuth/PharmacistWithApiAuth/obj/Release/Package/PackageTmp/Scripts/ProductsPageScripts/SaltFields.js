﻿//this object will serve as a counter of salt fields
var salt = [{ id: "salt1", saltNameTxt: "", saltAmountTxt: "", selectedSIUnit: "" }];

function AddSalt() {
    var saltContainer = $("#saltContainer");
    
    var newItemNo = salt.length + 1;
    salt.push({ 'id': 'salt' + newItemNo, saltNameTxt: "", saltAmountTxt: "", selectedSIUnit: "" });

    //make the field set object to append
    var fieldset = $("<fieldset id='salt" + newItemNo + "' class='row'></fieldset>");

    var saltName = $("<div class='col-sm-6'><div class='form-group'><span class='input-group-addon'>Salt Name</span><input type='text' class='form-control' placeholder='Enter Salt Name' id='saltName'></div></div>");

    var saltAmount = $("<div class='col-sm-6'><div class='form-group'><span class='input-group-addon'>Salt Amount</span><input type='number' class='form-control' placeholder='Enter Salt Amount' id='saltAmount'></div></div>");

    var saltUnit = $("<div class='col-xs-12'><div class='form-group'><span class='input-group-addon'>Salt Unit</span></div></div>");

    var selectForUnit = $("<select class='form-control'  id='saltUnit'></select>");

    //and adding units
    for (var j in units) {
        selectForUnit.append("<option>" + units[j] + "</option>");
    }

    saltUnit.append(selectForUnit);

    var removeBtn = $("<div class='col-xs-12'><br/><div class='text-right'><button class='remove btn btn-danger' >x</button></div><hr/></div>");

    removeBtn.find(".remove").click(RemoveSalt);

    fieldset.append(saltName);
    fieldset.append(saltAmount);
    fieldset.append(saltUnit);
    fieldset.append(removeBtn);

    saltContainer.append(fieldset);
}

//remove all salt fields of the selected salt
RemoveSalt = function () {
    var fieldset = $(this).closest("fieldset");

    fieldset.remove();

};
