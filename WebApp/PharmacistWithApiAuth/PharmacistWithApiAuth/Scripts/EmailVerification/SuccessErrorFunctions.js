﻿//triggered on success of ajax request For Email verification
function OnSuccess(response, msg) {
    //hide loader
    $("#loader").hide();
    if (response.success == true) {
        MessageModalPopUp("Success", msg + "Email Sent. Login Now.");
    }
    else {
        MessageModalPopUp("Email Sending Failed",msg + "Email Sending Failed");
    }
}

//triggered on error of ajax request
function OnError(msg) {
    //hide loader
    $("#loader").hide();
    MessageModalPopUp("Somethings Wrong",msg + " Server not Responding. Try Later!!");
}