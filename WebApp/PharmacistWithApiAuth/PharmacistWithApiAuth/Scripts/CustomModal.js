﻿function MessageModalPopUp(title,message) {
    var modal = $("#messageModal");
    var $title = modal.find("#modalTitle");
    var $body = modal.find("#modalBody");

    $title.text(title);
    $body.text(message);

    modal.modal();
}