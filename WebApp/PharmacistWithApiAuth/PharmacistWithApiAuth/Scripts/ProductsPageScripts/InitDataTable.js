﻿//2.-------------------------------------------
//for the passed MedicineUSerInterface list initializes datatable
function InitDataTable(medUserInterfaceDTO) {
    //for the passed object initialize the datatable
    var table = $("#resultsTable").DataTable({
        data: medUserInterfaceDTO,
        columns: [
            { data: "MedicineId" },
            { data: "BrandName" },
            { data: "DrugForm" },
            { data: "PackSize" },
            { data: "TradePrice" },
            { data: "SaltName" },
            { data: "SaltAmount" },
            { data: 0 }//added extara column for displaying buttons
        ],
        columnDefs: [
        {//content for the default column
            "targets": -1,
            "data": null,
            "defaultContent": '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu"><li><a href="#"  class="updateClass">Update</a></li><li><a href="#" class="deleteClass">Delete</a></li><li class="divider"></li><li><a href="#" class="imgAddClass">Add Preview Picture</a></li><li><a href="#" class="imgDeleteClass">Delete Preview Image</a></li></ul></div>'
        }]
    });
    

    //adding click event to all update buttons which opens up the editing modal
    $("#resultsTable tbody").on("click", ".updateClass", openModal);
    //adding click event to all delete buttons
    $("#resultsTable tbody").on("click", ".deleteClass", Delete);
    //adding click event to image options
    $("#resultsTable tbody").on("click", ".imgAddClass", AddImage);
    $("#resultsTable tbody").on("click", ".imgDeleteClass", DeleteImage);

}


