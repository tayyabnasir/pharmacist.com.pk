﻿function PutImagesOnModal(imgArr) {
    var container = $("#imgContainerCarousal");
    var count = 0;

    var listToAppend;
    var imgToAppend;
    for (var index in imgArr) {
        if (count == 0) {
            
            imgToAppend = "<div class='item active'><a class=' thumbnail'><span class='imgId'>" + imgArr[index]["id"] + "</span><img class='img-responsive' src='" + imgPath + imgArr[index]["url"] + "' alt='Medicnie'></a></div>";
        }
        else {
            imgToAppend = "<div class='item'><a  class='thumbnail'><span class='imgId'>" + imgArr[index]["id"] + "</span><img class='img-responsive' src='" + imgPath + imgArr[index]["url"] + "' alt='Medicnie'></a></div>";
        }
        count++;
        container.append(imgToAppend);
    }

    //check if no result wsa found
    if (count == 0) {
        ErrorLoadingImagesForManufacturer();
        return;
    }

    $("#loader").hide();



    ShowModal();
}

function ErrorLoadingImagesForManufacturer() {

    $("#loader").hide();
    MessageModalPopUp("No Result", "No Images were found");
}

function ShowModal() {
    $("#picDeleteModal").modal();
}

function DeleteImage() {
    //call loader images
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the medid of the product
    medidForPicLoad = record[0].innerText;
    LoadImagesForMedicineId(medidForPicLoad, PutImagesOnModal, ErrorLoadingImagesForManufacturer);
}


function DeleteSelectedImage() {
    var currentImg = $("#imgContainerCarousal .active .imgId").text();
    var data = {
        medicineId: medidForPicLoad,
        id: currentImg
    };

    var packet = {
        data: data,
        url: "/MedicineData/DeleteMedicine",
        type: "POST",
        datatype: "JSON",
        success: function (resposne) {
            $("#loader").hide();
            if (resposne.success == true) {
                MessageModalPopUp("Success","Medicine Deleted");
            }
            else {
                MessageModalPopUp("Delete Failed", "Medicine deleting Failed. Try again.");
            }
            
        },
        error: function () {
            $("#loader").hide();
            MessageModalPopUp("Connection Fail","Connection to server could not be made at this time.");
            
        }
    };
    $("#loader").show();
    $.ajax(packet);
    ResetImageDeleteModal();
}

function ResetImageDeleteModal() {
    $("#imgContainerCarousal").empty();
    $("#picDeleteModal").modal("toggle");
}