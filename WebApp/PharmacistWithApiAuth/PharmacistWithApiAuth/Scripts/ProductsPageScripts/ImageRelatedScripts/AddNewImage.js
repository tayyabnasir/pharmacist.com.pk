﻿var medidForImages = -1;

function AddImage() {
    //1. show the image upload model
    $("#picModal").modal();
    //2. get medicine id for the record selected
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the medid of the product
    medidForImages = record[0].innerText;

}


//function to upload image to server
function UploadImageToServer() {
    //1. Validate image
    var $image = $("#imgToUpload");
    var files = $image.prop('files');
    console.log(files[0]);
    if (files && files.length > 0) {
        //check if file size and type attrributes are valid
        if (files[0].size > 1048576) {
            MessageModalPopUp("Error", "File must be less than 1 MB in size");
            return;
        }

        f = files[0];

        var ext = f.name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            MessageModalPopUp("Error", 'Please select an image file');
            return;
        }
    }

    //2. prepare the dto to send
    
    var data = new FormData();
    data.append("Image", f);
    data.append("Name", f.name);
    data.append("MedicineId", medidForImages);
    console.log(f);
    var packet = {
        data: data,
        url: '/MedicineData/AddMedicineImages',
        type: 'POST',
        contentType: false,
        processData: false,
        datatype: "JSON",
        success: function (d) {
            $("#loader").hide();
            if (d.success == true) {
                MessageModalPopUp("Success", "Image was uploaded successfully.");
            }
            else {
                MessageModalPopUp("Error", "Image Upload Failed.");
            }
        },
        error: function (d) {
            $("#loader").hide();
            MessageModalPopUp("Error", "Image Upload Failed.");
        }
    };
    $("#loader").show();
    //make ajax call to upload image
    $.ajax(packet);
    ResetImageModal();
}


//reset the add image modal


//function to update image preview
function UpdateImagePreview() {
    var files = this.files;
    if (files && files.length > 0) {
        //check if file size and type attrributes are valid
        if (files[0].size > 1048576) {
            alert(files[0].size);
            MessageModalPopUp("Error", "File must be less than 1 MB in size");
            return;
        }

        f = files[0];

        var ext = f.name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            MessageModalPopUp("Error", 'Please select an image file');
            return;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            $("#imgPreview").attr("src", e.target.result);
        };

        reader.readAsDataURL(files[0]);

        //also enable add image button
        $('#addImageBtn').prop('disabled', false);
    }
}

//bind the close button of modal with modal reset


//bind the AddImage Button