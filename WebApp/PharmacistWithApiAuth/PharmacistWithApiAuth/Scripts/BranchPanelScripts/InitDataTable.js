﻿//recives branches objects array and put them in datatable to be displayed
function InitTable(branches) {
    var table = $("#resultsTable").DataTable({
        data: branches,
        columns: [
            { data: "BranchId" },
            { data: "Address" },
            { data: "Phone" },
            { data: "Longitude" },
            { data: "Latitude" },
            { data: 0 }//added extara column for displaying buttons
        ],
        columnDefs: [{//content for the default column
            "targets": -1,
            "data": null,
            "defaultContent": '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu"><li><a href="#"  class="updateClass">Update</a></li><li class="divider"></li><li><a href="#" class="deleteClass">Delete</a></li></ul></div>'
        }]
    });

    //adding click event to all update buttons which opens up the editing modal
    $("#resultsTable tbody").on("click", ".updateClass", UpdateModal);
    //adding click event to all delete buttons
    $("#resultsTable tbody").on("click", ".deleteClass", Delete);
}