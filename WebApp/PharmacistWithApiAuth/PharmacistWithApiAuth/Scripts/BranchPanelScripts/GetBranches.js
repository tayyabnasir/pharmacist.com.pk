﻿var branches = [];

//responsible to get all the branches of pharmacy and pass it on to datatable to be
//initialized
function GetBranchesForPharmacy(pharmacy) {
    var packet = {
        data: { "pharmacy": pharmacy },
        url: "/BranchManager/GetBranchesOfPharmacy",
        type: "POST",
        datatype: "JSON",
        success: function (d) {
            branches = d;
            //call to init dataTable
            InitTable(branches);
        },
        error: function () {
            MessageModalPopUp("Branches Error","No Branches To Display.");
        }
    };
    $.ajax(packet);
}