﻿function LoadImagesForMedicineId(medid,successFunc,errFunc) {
    var data = { "medid": medid };

    var packet = {
        url: "/MedicineDetails/GetImagesForMedicine",
        type: "POST",
        datatype: "JSON",
        data: data,
        success: successFunc,//must hide loader
        error: errFunc//must hide loader

    };

    $.ajax(packet);
    $("loader").show();
}

