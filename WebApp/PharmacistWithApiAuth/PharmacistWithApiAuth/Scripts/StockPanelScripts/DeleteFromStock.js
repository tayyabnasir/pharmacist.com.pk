﻿//to remove a product from stock
function RemoveFormStock() {
    //ask for confirmation
    if (ConfirmationBox("Are you sure you want to delete Stock Item?")) {
        //do nohitng
    }
    else {
        return;
    }

    //get the row for which delete button was pressed
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the medid of the product
    var itemId = record[0].innerText;

    //try to delete the product

    var packet = {
        data: {
            "itemId": itemId
        },
        url: "/StockManage/RemoveFromStock",
        type: "POST",
        dataType: "josn",
        success: function (response) {
            OnSuccess(response, "Stock Delete");
        },
        error: function (response) {
            OnError("Stock Delete Failed.");
        }
    };

    $.ajax(packet);
    $("#loader").show();
}