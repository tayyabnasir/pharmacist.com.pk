﻿function openModal() {
    var row = $(this).closest("tr");

    var record = row.find("td");

    //find the itemid of the product
    itemId = record[0].innerText;

    //in case a previous script could not load
    if (stock == null) {
        MessageModalPopUp("Error", "Please refresh the page");
        return;
    }

    var stockObj = null;
    //get the Medicine Object for the medicineid
    for (var i in stock) {
        if (stock[i]["ItemId"] == itemId) {
            stockObj = stock[i];
            break;
        }
    }

    //in case of tempered medicineid
    if (stockObj == null) {
        MessageModalPopUp("Error", "No Stock object found");
        return;
    }

    BindToModal(stockObj);
    $("#updateModal").modal();
}

//fill in the fields of modal with the given medObj
function BindToModal(Obj) {
    //1. set values for basic attributes

    $("#brandName").text(Obj["BrandName"]);
    $("#tradePrice").text(Obj["TradePrice"]);

    $("#drugForm").text(Obj["DrugForm"]);
    $("#manufacturer").text(Obj["Manufacturer"]);

    $("#salt").text(Obj["Salt"]);
    $("#saltAmount").text(Obj["SaltAmount"]);

    $("#retailPrice").val(Obj["RetailPrice"]);

    var available = Obj["Available"];
    
    if (available ==true) {
        $("#availability").val("true");
    }
    else {
        $("#availability").val("false");
    }

}


//call this to reset each field of Modal
function ResetModal() {
    var modal = $("#updateModal");

    modal.find("input").each(function () {
        $(this).val("");
    });

    modal.find("p").each(function () {
        $(this).text("");
    });

    $("#updateModal").modal("toggle");
}