﻿function SendDataToUpdate(){
    var stockItem = {};
    
    ValidateInputs(stockItem);
}

function ValidateInputs(stockItem) {
    stockItem.ItemId = itemId;

    //validate the item id
    if (itemId < 0) {
        MessageModalPopUp("Error", "Something went wrong. Please Refresh and try again");
        return;
    }
    else {
        //do nothing
    }

    //validate availability
    var selectedAvailable = $("#availability").val();
    if (selectedAvailable == "true") {
        stockItem.Available = true;
    }
    else if (selectedAvailable == "false") {
        stockItem.Available = false;
    }
    else {
        MessageModalPopUp("Error", "Please Select availablity");
        return;
    }

    //validate Retail price
    var retPrice = $("#retailPrice").val();

    if (isNaN(retPrice) || retPrice < 0) {
        MessageModalPopUp("Error","Please mention a valid retial price");
        return;
    }
    else {
        stockItem.RetailPrice = retPrice;
    }

    //if all validated then send to server
    PostToServer(stockItem);
}


function PostToServer(stockItem){
    var packet = {
        data: stockItem,
        url: "/StockManage/UpdateStock",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            OnSuccess(response, "Stock Update");
        },
        error: function (response) {
            OnError("Stock Update Failed.");
        }
    };

    $.ajax(packet);
    ResetModal();
    $("#loader").show();
}