﻿var stock = [];

//to get the stock Medicine from server and put it in stock
function InitStockFromServer() {
    var packet = {
        type: "POST",
        data: {'pharmacy':pharmacy},
        url: urlForDataGet,
        dataType: "json",
        success: function (d) {
            //display data in the data table
            PutDataInLocalObject(d);
            InitDataTable(stock);
        },
        error: function (d) {
            MessageModalPopUp("Init Error", "Server is Currenlty Down");
        }
    };

    $.ajax(packet);
}

//to put the data from server into stock object before passing to Datatable
function PutDataInLocalObject(obj) {
    for (var i in obj) {
        var stockTemp = {};
        stockTemp.ItemId=obj[i]["ItemId"];
        stockTemp.MedicineId = obj[i]["Medicine"]["MedicineId"];
        stockTemp.BrandName = obj[i]["Medicine"]["BrandName"];
        stockTemp.Manufacturer = obj[i]["Medicine"]["Manufacturer"];
        stockTemp.DrugForm = obj[i]["Medicine"]["DrugForm"];
        stockTemp.PackSize = obj[i]["Medicine"]["PackSize"];
        stockTemp.TradePrice = obj[i]["Medicine"]["TradePrice"];
        stockTemp.Salt = obj[i]["Medicine"]["Salt"];
        stockTemp.SaltAmount = obj[i]["Medicine"]["SaltAmount"];
        stockTemp.RetailPrice = obj[i]["RetailPrice"];
        stockTemp.Available = obj[i]["Availability"];

        stock.push(stockTemp);
    }
}