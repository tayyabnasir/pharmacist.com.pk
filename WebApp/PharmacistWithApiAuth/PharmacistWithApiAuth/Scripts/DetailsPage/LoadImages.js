﻿function LoadImages(medid) {
    LoadImagesForMedicineId(medid, PutImagesOnPreview, ErrorLoadingImages);
}

function PutImagesOnPreview(imgArr) {
    var container = $("#imgContainerCarousal");
    var count = 0;

    var listToAppend;
    var imgToAppend;
    for (var index in imgArr) {
        if (count == 0) {

            imgToAppend = "<div class='item active'><a class=' thumbnail'><img class='img-responsive' src='" + imgPath + imgArr[index]["url"] + "' alt='Medicnie'></a></div>";
        }
        else {
            imgToAppend = "<div class='item'><a  class='thumbnail'><img class='img-responsive' src='" + imgPath + imgArr[index]["url"] + "' alt='Medicnie'></a></div>";
        }
        count++;
        container.append(imgToAppend);
    }
    //check if no result wsa found
    if (count == 0) {
        ErrorLoadingImages();
        return;
    }

    $("#loader").hide();
}

function ErrorLoadingImages() {
    var container = $("#imgContainerCarousal");

    var imgToAppend = "<div class='item active'><a class=' thumbnail'><img class='img-responsive' src='" + imgPath + "default.png" + "' alt='Medicnie'></a></div>";
    container.append(imgToAppend);
    $("#loader").hide();
}
