﻿var offset = 0;

function GetComments() {
    var data = {
        "medid": medid,
        "start": offset
    };

    var packet = {
        data: data,
        url: "/CommentsHandler/GetNextComments",
        type: "POST",
        datatype: "JSON",
        success: function (response) {
            var commentContainer = $("#commentContainer");
            for (var com in response) {
                commentContainer.append("<div class='col-md-11'><div class='row'><div class='col-md-12'><span><strong>" + response[com]["user"]["FirstName"] + " " + response[com]["user"]["LastName"] + " </strong></span><span class='label label-info'>" + response[com]["CommentDate"] + "</span><br><p class=''>" + response[com]["Comment"] + "</p></div></div><hr /></div>");
            }
            offset += 15;
        },
        error: function () {

        }
    };

    $.ajax(packet);
}