﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permissions
{
    public class CheckPermission
    {
        public Boolean HasPermission(User dto,String perm)
        {
            perm = perm.ToLower();
            String rolePerm = dto.Permissions.Where(x => x.ToLower().Equals(perm)).FirstOrDefault();

            if(rolePerm==null || rolePerm.Equals(""))
            {
                //in case not permitted
                return false;
            }
            else
            {
                //in case has permission
                return true;
            }
        }
    }
}
