﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permissions
{
    // to check for roles of certain user
    public class CheckRole
    {
        public Boolean HasRole(User dto,String role)
        {
            role = role.ToLower();
            String userRole = dto.Roles.Where(x => x.ToLower().Equals(role)).FirstOrDefault();

            if (userRole == null || userRole.Equals(""))
            {
                //in case has no role
                return false;
            }
            else
            {
                //in case has role
                return true;
            }
        }
    }
}
