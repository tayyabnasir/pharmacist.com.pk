﻿using DAL.ConnectionManagers;
using Models.Others;
using Models.Roles;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ManufacturerTasks
{
    //to access Manaufacturer Table
    public class ManufacturerAccessDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //get manufacturer details for the userid
        //may return null so be cautious
        public Manufacturer GetManufacturerForUserId(Int32 userId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "select ManufacturerId,ManufacturerName,Website,Phone,UserId FROM dbo.Manufacturer WHERE UserId = @userId";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("userId", SqlDbType.Int, userId.ToString()));
                //now to create Select Command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }

                DataTable table = new DataTable();
                
                try
                {
                    adp.Fill(table);    
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting manufacturer object for UserId:" + userId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                if (table.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    //do nothing
                }

                Manufacturer manufacturer = Manufacturer.GetManufacturerForRecord(table.Rows[0]);

                //Dissconnect from DB
                obj.Disconnect();

                //return manufacturer's name
                return manufacturer;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        //to update a manufacturer information
        public Boolean UpdateManufacturer(Manufacturer dto)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "update dbo.Manufacturer Set ManufacturerName=@mname,Website=@site,Phone=@phone  WHERE ManufacturerId = @mid";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("mid", SqlDbType.Int, dto.ManufacturerId.ToString()));
                Params.Add(new NameTypePair("mname", SqlDbType.NVarChar, dto.ManufacturerName));
                Params.Add(new NameTypePair("site", SqlDbType.NVarChar, dto.Website));
                Params.Add(new NameTypePair("phone", SqlDbType.NVarChar, dto.Phone));

                //now to create Select Command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                
                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in updating manufacturer object for :" + dto.ManufacturerId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                

                //Dissconnect from DB
                obj.Disconnect();

                //return manufacturer's name
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
