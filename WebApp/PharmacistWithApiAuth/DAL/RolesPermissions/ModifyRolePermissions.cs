﻿using DAL.ConnectionManagers;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RolesPermissions
{
    //code reviwed and no error so far
    //to update, insert, delete roles and permissions
    public class ModifyRolePermissions
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //to put a new user in UserRoles Table
        public Boolean DefineUserRole(Int32 userId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<===========SEE

            //open connection
            if (obj.Connect())
            {
                //1. get Role id for User
                GetRolePermissionsDAL rolePerms = new GetRolePermissionsDAL();

                Int32 roleId = rolePerms.GetRoleIdForRoleName("User", obj.connection);

                if (roleId == -1)
                {
                    //in case of error return false;
                    return false;
                }
                else
                {
                    //do nothing
                }

                //2. now define the role for user based on id

                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "insert into dbo.UserRoles (UserId,RoleId) Values(@uid,@rid)";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("uid", SqlDbType.Int, userId.ToString()));
                parameters.Add(new NameTypePair("rid", SqlDbType.Int, roleId.ToString()));


                //make the select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters,obj.connection))
                {
                    //do nothing
                }
                else
                {
                    return false;
                }

                

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in defining user role for:" + userId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                
                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
