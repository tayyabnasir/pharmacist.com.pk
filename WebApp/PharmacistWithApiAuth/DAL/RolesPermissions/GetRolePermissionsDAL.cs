﻿using DAL.ConnectionManagers;
using Models.Others;
using Models.Roles;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RolesPermissions
{
    //code reviewed and no errors detected so far
    //to get roles and permissions and to init the basic dtos
    public class GetRolePermissionsDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //to get all permissions given a user Id
        //might return null so caution require
        public List<String> GetPermissionsOfUser(Int32 userId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<=======SEE

            //open connection
            if (obj.Connect())
            {
                //call the stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetPermissionsOfUser", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Userid", SqlDbType.Int).Value = userId;

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting permissions for User:" + userId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                //convert the data to List of Permissions String

                List<String> perms = User.GetPermissionsFromRecords(table.Rows);


                //Dissconnect from DB
                obj.Disconnect();
                return perms;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        //returns a role id for given role name and -1 if error or not found
        public Int32 GetRoleIdForRoleName(String roleName,SqlConnection obj)
        {
            //call the stored procedure
            SqlDataAdapter adp = new SqlDataAdapter();
            String query = "Select RoleId from dbo.Role where RoleName=@name";
            //make select command
            List<NameTypePair> parameters = new List<NameTypePair>();
            parameters.Add(new NameTypePair("name", SqlDbType.NVarChar, roleName));

            if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj))
            {
                //do nothing
            }
            else
            {
                //in case of error return -1
                return -1;
            }

            //Fill in DataTable
            DataTable table = new DataTable();

            try
            {
                adp.Fill(table);
            }
            catch (Exception ex)
            {
                //in case of error
                logger.Error("Error in getting roleid for role:" + roleName + " At: " + DateTime.Now + " Due to " + ex.Message);


                
                

                return -1;
            }

            //get role id
            if(table.Rows==null || table.Rows.Count == 0)
            {
                return -1;
            }
            else
            {
                //get and return the role id
                return (int)table.Rows[0]["RoleId"];
            }


        }



        //new added unreviewd code
        //to geta userid for that id populate the Roles and permissions for that 
        //user
        public List<String> GetRolesOfUser(Int32 userId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<=======SEE

            //open connection
            if (obj.Connect())
            {
                //call the stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetRolesOfUser", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Userid", SqlDbType.Int).Value = userId;

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting Roles for User:" + userId + " At: " + DateTime.Now + " Due to " + ex.Message);



                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                //convert the data to List of Permissions String

                List<String> perms = User.GetRolesFromRecords(table.Rows);


                //Dissconnect from DB
                obj.Disconnect();
                return perms;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }
    }
}
