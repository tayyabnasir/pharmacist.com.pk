﻿using DAL.ConnectionManagers;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.WePageManager
{
    //to provide functions to access and edit wepage table
    public class WebPageHandlerDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        //get web page for given userid
        //may return null
        public WebPage GetPageForUserId(Int32 userId)
        {
            //must be some specific handler <<<<<<==================SEE
            DBHandlerUser obj = new DBHandlerUser();

            //try to connect
            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //see if user exists
                String query = "Select * from dbo.Webpage where UserId=@uid";

                //define parameters of query
                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("uid", SqlDbType.Int, userId.ToString()));
                

                //make the select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {//try to get the valid user row(s)
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error Retrieving WebPage for: " + userId + " At: " + DateTime.Now + " Due to: " + ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }
                    else
                    {
                        //do nothing
                    }

                    //put the user record in User object
                    WebPage page = WebPage.GetWebPageForRecord(table.Rows[0]);


                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return page;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return null;
                }

            }
            else
            {
                //error in connection
                return null;
            }

        }


        //puts the provided web page html for the given userid
        //may return null
        public Boolean UpdateHTMLForUser(String html,Int32 userId)
        {
            //must be some specific handler <<<<<<==================SEE
            DBHandlerUser obj = new DBHandlerUser();

            //try to connect
            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //see if user exists
                String query = "update dbo.Webpage Set PageHtml=@html where UserId=@uid";

                //define parameters of query
                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("uid", SqlDbType.Int, userId.ToString()));

                parameters.Add(new NameTypePair("html", SqlDbType.NVarChar, html));


                //make the select command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    
                    try
                    {//try to get the valid user row(s)
                        adp.UpdateCommand.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error Updating WebPage for: " + userId + " At: " + DateTime.Now + " Due to: " + ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return false; ;
                    }
                    

                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return true;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return false;
                }

            }
            else
            {
                //error in connection
                return false;
            }

        }


        //for provided manufacturer name returns the html of its webpage
        //may return null
        public String GetPageForManufacturer(String name,String role)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //stored procedure
                SqlDataAdapter adp = null;

                if (role.Equals("Pharmacy"))
                {
                    adp = new SqlDataAdapter("dbo.GetWebPageForPharmacy", obj.connection);
                }
                else if(role.Equals("Manufacturer"))
                {
                    adp = new SqlDataAdapter("dbo.GetWebPageForManufacturerName", obj.connection);
                }
                else
                {
                    obj.Disconnect();
                    return null;
                }

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = name;
                

                //creating datatable to hold up the result set of SP
                DataTable table = new DataTable();

                //finally calling the stored procedure
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting WebPage Name for :" + name + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                String html = "";
                if(table.Rows==null || table.Rows.Count == 0)
                {
                    html = "";
                }
                else
                {
                    html = table.Rows[0]["PageHTML"].ToString();
                }

                //disconnect DB success case for executed query
                obj.Disconnect();
                return html;
            }
            else
            {
                //in case of error connecting to Db return -1
                return null;
            }
        }
    }
}
