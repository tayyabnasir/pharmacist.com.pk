﻿using DAL.ConnectionManagers;
using Models.MedicineRecord;
using Models.Others;
using Models.Stock;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.StockAccessDAL
{
    public class GetStockDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //for a given pharmacyId get all matching records for that pharmacy
        //may return null so be cautious
        public List<Stock> GetStockForPharmacy(Int32 pharmacyId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("pharmacyid", SqlDbType.Int, pharmacyId.ToString()));

                String query = "Select * from dbo.Stock where pharmacyid=@pharmacyid  and IsActive=1";

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return null;
                }

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch(Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting stock for pharmacy:" + pharmacyId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                //convert the data to List of Stock

                List<Stock> stock = Stock.GetStockListFromDataRows(table.Rows);

                
                //Dissconnect from DB
                obj.Disconnect();
                return stock;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }
        
        //for given medicine id and pharmacy id checks if a same record is present already
        //return 1 if exist, 0 if not exit, -1 if error
        public Int32 CheckForStockEntry(Int32 medicineId,Int32 pharmacyId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                List<NameTypePair> parameters = new List<NameTypePair>();

                String query = "Select * from dbo.Stock where pharmacyid=@pharmacyid and medicineId=@medid  and IsActive=1";

                parameters.Add(new NameTypePair("pharmacyid", SqlDbType.Int, pharmacyId.ToString()));
                parameters.Add(new NameTypePair("medid", SqlDbType.Int, medicineId.ToString()));

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return -1;
                }

                SqlDataReader reader = null;

                try
                {
                    reader=adp.SelectCommand.ExecuteReader();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in checking stock entry for pharmacy:" + pharmacyId + " and medid: " +medicineId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return -1;
                }

                if (reader == null)
                {
                    //error
                    return -1;
                }
                else
                {
                    //do nothing
                }

                if (reader.Read())
                {
                    //record found
                }
                else
                {
                    //no id found 
                    //disconnect DB
                    obj.Disconnect();
                    return 0;
                }

                
                //Dissconnect from DB
                obj.Disconnect();
                return 1;
            }
            else
            {
                //in case of error connecting to Db return Null
                return -1;
            }
        }

        //to return a list of all pharmacies having a certain medicine in their stock
        //may return null so be cautious
        public List<MedicineContainingPharmacies> GetPharmaciesHavingMedicine(Int32 medid)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //call the stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetPharmacyHavingMedicine",obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Medid", SqlDbType.Int).Value = medid;

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting pharmacy for medicine:" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);


                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                //convert the data to List of Stock

                List<MedicineContainingPharmacies> stock = MedicineContainingPharmacies.GetRecordsFromDataTable(table.Rows);


                //Dissconnect from DB
                obj.Disconnect();
                return stock;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }
     
        
        //to return rows containg Stock Medicine for the provided Pharmacy name
        public DataTable GetMedicineStockForPharmacyName(String pharmacy)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //call the stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("GetMedicineStockForPharmacy", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@PharmacyName", SqlDbType.NVarChar).Value = pharmacy;

                //Fill in DataTable
                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting pharmacy Medicine Stock Pair for pharmacy:" + pharmacy + " At: " + DateTime.Now + " Due to " + ex.Message);



                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                
                //Dissconnect from DB
                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }


        //to return pharmacy id for given itemid
        public Int32 GetOwnerOfItem(Int32 itemId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                List<NameTypePair> parameters = new List<NameTypePair>();

                String query = "Select pharmacyId from dbo.Stock where itemId=@item  and IsActive=1";

                parameters.Add(new NameTypePair("item", SqlDbType.Int, itemId.ToString()));

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return -1;
                }

                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting pharmacyid entry for item:" + itemId + " and  At: " + DateTime.Now + " Due to " + ex.Message);



                    //Dissconnect from DB
                    obj.Disconnect();
                    return -1;
                }

                int pharmId = -1;

                if(table==null || table.Rows.Count == 0)
                {
                    obj.Disconnect();
                    return -1;
                }
                else
                {
                    //do nothing
                }

                pharmId = (int)table.Rows[0]["pharmacyId"];

                //Dissconnect from DB
                obj.Disconnect();
                return pharmId;
            }
            else
            {
                //in case of error connecting to Db return Null
                return -1;
            }
        }
    }
}
