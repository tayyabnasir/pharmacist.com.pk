﻿using DAL.ConnectionManagers;
using Models.Others;
using Models.Stock;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.StockAccessDAL
{
    public class ModifyStockDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //responsible for adding a new medicine in the given pharmacy's stock
        public Boolean AddMedicineToStock(Stock stockObj)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerPharmacy obj = new DBHandlerPharmacy();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "insert into dbo.stock (MedicineId,RetailPrice,PharmacyId) values(@medid,@retailprice,@pharmaid)";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("medid", SqlDbType.Int, stockObj.MedicineId.ToString()));
                parameters.Add(new NameTypePair("retailprice", SqlDbType.Float, stockObj.RetailPrice.ToString()));
                parameters.Add(new NameTypePair("pharmaid", SqlDbType.Int, stockObj.PharmacyId.ToString()));

                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in adding medicine to stock for :" + stockObj.ItemId + " At: " + DateTime.Now + " Due to " + ex.Message);

                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }


                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }

        //responsible for updating a medicine in the given pharmacy's stock
        public Boolean UpdateMedicineInStock(Stock stockObj)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerPharmacy obj = new DBHandlerPharmacy();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "update dbo.stock Set RetailPrice=@retailprice,Available=@available where itemid=@itemid";

                List<NameTypePair> parameters = new List<NameTypePair>();

                
                parameters.Add(new NameTypePair("retailprice", SqlDbType.Float, stockObj.RetailPrice.ToString()));
                parameters.Add(new NameTypePair("available", SqlDbType.Bit, stockObj.Available.ToString()));
                parameters.Add(new NameTypePair("itemid", SqlDbType.Int, stockObj.ItemId.ToString()));

                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in updating medicine for :" + stockObj.ItemId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }

        //resposnible to remove an item from stock
        public Boolean RemoveFromStock(Int32 ItemId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "UPDATE dbo.Stock SET isactive=0 WHERE ItemId = @item";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("item", SqlDbType.Int, ItemId.ToString()));


                //now to create Select Command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                //now update the record

                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in deactivating stock item  for:" + ItemId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
