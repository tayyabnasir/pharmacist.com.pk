﻿using DAL.ConnectionManagers;
using Models.MedicineRecord;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.MedicineAccessDAL
{
    public class MedicineImagesAccess
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public List<MedicineImages> GetImagesForMedicineId(Int32 medid)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                List<MedicineImages> images = null;
                //if brandname was valid return salt

                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "select * from dbo.MedicineImages where MedicineId=@medid";

                //make listof all parameters to pass to query
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("medid", SqlDbType.Int, medid.ToString()));

                //now to create Select Command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }


                //get data and Load in datatable
                DataTable table = new DataTable();


                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting images for medid:" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }


                //now fill in the images list from the DataTable
                images = MedicineImages.GetMedImagesForRecord(table.Rows);


                //disconnect DB success case for executed query
                obj.Disconnect();
                return images;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        public String DeleteMedicineImage(MedicineImages dto)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "Delete from dbo.MedicineImages OUTPUT deleted.Url WHERE id = @id";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("id", SqlDbType.BigInt, dto.id.ToString()));



                //now to create Select Command
                if (DALCommonCode.MakeDeleteCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }

                //now update the record
                String url = null;
                try
                {
                    url=adp.DeleteCommand.ExecuteScalar().ToString();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in deleting medicine image for:" + dto.medicineId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return url;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        public Boolean AddMedicineImage(MedicineImages dto)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "insert into dbo.MedicineImages (MedicineId,Url) Values (@medid,@url)";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("medid", SqlDbType.Int, dto.medicineId.ToString()));
                Params.Add(new NameTypePair("url", SqlDbType.NVarChar, dto.url.ToString()));



                //now to create Select Command
                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                //now update the record

                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in adding medicine image for:" + dto.medicineId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
