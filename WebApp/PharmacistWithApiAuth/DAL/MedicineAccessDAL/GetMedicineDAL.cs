﻿using DAL.ConnectionManagers;
using Models;
using Models.MedicineRecord;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.MedicineAccessDAL
{
    public class GetMedicineDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //responsible to return Medicine id for provided brand Name
        //check for -1 on error
        public Int32 GetMedicineIdForBrandName(String BrandName)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //query to get MedId for brandname
                String query = "select MedicineId from dbo.Medicine where brandname=@brand  and IsActive=1";

                //creating parametrized query
                SqlCommand cmd = new SqlCommand(query, obj.connection);
                cmd.Parameters.AddWithValue("brand", BrandName);
                SqlDataReader reader = null;

                //check for error in executing query
                try
                {
                    reader = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    //Make Error Log Entry for Read Error
                    logger.Error("Error in getting medicineId for brand:" + BrandName + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //disconnect DB
                    obj.Disconnect();
                    return -1;
                }

                Int32 medid = -1;

                if (reader.Read())
                {
                    medid = reader.GetInt32(0);
                }
                else
                {
                    //no id found for brandname
                    //disconnect DB
                    obj.Disconnect();
                    return -1;
                }



                //disconnect DB success case for executed query
                obj.Disconnect();
                return medid;
            }
            else
            {
                //in case of error connecting to Db return -1
                return -1;
            }
        }

        //responsible to get salt from BrandName
        //caller must consider null returned by this function
        public List<Salt> GetSaltForMedicineId(Int32 medid)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                List<Salt> salt = null;
                //if brandname was valid return salt

                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "select * from dbo.Salt where MedicineId=@medid";

                //make listof all parameters to pass to query
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("medid", SqlDbType.Int, medid.ToString()));

                //now to create Select Command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }


                //get data and Losd in datatable
                DataTable table = new DataTable();


                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting salt for medid:" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);

                    
                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }


                //now fill in the salt list from the DataTable
                salt = Salt.ConvertSaltTableToList(table);


                //disconnect DB success case for executed query
                obj.Disconnect();
                return salt;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        //responsible to get a list of SaltNames and return all the matching 
        //medicine record against it by invoking a stored procedure
        //caution may return null so handle null condition
        public DataTable GetMedicinesForSalt(List<String> salts)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make a call to dynamic where clause generator
                //it will return where clause string along wiht saltCount
                //in WhereClause Object
                WhereClauseDTO whereClause = WhereClauseDTO.getWhereClause(salts);

                //first build up the necessary objects required to call the
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetMedsForSalt", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Where", SqlDbType.NVarChar).Value = whereClause.where;
                adp.SelectCommand.Parameters.Add("@SaltCount", SqlDbType.Int).Value = whereClause.saltCount;

                //creating datatable to hold up the result set of SP
                DataTable table = new DataTable();

                //finally calling the stored procedure
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting medicine for salt:" + salts.ToString() + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }


                //disconnect DB success case for executed query
                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of error connecting to Db return -1
                return null;
            }
        }

        //resposible to get a medical record for provided medid
        public DataTable GetMedicineForMedId(Int32 medid)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //1. Get All the matching records from Medicine Table
                //and joined Salt Table

                //if successfull connection then use adapter to update required data

                //first build up the necessary objects required to call the
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetMedicineForMedicineId", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;

                //adding the parameters
                adp.SelectCommand.Parameters.Add("@MedId", SqlDbType.Int).Value = medid;

                DataTable table = new DataTable();
                //now put data in datatable and return it
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting medicine for medid:" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);
                    

                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }


        //to get Medicine Records for simillar brand names
        public DataTable GetSimillarBrandNameMedicines(String brandName)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //1. Get All the matching records from Medicine Table
                //and joined Salt Table

                //if successfull connection then use adapter to update required data

                //first build up the necessary objects required to call the
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetSimillarBrandName", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;

                //get the Like string 
                String like = DALCommonCode.GetStringParsedWithPercents(brandName);

                //adding the parameters
                adp.SelectCommand.Parameters.Add("@LikeStatement", SqlDbType.NVarChar).Value = like;

                DataTable table = new DataTable();
                //now put data in datatable and return it
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting Simillar Brand Name for brandName:" + brandName + " and generated like:" + like + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

    }
}
