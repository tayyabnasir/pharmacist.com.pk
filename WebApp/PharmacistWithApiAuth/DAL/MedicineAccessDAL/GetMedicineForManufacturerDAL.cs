﻿using DAL.ConnectionManagers;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.MedicineAccessDAL
{
    public class GetMedicineForManufacturerDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //get all medicine record for a certain manufacturer
        //caution null may be returned so check for it
        public DataTable GetMedicalRecords(String Manufacturer)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //1. Get All the matching records from Medicine Table
                //and joined Salt Table

                //if successfull connection then use adapter to update required data

                //first build up the necessary objects required to call the
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetMedicineForManufacturer", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;

                //adding the parameters
                adp.SelectCommand.Parameters.Add("@Manufacturer", SqlDbType.NVarChar).Value = Manufacturer;

                DataTable table = new DataTable();
                //now put data in datatable and return it
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting medicine for manufacturer:" + Manufacturer + " At: " + DateTime.Now + " Due to " + ex.Message);
                    

                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }

        //caution null returner
        //get manufacturerid and name for provided medid
        public String GetMedicineManufacturer(Int32 medid)

        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "select Manufacturer FROM dbo.Medicine WHERE MedicineId = @medId";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("medid", SqlDbType.Int, medid.ToString()));
                //now to create Select Command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }

                //now update the record
                String manufacturer = null;
                try
                {
                    SqlDataReader reader = adp.SelectCommand.ExecuteReader();
                    //now if no row retunred then return null
                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    //if their are rows than read the first one (as there wil always be on row returned)
                    reader.Read();
                    //get manufacturer name
                    manufacturer = reader.GetString(reader.GetOrdinal("Manufacturer"));
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting manufacturer for medid:" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }



                //Dissconnect from DB
                obj.Disconnect();

                //return manufacturer's name
                return manufacturer;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }


        
    }
}
