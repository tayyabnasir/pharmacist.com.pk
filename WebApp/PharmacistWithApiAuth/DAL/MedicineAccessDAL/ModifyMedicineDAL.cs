﻿using DAL.ConnectionManagers;
using Models;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.MedicineAccessDAL
{
    public class ModifyMedicineDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        /// <start>
        /// Helper Functions for Inserting
        public Boolean InsertSaltRecord(SqlDataAdapter adp, SqlConnection conn, Medicine med)
        {
            //making a parametrized insert command for adapter
            String query = "INSERT INTO dbo.Salt (MedicineId, SaltName, Amount, Unit) Values(@medid, @saltName, @amount, @unit)";

            foreach (Salt saltObj in med.Salt)
            {
                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("saltName", SqlDbType.NVarChar, saltObj.SaltName));
                Params.Add(new NameTypePair("medId", SqlDbType.Int, med.MedicineId.ToString()));
                Params.Add(new NameTypePair("amount", SqlDbType.Float, saltObj.Amount.ToString()));
                Params.Add(new NameTypePair("unit", SqlDbType.NVarChar, saltObj.Unit));


                //now to create Select Command
                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, Params, conn))
                {
                    //do nothing
                }
                else
                {
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                //now update the record

                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Fatal("Error in inserting salt for :" + med.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);

                    
                    return false;
                }
            }
            return true;
        }

        public Int32 InsertMedicineRecord(SqlDataAdapter adp, SqlConnection conn, Medicine med)
        {
            //making a parametrized insert command for adapter
            String query = "INSERT INTO dbo.Medicine(BrandName, Manufacturer, DrugForm, TotalUnitsPerPack, TradePrice, PricePerUnit, PackSize, SaltCount) Output inserted.MedicineId VALUES(@brandName, @manufacturer, @form, @units, @tradePrice, @pricePerUnit, @packaging, @saltCount)";



            //make a list of all parameters to be passed to command
            List<NameTypePair> Params = new List<NameTypePair>();
            Params.Add(new NameTypePair("brandName", SqlDbType.NVarChar, med.BrandName));
            Params.Add(new NameTypePair("manufacturer", SqlDbType.NVarChar, med.Manufacturer));
            Params.Add(new NameTypePair("form", SqlDbType.NVarChar, med.DrugForm));
            Params.Add(new NameTypePair("units", SqlDbType.Int, med.TotalUnitsPerPack.ToString()));
            Params.Add(new NameTypePair("tradePrice", SqlDbType.Float, med.TradePrice.ToString()));
            Params.Add(new NameTypePair("pricePerUnit", SqlDbType.Float, med.PricePerUnit.ToString()));
            Params.Add(new NameTypePair("saltCount", SqlDbType.Int, med.SaltCount.ToString()));
            Params.Add(new NameTypePair("packaging", SqlDbType.NVarChar, med.PackSize));




            //now to create insert Command
            if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, Params, conn))
            {
                //do nothing
            }
            else
            {
                //if fail due to wrong parameters in list return null
                return -1;
            }

            //now update the record
            int medid = -1;
            try
            {
                medid = (Int32)adp.InsertCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                logger.Fatal("Error in inserting medicine record for:" + med.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);

                return -1;
            }

            return medid;
        }
        /// </end>
        /// 
        /// Insert Function
        //responsible to update a record of medicine data
        public Boolean InsertRecord(Medicine med)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                int medid = InsertMedicineRecord(adp, obj.connection, med);
                //first insert the MedicinePart
                if (medid!=-1)
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                //to insert salt for the inserted medicine
                med.MedicineId = medid;

                //next insert the salt part
                if (InsertSaltRecord(adp, obj.connection, med))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                
                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }


        /// <start>
        /// Helper Functions for MedicineUpdate
        public Boolean UpdateMedincePart(SqlConnection conn, SqlDataAdapter adp, Medicine med)
        {
            //making a parametrized select command for adapter
            String query = "UPDATE dbo.Medicine SET BrandName = @brandName, DrugForm = @form, TotalUnitsPerPack = @units, TradePrice = @tradePrice, PricePerUnit = @pricePerUnit, PackSize=@packaging, SaltCount=@saltCount WHERE MedicineId = @medId";
            

            //make a list of all parameters to be passed to command
            List<NameTypePair> Params = new List<NameTypePair>();
            Params.Add(new NameTypePair("brandName", SqlDbType.NVarChar, med.BrandName));
            Params.Add(new NameTypePair("form", SqlDbType.NVarChar, med.DrugForm));
            Params.Add(new NameTypePair("saltCount", SqlDbType.Int, med.SaltCount.ToString()));
            Params.Add(new NameTypePair("units", SqlDbType.Int, med.TotalUnitsPerPack.ToString()));
            Params.Add(new NameTypePair("tradePrice", SqlDbType.Float, med.TradePrice.ToString()));
            Params.Add(new NameTypePair("pricePerUnit", SqlDbType.Float, med.PricePerUnit.ToString()));
            Params.Add(new NameTypePair("medId", SqlDbType.Int, med.MedicineId.ToString()));
            Params.Add(new NameTypePair("packaging", SqlDbType.NVarChar, med.PackSize));




            //now to create update Command
            if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, conn))
            {
                //do nothing
            }
            else
            {
                //if fail due to wrong parameters in list return null
                return false;
            }

            //now update the record

            try
            {
                adp.UpdateCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                logger.Error("Error in updating medicine part for:" + med.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);

                return false;
            }

            return true;
        }

        public Boolean UpdateSaltPart(SqlConnection conn, SqlDataAdapter adp, Medicine med)
        {
            //1. remove all salts for current medicine
            String queryRemove = "Delete from dbo.Salt where MedicineId=@medid";

            adp.DeleteCommand = new SqlCommand(queryRemove, conn);
            adp.DeleteCommand.Parameters.AddWithValue("medid", med.MedicineId);

            try
            {
                adp.DeleteCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //Make Error Log Entry for Deletion entry Error
                logger.Error("Error in updating salt part for:" + med.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);

                return false;
            }

            //2. now insert all the updated/added/removed salt for the medicine

            if (!InsertSaltRecord(adp, conn, med))
            {
                return false;
            }


            return true;
        }
        /// </end>
        /// Insert function itself
        //responsible to update a record of medicine data
        public Boolean UpdateRecord(Medicine med)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                if (UpdateMedincePart(obj.connection, adp, med))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                if (UpdateSaltPart(obj.connection, adp, med))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }


        //deactivate a medicine record
        public Boolean DeactiveRecord(Medicine med)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "UPDATE dbo.Medicine SET isactive=0 WHERE MedicineId = @medId";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("medId", SqlDbType.Int, med.MedicineId.ToString()));


                //now to create Select Command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                //now update the record

                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in deactivating medicine  for:" + med.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);
                    

                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }

    }
}
