﻿using DAL.ConnectionManagers;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.MedicineAccessDAL
{
    //Code Reviewed and no errors so far

    //provide functions for search recommendations
    public class SearchHelpDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //to create a string interleaved with % sign for like clause
        private String MakeLikeStatement(String name)
        {
            if (name == null)
            {
                return "";
            }
            String result = "";
            for (int i= 0;i < name.Length; i++)
            {
                result += name[i] + "%";
            }

            return result;
        }

        //get recommendations for provided part of salt
        //may return null so be cautious
        public DataTable GetRecommendationsForSalt(String salt)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<==============SEE
            //try to connect to DB
            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //make the query to get 10 matching records
                String query = "Select distinct top 10 SaltName from dbo.Salt where SaltName like @pattern";

                List<NameTypePair> parameters = new List<NameTypePair>();
                parameters.Add(new NameTypePair("pattern", SqlDbType.NVarChar, MakeLikeStatement(salt)));

                //make select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of error 
                    obj.Disconnect();
                    return null;
                }

                DataTable table = new DataTable();

                //exceute query
                try
                {
                    adp.Fill(table);
                }
                catch(Exception ex)
                {
                    logger.Error("Error in getting recommendations for salt:"+ salt +" At: " + DateTime.Now + " Due to " + ex.Message);
                    //incase of error
                    obj.Disconnect();
                    return null;
                }

                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of no connection
                return null;
            }
        }

        //get recommendations for provided part of brand
        public DataTable GetRecommendationsForBrand(String brand)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<<<<<<<<<<========SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //make the query
                String query = "Select distinct top 10 BrandName from dbo.Medicine where BrandName like @pattern";

                List<NameTypePair> parameters = new List<NameTypePair>();
                parameters.Add(new NameTypePair("pattern", SqlDbType.NVarChar, MakeLikeStatement(brand)));

                //make select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of error 
                    obj.Disconnect();
                    return null;
                }

                DataTable table = new DataTable();

                //exceute query
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting recommendations for brand:" + brand + " At: " + DateTime.Now + " Due to " + ex.Message);

                    //incase of error
                    obj.Disconnect();
                    return null;
                }

                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of no connection
                return null;
            }
        }


        //get recommendations for provided part of manufacturer
        //may return null so be cautious
        public DataTable GetRecommendationsForManufacturer(String manufacturer)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<==============SEE
            //try to connect to DB
            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //make the query to get 10 matching records
                String query = "Select distinct top 10 Manufacturer from dbo.Medicine where Manufacturer like @pattern";

                List<NameTypePair> parameters = new List<NameTypePair>();
                parameters.Add(new NameTypePair("pattern", SqlDbType.NVarChar, MakeLikeStatement(manufacturer)));

                //make select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of error 
                    obj.Disconnect();
                    return null;
                }

                DataTable table = new DataTable();

                //exceute query
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting recommendations for Manufacturer:" + manufacturer + " At: " + DateTime.Now + " Due to " + ex.Message);
                    //incase of error
                    obj.Disconnect();
                    return null;
                }

                obj.Disconnect();
                return table;
            }
            else
            {
                //in case of no connection
                return null;
            }
        }
    }
}
