﻿using DAL.ConnectionManagers;
using Models.AccountManagement;
using Models.Others;
using Models.Roles;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountsManagerDAL
{
    //Reviewed and no error so far
    //to get data from users tables
    public class AccountDataAccess
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //reviewed

        //check if provided login credential exist and if yes return the user object for the user
        //may return null so be cautious
        public User IsLoginValid(LoginDTO login)
        {
            //must be some specific handler <<<<<<==================SEE
            DBHandlerUser obj = new DBHandlerUser();

            //try to connect
            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //see if user exists
                String query = "Select UserId,FirstName,LastName,Login,Password,ContactEmail,IsVerified from dbo.Users where Login=@login and Password=@password and IsActive=1";

                //define parameters of query
                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("login", SqlDbType.NVarChar, login.Login));
                parameters.Add(new NameTypePair("password", SqlDbType.NVarChar, login.Password));
                
                //make the select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {//try to get the valid user row(s)
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error at Checking for Login for: " + login.Login + " At: " + DateTime.Now + " Due to: " + ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }
                    else
                    {
                        //do nothing
                    }

                    //put the user record in User object
                    User user = User.GetUserFromRecord(table.Rows[0]);


                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return user;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return null;
                }

            }
            else
            {
                //error in connection
                return null;
            }



        }


        //check if an email is already registered as a login
        private Boolean IsExistingEmail(String email, DBHandlerUser handle)
        {
            //1. check if email exist
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = new SqlCommand("select login from dbo.Users where login=@email", handle.connection);

            adp.SelectCommand.Parameters.AddWithValue("@email", email);

            SqlDataReader reader = null;


            //check for error in executing query
            try
            {
                reader = adp.SelectCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                // Make Error Log Entry for Read Error
                logger.Error("Error in Checking for existing email for: " + email + " At: " + DateTime.Now + " Due to: " + ex.Message);

                //in case of error return true as it is not known if email exist
                return true;
            }

            //check if a row was returned
            if (reader==null || reader.Read())
            {
                reader.Close();
                return true;
            }
            else
            {//in case of no read means no row
                reader.Close();
                return false;
            }
        }

        //check if user email exist
        public Boolean IsExistingEmail(String email)
        {
            DBHandlerUser handle = new DBHandlerUser();//<<<<<<==========SEE
            if (!handle.Connect())
            {
                //as it is not known if email exist
                return true;
            }
            else
            {
                //do nothing
            }

            if (IsExistingEmail(email, handle))
            {
                //in case email exists
                handle.Disconnect();
                return true;
            }
            else
            {
                //in case email does not exist
                handle.Disconnect();
                return false;
            }
        }


        //adds a new normal user
        public Boolean AddNewUser(User user)
        {
            //1. connect with the Database
            DBHandlerUser handler = new DBHandlerUser();//<<<<<<=======SEE

            if (!handler.Connect())
            {
                //in case of connection error
                return false;
            }
            else
            {
                //do nothing
            }

            //2. check if email is unique
            if (IsExistingEmail(user.Login, handler))
            {
                //in case email exist disconnect and return
                handler.Disconnect();

                return false;
            }
            else
            {
                //do nothing
            }

            //3. Insert the record
            SqlDataAdapter adp = new SqlDataAdapter();

            String query = "insert into dbo.Users (FirstName,LastName,Login,Password,ContactEmail,IsVerified) Values(@fname,@lname,@login,@password,@email,@verified)";

            List<NameTypePair> parameters = new List<NameTypePair>();

            parameters.Add(new NameTypePair("fname", SqlDbType.NVarChar, user.FirstName));
            parameters.Add(new NameTypePair("lname", SqlDbType.NVarChar, user.LastName));
            parameters.Add(new NameTypePair("login", SqlDbType.NVarChar, user.Login));
            parameters.Add(new NameTypePair("password", SqlDbType.NVarChar, user.Password));
            parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, user.Email));
            parameters.Add(new NameTypePair("verified", SqlDbType.Bit, user.IsVerified.ToString()));



            if (DALCommonCode.MakeInsertCommandForAdapter(adp, query,parameters, handler.connection))
            {
                //do nothing
            }
            else
            {
                //in case of error
                handler.Disconnect();
                return false;
            }
            

            //4. execute query
            try
            {
                adp.InsertCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //in case of any error
                // Make Error Log Entry for Read Error
                logger.Error("Error at adding new user at: " + DateTime.Now + " Due to :" + ex.Message);
                handler.Disconnect();

                return false;
            }


            //in case of successfully adding user
            handler.Disconnect();
            return true;
        }


        //get User id for provided email
        //returns -1 if id not found or error
        public Int32 GetUserIdForEmail(String email)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<=========SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "Select UserId from dbo.Users where Login=@email and IsActive=1";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, email));
                

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error at Getting userId for email: "+email+" At:  "+DateTime.Now+" Due to: "+ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return -1;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return -1;
                    }
                    else
                    {
                        //do nothing
                    }

                    Int32 userId = (int)table.Rows[0]["UserId"];


                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return userId;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return -1;
                }

            }
            else
            {
                //error in connection
                return -1;
            }

        }

        //to verify a user
        public Boolean VerifyUser(String email)
        {
            //1. connect with the Database
            DBHandlerUser handler = new DBHandlerUser();//<<============SEE

            if (!handler.Connect())
            {
                //in case of connection error
                return false;
            }
            else
            {
                //do nothing
            }

            

            //2. update the record in Users
            SqlDataAdapter adp = new SqlDataAdapter();

            String query = "update dbo.Users set IsVerified=1 where Login=@email";

            List<NameTypePair> parameters = new List<NameTypePair>();

            parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, email));
            

            if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, handler.connection))
            {
                //do nothing
            }
            else
            {
                //in case of error
                handler.Disconnect();
                return false;
            }


            //3. execute query
            try
            {
                adp.UpdateCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //in case of any error
                // Make Error Log Entry for Read Error
                logger.Error("Error at Verifying user for Email: " + email + " At: " + DateTime.Now + " Due to: " + ex.Message);

                handler.Disconnect();

                return false;
            }


            //in case of successfully adding user
            handler.Disconnect();
            return true;
        }


        //ge the User for provided Login
        //may return null so be cautious
        public User GetUserForLogin(String login)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<<<<<<<<============SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "Select UserId,FirstName,LastName,Login,Password,ContactEmail,IsVerified from dbo.Users where Login=@login and IsActive=1";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("login", SqlDbType.NVarChar, login));
                

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error at getting user for login: " + login + " At: " + DateTime.Now + " Due to: " + ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }
                    else
                    {
                        //do nothing
                    }

                    User user = User.GetUserFromRecord(table.Rows[0]);


                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return user;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return null;
                }

            }
            else
            {
                //error in connection
                return null;
            }



        }


        //to update user info
        public Boolean UpdateUserInfo(UserSettingsDTO user)
        {
            //1. connect with the Database
            DBHandlerUser handler = new DBHandlerUser();//<<<<<<=======SEE

            if (!handler.Connect())
            {
                //in case of connection error
                return false;
            }
            else
            {
                //do nothing
            }

            

            //2. Insert the record
            SqlDataAdapter adp = new SqlDataAdapter();

            String query = "update dbo.Users SET FirstName=@fname,LastName=@lname where login=@login";

            List<NameTypePair> parameters = new List<NameTypePair>();

            parameters.Add(new NameTypePair("fname", SqlDbType.NVarChar, user.FirstName));
            parameters.Add(new NameTypePair("lname", SqlDbType.NVarChar, user.LastName));
            parameters.Add(new NameTypePair("login", SqlDbType.NVarChar, user.Email));
            


            if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, handler.connection))
            {
                //do nothing
            }
            else
            {
                //in case of error
                handler.Disconnect();
                return false;
            }


            //4. execute query
            try
            {
                adp.UpdateCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //in case of any error
                // Make Error Log Entry for Read Error
                logger.Error("Error at updating user at: " + DateTime.Now + " Due to :" + ex.Message);
                handler.Disconnect();

                return false;
            }


            //in case of successfully adding user
            handler.Disconnect();
            return true;
        }

        public Boolean UpdateUserPassword(LoginDTO user)
        {
            //1. connect with the Database
            DBHandlerUser handler = new DBHandlerUser();//<<<<<<=======SEE

            if (!handler.Connect())
            {
                //in case of connection error
                return false;
            }
            else
            {
                //do nothing
            }



            //2. Insert the record
            SqlDataAdapter adp = new SqlDataAdapter();

            String query = "update dbo.Users SET Password=@pass where login=@login";

            List<NameTypePair> parameters = new List<NameTypePair>();

            parameters.Add(new NameTypePair("pass", SqlDbType.NVarChar, user.Password));
            parameters.Add(new NameTypePair("login", SqlDbType.NVarChar, user.Login));



            if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, handler.connection))
            {
                //do nothing
            }
            else
            {
                //in case of error
                handler.Disconnect();
                return false;
            }


            //4. execute query
            try
            {
                adp.UpdateCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //in case of any error
                // Make Error Log Entry for Read Error
                logger.Error("Error at updating password user at: " + DateTime.Now + " Due to :" + ex.Message);
                handler.Disconnect();

                return false;
            }


            //in case of successfully adding user
            handler.Disconnect();
            return true;
        }
    }
}
