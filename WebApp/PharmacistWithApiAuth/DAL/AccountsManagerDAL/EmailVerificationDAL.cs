﻿using DAL.ConnectionManagers;
using Models.AccountManagement;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountsManagerDAL
{
    //Reviewed and no error so far

    //operations related to email verification
    public class EmailVerificationDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //to place an entery in the email verification table
        public Boolean PutUpAnEnteryInVerificationTable(EmailVerificationDTO dto)
        {
            //connect with the Database
            DBHandlerUser handler = new DBHandlerUser();//<<<<<<<<<==========SEE

            if (!handler.Connect())
            {
                //in case of connection error
                return false;
            }
            else
            {
                //do nothing
            }

            
            //Insert the record
            SqlDataAdapter adp = new SqlDataAdapter();

            //make a parametrized query
            String query = "insert into dbo.EmailVerify (Email,Token) Values(@email,@token)";

            List<NameTypePair> parameters = new List<NameTypePair>();

            parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, dto.Email));
            parameters.Add(new NameTypePair("token", SqlDbType.NVarChar, dto.Token));
            


            //try to make insert command
            if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, parameters, handler.connection))
            {
                //do nothing
            }
            else
            {
                //in case of error
                handler.Disconnect();
                return false;
            }


            //4. execute query
            try
            {
                adp.InsertCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //in case of any error
                // Make Error Log Entry for Read Error
                logger.Error("Error in putting entry in verification table for Email: " + dto.Email + " At: " + DateTime.Now + " because of " + ex.Message);

                handler.Disconnect();

                return false;
            }


            //in case of successfully adding user
            handler.Disconnect();
            return true;
        }

        //to get an entry from email verification table for provided email
        //may return null so be cautious
        public EmailVerificationDTO GetEmailToken(String Email)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<<<<<<<===========SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "Select Id,Email,Token from dbo.EmailVerify where Email=@email";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, Email));

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error at Getting email token for :"+Email+" At: "+DateTime.Now+" Due to: "+ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return null;
                    }
                    else
                    {
                        //do nothing
                    }

                    EmailVerificationDTO dto = EmailVerificationDTO.GetEmailVerificationDTOFromRecord(table.Rows[0]);


                    //now get the User object from the dataTable
                    //disconnect DB
                    obj.Disconnect();
                    return dto;
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return null;
                }

            }
            else
            {
                //error in connection
                return null;
            }
        }


        //to check if a given emailverificationdto exist in table
        public Boolean CheckIfTokenEmailPairExist(EmailVerificationDTO dto)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<============SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                //make parmetrized query
                String query = "Select Id,Email,Token from dbo.EmailVerify where Email=@email and Token=@token";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, dto.Email));
                parameters.Add(new NameTypePair("token", SqlDbType.NVarChar, dto.Token));
                //make select command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    DataTable table = new DataTable();

                    try
                    {
                        adp.Fill(table);

                    }
                    catch (Exception ex)
                    {
                        //Make Error Log Entry for Read Error
                        logger.Error("Error in getting email token pair for :" + dto.Email + " At: " + DateTime.Now + " Due to: " + ex.Message);

                        //disconnect DB
                        obj.Disconnect();
                        return false;
                    }

                    //in case no rows were returned

                    if (table.Rows == null || table.Rows.Count == 0)
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return false;
                    }
                    else
                    {
                        //disconnect DB
                        obj.Disconnect();
                        return true;
                    }
                    
                }
                else
                {
                    //in case of error
                    obj.Disconnect();
                    return false;
                }

            }
            else
            {
                //error in connection
                return false;
            }
        }

        //to remove an entry from EmailVErification
        public Boolean RemoveFromEmailVErification(String email)
        {
            DBHandlerUser obj = new DBHandlerUser();//<<<<<<<<=======SEE

            if (obj.Connect())
            {
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "delete from dbo.EmailVerify where email=@id";

                adp.DeleteCommand = new SqlCommand(query,obj.connection);


                SqlParameter parma = new SqlParameter("id", SqlDbType.NVarChar);
                parma.Value = email;

                adp.DeleteCommand.Parameters.Add(parma);


                try
                {
                    adp.DeleteCommand.ExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    logger.Fatal("Error in removing entry from Email Verification for: " + email + " At: " + DateTime.Now + "Due to " + ex.Message);

                    obj.Disconnect();
                    return false;
                }

                obj.Disconnect();
                return true;
            }
            else
            {
                //error in connection
                return false;
            }
        }
    }
}
