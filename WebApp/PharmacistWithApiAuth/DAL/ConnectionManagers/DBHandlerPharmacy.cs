﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ConnectionManagers
{
    class DBHandlerPharmacy
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public SqlConnection connection;
        static String connectionString;

        public DBHandlerPharmacy()
        {
            //Connection String
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["User"].ConnectionString;
        }

        public Boolean Connect()
        {
            try
            {
                //Create and open connection
                connection = new SqlConnection(connectionString);
                connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                //Make Error Log Entry for Connection Error
                logger.Error("Error in connecting for Pharmacy At: " + DateTime.Now + " Due to " + ex.Message);

                return false;
            }
        }

        public Boolean Disconnect()
        {
            try
            {
                //Close Connection
                connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Make Error Log Entry for Closing Connection Error
                logger.Error("Error in disconnecting for Pharmacy At: " + DateTime.Now + " Due to " + ex.Message);

                return false;
            }
        }
    }
}
