﻿using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class DALCommonCode
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //parameters be like a class with name and type marker
        //returns true if success else false
        public static Boolean MakeSelectCommandForAdapter(SqlDataAdapter adp, String Query, List<NameTypePair> Parameters, SqlConnection conn)
        {
            //Pass Query and Connection
            SqlCommand cmd = new SqlCommand(Query, conn);

            SqlParameter par = null;
            //now for each parameter in list append Parameter to SqlCommand
            //moreover in case of any wrong parameter return false
            foreach (NameTypePair pair in Parameters)
            {
                par = new SqlParameter(pair.Name, pair.Type);
                par.Value = pair.Value;
                try
                {
                    cmd.Parameters.Add(par);

                }
                catch (Exception ex)
                {
                    logger.Fatal("Error in making Select Command:" + Query + " At: " + DateTime.Now + " Due to " + ex.Message);
                    

                    return false;
                }
            }

            //If succesfully appended all parameters then attach the command to
            //adapter SelectCommand
            adp.SelectCommand = cmd;
            return true;
        }

        //parameters be like a class with name and type marker
        //returns true if success else false
        public static Boolean MakeUpdateCommandForAdapter(SqlDataAdapter adp, String Query, List<NameTypePair> Parameters, SqlConnection conn)
        {
            //Pass Query and Connection
            SqlCommand cmd = new SqlCommand(Query, conn);

            SqlParameter par = null;
            //now for each parameter in list append Parameter to SqlCommand
            //moreover in case of any wrong parameter return false
            foreach (NameTypePair pair in Parameters)
            {
                par = new SqlParameter(pair.Name, pair.Type);
                par.Value = pair.Value;
                try
                {
                    cmd.Parameters.Add(par);

                }
                catch (Exception ex)
                {
                    logger.Fatal("Error in making update Command:" + Query + " At: " + DateTime.Now + " Due to " + ex.Message);


                    return false;
                }
            }

            //If succesfully appended all parameters then attach the command to
            //adapter UpdateCommand
            adp.UpdateCommand = cmd;
            return true;
        }


        //parameters be like a class with name and type marker
        //returns true if success else false
        public static Boolean MakeInsertCommandForAdapter(SqlDataAdapter adp, String Query, List<NameTypePair> Parameters, SqlConnection conn)
        {
            //Pass Query and Connection
            SqlCommand cmd = new SqlCommand(Query, conn);

            SqlParameter par = null;
            //now for each parameter in list append Parameter to SqlCommand
            //moreover in case of any wrong parameter return false
            foreach (NameTypePair pair in Parameters)
            {
                par = new SqlParameter(pair.Name, pair.Type);
                par.Value = pair.Value;
                try
                {
                    cmd.Parameters.Add(par);

                }
                catch (Exception ex)
                {
                    logger.Fatal("Error in making insert Command:" + Query + " At: " + DateTime.Now + " Due to " + ex.Message);


                    return false;
                }
            }

            //If succesfully appended all parameters then attach the command to
            //adapter InsertCommand
            adp.InsertCommand = cmd;
            return true;
        }


        //parameters be like a class with name and type marker
        //returns true if success else false
        public static Boolean MakeDeleteCommandForAdapter(SqlDataAdapter adp, String Query, List<NameTypePair> Parameters, SqlConnection conn)
        {
            //Pass Query and Connection
            SqlCommand cmd = new SqlCommand(Query, conn);

            SqlParameter par = null;
            //now for each parameter in list append Parameter to SqlCommand
            //moreover in case of any wrong parameter return false
            foreach (NameTypePair pair in Parameters)
            {
                par = new SqlParameter(pair.Name, pair.Type);
                par.Value = pair.Value;
                try
                {
                    cmd.Parameters.Add(par);

                }
                catch (Exception ex)
                {
                    logger.Fatal("Error in making update Command:" + Query + " At: " + DateTime.Now + " Due to " + ex.Message);


                    return false;
                }
            }

            //If succesfully appended all parameters then attach the command to
            //adapter UpdateCommand
            adp.DeleteCommand = cmd;
            return true;
        }


        //generate a string with it divided into three parts seperated 
        //by % to be used with like statement
        public static String GetStringParsedWithPercents(String str)
        {
            int length = str.Length;

            int parts = length / 3;

            if (parts >= 1)
            {
                //do nothing
            }
            else
            {
                //return the string itself
                return str;
            }

            String temp = "";

            for (int i = 0; i < length; i = i + parts)
            {
                if ((i + parts) > length)
                {
                    temp += str.Substring(i) + "%";
                }
                else
                {
                    temp += str.Substring(i, parts) + "%";
                }
            }

            temp = "%" + temp;
            return temp;
        }
    }
}
