﻿using DAL.ConnectionManagers;
using Models.Comments;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.CommentDAL
{
    public class CommentsHandlerDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public List<MedicineComment> GetCommentsForMedicineId(Int32 medid,Int64 startOffset)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetNextFifteenCommentsForMedicineId", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@Medid", SqlDbType.Int).Value = medid;
                adp.SelectCommand.Parameters.Add("@CustomOffset", SqlDbType.Int).Value = startOffset;


                //creating datatable to hold up the result set of SP
                DataTable table = new DataTable();

                //finally calling the stored procedure
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting comment for :" + medid + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }


                if (table.Rows == null || table.Rows.Count == 0)
                {
                    obj.Disconnect();
                    return null;
                }
                else
                {
                    //do nothing
                }
                List<MedicineComment> list = MedicineComment.GetMedicineCommentsFromRecord(table.Rows);

                //disconnect DB success case for executed query
                obj.Disconnect();
                return list;
            }
            else
            {
                //in case of error connecting to Db return -1
                return null;
            }
        }

        public Boolean SetComment(MedicineComment dto)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "insert into dbo.MedicineComment (MedicineId,UserId,Comment) values(@medid,@uid,@cmnt)";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("medid", SqlDbType.Int, dto.MedicineId.ToString()));
                parameters.Add(new NameTypePair("uid", SqlDbType.Int, dto.UserId.ToString()));
                parameters.Add(new NameTypePair("cmnt",SqlDbType.NVarChar,dto.Comment));


                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in adding comment for :" + dto.MedicineId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }


                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
