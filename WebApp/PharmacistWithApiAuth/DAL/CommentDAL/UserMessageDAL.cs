﻿using DAL.ConnectionManagers;
using Models.Comments;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.CommentDAL
{
    public class UserMessageDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public Boolean InsertUserMessage(UserMessage msg)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "insert into dbo.Messages (Email,Message) values(@email,@msg)";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("email", SqlDbType.NVarChar, msg.email));
                parameters.Add(new NameTypePair("msg", SqlDbType.NVarChar, msg.message));
                

                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in adding user message for :" + msg.email + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }


                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
        
    }
}
