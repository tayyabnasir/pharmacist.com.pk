﻿using DAL.ConnectionManagers;
using Models.BranchManagement;
using Models.Others;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.PharmacyTasks
{
    //operations related to branches of pharmacies
    public class BranchManagementDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //get for provided PharmacyId all the branches
        public List<PharmacyBranch> GetBranchesForPharmacyName(String pharmacyName)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //stored procedure
                SqlDataAdapter adp = new SqlDataAdapter("dbo.GetBranchesForPharmacyName", obj.connection);

                //setting select command to be type stored procedure
                adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

                //adding the where and saltcount parameters
                adp.SelectCommand.Parameters.Add("@PharmacyName", SqlDbType.NVarChar).Value = pharmacyName;


                //creating datatable to hold up the result set of SP
                DataTable table = new DataTable();

                //finally calling the stored procedure
                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting branches for :" + pharmacyName + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                
                if (table.Rows == null || table.Rows.Count == 0)
                {
                    obj.Disconnect();
                    return null;
                }
                else
                {
                    //do nothing
                }
                List<PharmacyBranch> list = PharmacyBranch.GetBranchesFromRecords(table.Rows);

                //disconnect DB success case for executed query
                obj.Disconnect();
                return list;
            }
            else
            {
                //in case of error connecting to Db return -1
                return null;
            }
        }

        //to add a new branch for pharmacy
        public Boolean AddBranch(PharmacyBranch branch)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerPharmacy obj = new DBHandlerPharmacy();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "insert into dbo.PharmacyBranch (Address,Phone,Longitude,Latitude,PharmacyId) values(@address,@phone,@longitude,@latitude,@pharmacyid)";

                List<NameTypePair> parameters = new List<NameTypePair>();

                parameters.Add(new NameTypePair("pharmacyid", SqlDbType.Int, branch.PharmacyId.ToString()));
                parameters.Add(new NameTypePair("longitude", SqlDbType.Float, branch.Longitude.ToString()));
                parameters.Add(new NameTypePair("latitude", SqlDbType.Float, branch.Latitude.ToString()));
                parameters.Add(new NameTypePair("phone", SqlDbType.NVarChar, branch.Phone));
                parameters.Add(new NameTypePair("address", SqlDbType.NVarChar, branch.Address));

                if (DALCommonCode.MakeInsertCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in adding branch for :" + branch.PharmacyId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }


                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }

        //to update a branch
        public Boolean UpdateBranch(PharmacyBranch branch)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerPharmacy obj = new DBHandlerPharmacy();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                String query = "update dbo.PharmacyBranch SET Address =@address,Phone =@phone,Longitude =@longitude,Latitude=@latitude where BranchId=@branchid";

                List<NameTypePair> parameters = new List<NameTypePair>();


                parameters.Add(new NameTypePair("branchid", SqlDbType.Int, branch.BranchId.ToString()));
                parameters.Add(new NameTypePair("longitude", SqlDbType.Float, branch.Longitude.ToString()));
                parameters.Add(new NameTypePair("latitude", SqlDbType.Float, branch.Latitude.ToString()));
                parameters.Add(new NameTypePair("phone", SqlDbType.NVarChar, branch.Phone));
                parameters.Add(new NameTypePair("address", SqlDbType.NVarChar, branch.Address));

                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return false;
                }


                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in updating branch for :" + branch.BranchId + " At: " + DateTime.Now + " Due to " + ex.Message);



                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }


        //to deactivate a branch
        public Boolean DeleteBranch(Int32 branchId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerManufacturer obj = new DBHandlerManufacturer();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "UPDATE dbo.PharmacyBranch SET isactive=0 WHERE BranchId = @branchid";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("branchid", SqlDbType.Int, branchId.ToString()));


                //now to create Select Command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false;
                }

                //now update the record

                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in deactivating branch for:" + branchId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }



                //Dissconnect from DB
                obj.Disconnect();
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }


        // to get owner of pharmacy branch
        //to return pharmacy id for given itemid
        public Int32 GetOwnerOfBranch(Int32 branchId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //make parmetrized query to get data from database
                SqlDataAdapter adp = new SqlDataAdapter();

                List<NameTypePair> parameters = new List<NameTypePair>();

                String query = "Select pharmacyId from dbo.PharmacyBranch where BranchId=@branch  and IsActive=1";

                parameters.Add(new NameTypePair("branch", SqlDbType.Int, branchId.ToString()));

                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, parameters, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //in case of select command error
                    obj.Disconnect();
                    return -1;
                }

                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    //in case of error
                    logger.Error("Error in getting pharmacyid entry for Branch:" + branchId + " and  At: " + DateTime.Now + " Due to " + ex.Message);



                    //Dissconnect from DB
                    obj.Disconnect();
                    return -1;
                }

                int pharmId = -1;

                if (table == null || table.Rows.Count == 0)
                {
                    obj.Disconnect();
                    return -1;
                }
                else
                {
                    //do nothing
                }

                pharmId = (int)table.Rows[0]["pharmacyId"];

                //Dissconnect from DB
                obj.Disconnect();
                return pharmId;
            }
            else
            {
                //in case of error connecting to Db return Null
                return -1;
            }
        }
    }
}
