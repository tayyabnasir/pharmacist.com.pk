﻿using DAL.ConnectionManagers;
using Models.Others;
using Models.Roles;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.PharmacyTasks
{
    //to access pharmacy attributes for user
    public class PharmacyAccessDAL
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //get manufacturer details for the userid
        //may return null so be cautious
        public Pharmacy GetPharmacyForUserId(Int32 userId)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "select PharmacyId,PharmacyName,Website,UserId FROM dbo.Pharmacy WHERE UserId = @userId";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("userId", SqlDbType.Int, userId.ToString()));
                //now to create Select Command
                if (DALCommonCode.MakeSelectCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return null;
                }

                DataTable table = new DataTable();

                try
                {
                    adp.Fill(table);
                }
                catch (Exception ex)
                {
                    logger.Error("Error in getting pharmacy object for UserId:" + userId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return null;
                }

                if (table.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    //do nothing
                }

                Pharmacy pharmacy = Pharmacy.GetManufacturerForRecord(table.Rows[0]);

                //Dissconnect from DB
                obj.Disconnect();

                //return manufacturer's name
                return pharmacy;
            }
            else
            {
                //in case of error connecting to Db return Null
                return null;
            }
        }


        //to update pharmacy information
        public Boolean UpdatePharmacy(Pharmacy dto)
        {
            //Create an object for dealing conneciton disconnection to DB
            DBHandlerUser obj = new DBHandlerUser();

            //open connection
            if (obj.Connect())
            {
                //if successfull connection then use adapter to update required data
                SqlDataAdapter adp = new SqlDataAdapter();

                //making a parametrized select command for adapter
                String query = "update dbo.Pharmacy Set PharmacyName=@pname,Website=@site WHERE PharmacyId = @pid";



                //make a list of all parameters to be passed to command
                List<NameTypePair> Params = new List<NameTypePair>();
                Params.Add(new NameTypePair("pid", SqlDbType.Int, dto.PharmacyId.ToString()));
                Params.Add(new NameTypePair("site", SqlDbType.NVarChar, dto.Website));
                Params.Add(new NameTypePair("pname", SqlDbType.NVarChar, dto.PharmacyName));
                //now to create Select Command
                if (DALCommonCode.MakeUpdateCommandForAdapter(adp, query, Params, obj.connection))
                {
                    //do nothing
                }
                else
                {
                    //Dissconnect from DB
                    obj.Disconnect();
                    //if fail due to wrong parameters in list return null
                    return false ;
                }

                

                try
                {
                    adp.UpdateCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error("Error in updating pharmacy object for Pharmacy id:" + dto.PharmacyId + " At: " + DateTime.Now + " Due to " + ex.Message);


                    //Dissconnect from DB
                    obj.Disconnect();
                    return false;
                }

                
                //Dissconnect from DB
                obj.Disconnect();

                //return manufacturer's name
                return true;
            }
            else
            {
                //in case of error connecting to Db return Null
                return false;
            }
        }
    }
}
