﻿using Models.Others;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Comments
{
    public class MedicineComment
    {
        public Int64 Id { get; set; }
        public Int32 MedicineId { get; set; }
        public String Comment { get; set; }
        public Int32 UserId { get; set; }
        public UserInfo user { get; set; }
        public String CommentDate { get; set; }
        public Boolean ValidateComment()
        {
            if(Comment==null || Comment.Equals("") || Comment.Length > 500)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static List<MedicineComment> GetMedicineCommentsFromRecord(DataRowCollection rows)
        {
            List<MedicineComment> list = new List<MedicineComment>();

            foreach(DataRow row in rows)
            {
                MedicineComment temp = new MedicineComment();

                temp.Id = (Int64)row["Id"];
                temp.MedicineId = (Int32)row["MedicineId"];
                temp.Comment = row["Comment"].ToString();
                temp.CommentDate = row["UpdatedOn"].ToString();
                temp.user = UserInfo.GetUserInfoFromRecord(row);

                list.Add(temp);
            }

            return list;
        }
    }
}
