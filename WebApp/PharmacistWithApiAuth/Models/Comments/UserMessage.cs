﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Comments
{
    public class UserMessage
    {
        public String email { get; set; }
        public String message { get; set; }

        public Boolean validateMessage()
        {
            if(this.email==null || this.email.Equals("") || this.email.Length > 40)
            {
                return false;
            }
            else
            {
                this.email.Trim();
            }
            if(this.message==null || this.message.Equals("") || this.message.Length > 1500)
            {
                return false;
            }
            else
            {
                message.Trim();
            }

            return true;
        }
    }
}
