﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.BranchManagement
{
    //represent a branch entity
    public class PharmacyBranch
    {
        public Int32 BranchId { get; set; }
        public String Address { get; set; }
        public String Phone { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }
        public Int32 PharmacyId { get; set; }
        public Boolean IsActive { get; set; }

        //some static code
        public static List<PharmacyBranch> GetBranchesFromRecords(DataRowCollection Rows)
        {
            List<PharmacyBranch> list = new List<PharmacyBranch>();

            foreach(DataRow row in Rows)
            {
                PharmacyBranch temp = new PharmacyBranch();

                temp.Address = row["Address"].ToString();
                temp.BranchId = (Int32)row["BranchId"];
                temp.IsActive = (Boolean)row["IsActive"];
                temp.Latitude = (Double)row["Latitude"];
                temp.Longitude = (Double)row["Longitude"];
                temp.PharmacyId = (Int32)row["PharmacyId"];
                temp.Phone = row["Phone"].ToString();

                list.Add(temp);
            }

            return list;
        }

        //to verify all fields are ok before adding to database
        public Boolean VerifyBranch(Pharmacy pharm)
        {
            this.PharmacyId = pharm.PharmacyId;
            this.Phone = this.Phone.Trim();
            this.Address = this.Address.Trim();

            if (this.Address==null || this.Address.Equals("") || this.Address.Length > 69)
            {
                return false;
            }
            if(this.Phone==null || this.Phone.Equals("") || this.Phone.Length > 19)
            {
                return false;
            }

            return true;
        }
    }
}
