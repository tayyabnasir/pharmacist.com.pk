﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Roles
{
    //properties specific to Mnaufacturer type users
    public class Manufacturer
    {
        public Int32 ManufacturerId { get; set; }
        public String ManufacturerName { get; set; }
        public Int32 UserId { get; set; }
        public String Website { get; set; }
        public String Phone { get; set; }


        //some static code
        public static Manufacturer GetManufacturerForRecord(DataRow row)
        {
            Manufacturer obj = new Manufacturer();
            obj.ManufacturerId = (Int32)row["ManufacturerId"];
            obj.ManufacturerName = row["ManufacturerName"].ToString();
            obj.Phone = row["Phone"].ToString();
            obj.UserId = (Int32)row["UserId"];
            obj.Website = row["Website"].ToString();

            return obj;
        }

        public Boolean VerifyManufacturer()
        {
            if (Website == null)
            {
                Website = "";
            }
            if (Phone == null)
            {
                Phone = "";
            }
            if (this.ManufacturerName == null || this.ManufacturerName.Equals("") || this.ManufacturerName.Length > 50)
            {
                return false;
            }
            else
            {
                this.ManufacturerName.Trim();
            }
            if (this.Website.Length > 50)
            {
                return false;
            }
            else
            {
                this.Website.Trim();
            }
            if (this.Phone.Length > 20)
            {
                return false;
            }
            else
            {
                this.Phone.Trim();
            }

            return true;
        }
    }
}
