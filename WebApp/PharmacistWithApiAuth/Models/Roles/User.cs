﻿using Models.AccountManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Roles
{
    //represnt a user entity all other Roles are inherited from this
    public class User
    {
        public Int32 UserId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Login { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public Boolean IsVerified { get; set; }
        public List<String> Permissions { get; set; }
        public List<String> Roles { get; set; }

        public User()
        {

        }

        public User(List<String> perms,List<String> role)
        {
            Permissions = perms;
            Roles = role;
        }



        //////////////////////////////////////////////////////////////
        /// Static Codes
        // to get a datarows from db and extract Permission strings from them 
        public static List<String> GetPermissionsFromRecords(DataRowCollection rows)
        {
            List<String> perms = new List<String>();

            foreach(DataRow row in rows)
            {
                perms.Add(row["Name"].ToString());
            }

            return perms;
        }

        //to get datarows and extract Roles from them
        public static List<String> GetRolesFromRecords(DataRowCollection rows)
        {
            List<String> perms = new List<String>();

            foreach (DataRow row in rows)
            {
                perms.Add(row["RoleName"].ToString());
            }

            return perms;
        }

        //to get a dataRow and convert it back to user object
        public static User GetUserFromRecord(DataRow row)
        {
            User obj = new User();
            obj.Email = row["contactemail"].ToString();
            obj.FirstName = row["firstname"].ToString();
            obj.IsVerified = (bool)row["isverified"];
            obj.LastName = row["lastname"].ToString();
            obj.Login = row["login"].ToString();
            obj.Password = row["password"].ToString();
            obj.UserId = (int)row["userid"];

            return obj;
        }

        //to get USer object for SignUpDTO
        public static User GetUserForSignUpDTO(SignUpDTO signUpDTO)
        {
            User obj = new User();
            obj.Email = signUpDTO.Email;
            obj.Login = signUpDTO.Email;
            obj.FirstName = signUpDTO.FirstName;
            obj.IsVerified = false;
            obj.LastName = signUpDTO.LastName;
            obj.Password = signUpDTO.Password;

            return obj;
        }
    }
}
