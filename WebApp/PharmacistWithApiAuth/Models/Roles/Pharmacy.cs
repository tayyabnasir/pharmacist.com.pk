﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Roles
{
    //represent pharmacy entity in the database
    public class Pharmacy
    {
        public Int32 PharmacyId { get; set; }
        public String PharmacyName { get; set; }
        public Int32 UserId { get; set; }
        public String Website { get; set; }

        //some static code
        public static Pharmacy GetManufacturerForRecord(DataRow row)
        {
            Pharmacy obj = new Pharmacy();
            obj.PharmacyId = (Int32)row["PharmacyId"];
            obj.PharmacyName = row["PharmacyName"].ToString();
            obj.UserId = (Int32)row["UserId"];
            obj.Website = row["Website"].ToString();

            return obj;
        }

        public Boolean VerifyPharmacy()
        {
            if (Website == null)
            {
                Website = "";
            }
            if (this.PharmacyName==null || this.PharmacyName.Equals("") || this.PharmacyName.Length > 50)
            {
                return false;
            }
            else
            {
                this.PharmacyName.Trim();
            }
            if (this.Website.Length > 50)
            {
                return false;
            }
            else
            {
                this.Website.Trim();
            }

            return true;
        }
    }
}
