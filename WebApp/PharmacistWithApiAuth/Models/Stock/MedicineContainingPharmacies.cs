﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stock
{
    //to hold pharmacies and their stock, who have certain medicine in their stock
    public class MedicineContainingPharmacies
    {
        public Int32 PharmacyId { get; set; }
        public String PharmacyName { get; set; }
        public Double RetailPrice { get; set; }

        ////////////////////////////////////////////////////////////////
        //Static Code
        public static List<MedicineContainingPharmacies> GetRecordsFromDataTable(DataRowCollection rows)
        {
            List<MedicineContainingPharmacies> list = new List<MedicineContainingPharmacies>();

            foreach(DataRow row in rows)
            {
                MedicineContainingPharmacies obj = new MedicineContainingPharmacies();

                obj.PharmacyId = (int)row["Pharmacyid"];
                obj.PharmacyName = row["PharmacyName"].ToString();
                obj.RetailPrice = (double)row["RetailPrice"];

                list.Add(obj);
            }

            return list;
        }
    }
}
