﻿using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stock
{
    //to represent records for pharmacy i.e., all products a pharmacy has added
    public class MedicineStockPair
    {
        public MedicineUserInterface Medicine { get; set; }
        public Int32 ItemId { get; set; }
        public Double RetailPrice { get; set; }
        public Boolean Availability { get; set; }

      
        public MedicineStockPair()
        {

        }

        public MedicineStockPair(MedicineUserInterface medInterface,Stock stockObj)
        {
            Medicine = medInterface;
            ItemId = stockObj.ItemId;
            RetailPrice = stockObj.RetailPrice;
            Availability = stockObj.Available;
        }
    }
}
