﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stock
{
    //represent Stock entity
    public class Stock
    {
        public Int32 ItemId { get; set; }
        public Int32 MedicineId { get; set; }
        public Double RetailPrice { get; set; }
        public Boolean Available { get; set; }
        public Int32 PharmacyId { get; set; }



        ///////////////////////////////////////////////////////////////////////
        //Static Code

        //takes records from database and convert them to List of Stock
        public static List<Stock> GetStockListFromDataRows(DataRowCollection rows)
        {
            List<Stock> list = new List<Stock>();

            foreach(DataRow row in rows)
            {
                Stock obj = new Stock();
                obj.Available = (bool)row["available"];
                obj.ItemId = (Int32)row["itemid"];
                obj.MedicineId = (Int32)row["medicineid"];
                obj.PharmacyId = (Int32)row["pharmacyid"];
                obj.RetailPrice = (Double)row["retailprice"];

                list.Add(obj);
            }

            return list;
        }

        public Boolean PrepareStockObject(Pharmacy obj)
        {
            if (this.ItemId < 0)
            {
                return false;
            }
            if (this.PharmacyId != obj.PharmacyId)
            {
                this.PharmacyId = obj.PharmacyId;
            }
            if (this.RetailPrice < 0)
            {
                return false;
            }

            return true;
        }


    }
}
