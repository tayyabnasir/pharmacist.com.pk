﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AccountManagement
{
    //have fields for sign up information
    public class SignUpDTO
    {
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Password { get; set; }

        public Boolean ValidateAndFormatObject()
        {
            //For Email
            if (Email != null && !Email.Equals("") && Email.Length < 50)
            {
                //trim extra spaces from ends
                Email = Email.Trim();
            }
            else
            {
                return false;
            }
            //For First Name
            if (FirstName != null && !FirstName.Equals("") && FirstName.Length < 20)
            {
                //trim extra spaces from ends
                FirstName = FirstName.Trim();
            }
            else
            {
                return false;
            }
            //For Last Name
            if (LastName != null && !LastName.Equals("") && LastName.Length < 20)
            {
                //trim extra spaces from ends
                LastName = LastName.Trim();
            }
            else
            {
                return false;
            }
            //For Password
            if (Password != null && !Password.Equals("") && Password.Length < 25)
            {
                //trim extra spaces from ends
                Password = Password.Trim();
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
