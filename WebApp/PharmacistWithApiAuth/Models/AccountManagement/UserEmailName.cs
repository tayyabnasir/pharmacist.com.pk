﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AccountManagement
{
    public class UserEmailName
    {
        public String Name { get; set; }
        public String Email { get; set; }
    }
}
