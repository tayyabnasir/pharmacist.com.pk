﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AccountManagement
{
    public class UserSettingsDTO
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }

        public Boolean ValidateAndFormatObject()
        {
            
            //For First Name
            if (FirstName != null && !FirstName.Equals("") && FirstName.Length < 20)
            {
                //trim extra spaces from ends
                FirstName = FirstName.Trim();
            }
            else
            {
                return false;
            }
            //For Last Name
            if (LastName != null && !LastName.Equals("") && LastName.Length < 20)
            {
                //trim extra spaces from ends
                LastName = LastName.Trim();
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
