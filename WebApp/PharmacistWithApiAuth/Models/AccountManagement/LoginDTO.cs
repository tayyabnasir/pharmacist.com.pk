﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AccountManagement
{
    //code reviwed and no possible errors so far

    //contains the login credential fields
    public class LoginDTO
    {
        public String Login { get; set; }
        public String Password { get; set; }

        //verifies the login object to be not valid or null
        public Boolean ValidateLogin()
        {
            if (Login == null || Login.Equals(""))
            {
                return false;
            }
            else if (Password == null || Password.Equals(""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
