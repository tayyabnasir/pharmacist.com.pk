﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AccountManagement
{
    //represent an email verification entity
    public class EmailVerificationDTO
    {
        public Int32 Id { get; set; }
        public String Email { get; set; }
        public String Token { get; set; }

        public Boolean VerifyEmail()
        {
            //still requires email format to be matched

            if(Email==null || Email.Equals("") || Email.Length > 50)
            {
                return false;
            }
            else
            {
                return true;
            }
        }



        /////////////////////////////////////////////////////////////
        //Static code
        public static EmailVerificationDTO GetEmailVerificationDTOFromRecord(DataRow row)
        {
            EmailVerificationDTO obj = new EmailVerificationDTO();
            obj.Email = row["Email"].ToString();
            obj.Id = (int)row["Id"];
            obj.Token = row["Token"].ToString();

            return obj;
        }
    }
}
