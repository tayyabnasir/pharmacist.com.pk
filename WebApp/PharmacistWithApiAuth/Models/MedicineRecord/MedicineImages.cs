﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    public class MedicineImages
    {
        public String url { get; set; }
        public Int32 medicineId { get; set; }
        public Int64 id { get; set; }


        /////////////////////////////////////////////////////////////////
        /////////////static code/////////////////////////////////////////
        public static List<MedicineImages> GetMedImagesForRecord(DataRowCollection rows)
        {
            List<MedicineImages> list = new List<MedicineImages>();

            foreach(DataRow row in rows)
            {
                MedicineImages obj = new MedicineImages();

                obj.id = (Int64)row["Id"];
                obj.medicineId = (Int32)row["MedicineId"];
                obj.url = row["Url"].ToString();

                list.Add(obj);
            }

            return list;
        }
    }
}
