﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    //used for search by brandname
    public class MedicineAmoutPair
    {
        public MedicineUserInterface medicine { get; set; }
        public double RequiredAmount { get; set; }
    }
}
