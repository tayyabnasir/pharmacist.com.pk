﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    //the purpose of this DTO is to contain
    //med records modified to send to client
    //where salt is single comma seperated string so are the other entities
    public class MedicineUserInterface
    {
        public Int32 MedicineId { get; set; }
        public String BrandName { get; set; }
        public String Manufacturer { get; set; }
        public String DrugForm { get; set; }
        public Double PricePerUnit { get; set; }
        public Double TradePrice { set; get; }
        public String PackSize { get; set; }
        //form by joining all salts with a + sign
        public String Salt { get; set; }
        //form by joining all salt amounts with a + sign
        //also unit for each amount will be in this string
        public String SaltAmount { get; set; }

        public MedicineUserInterface(Medicine medObj)
        {
            BrandName = medObj.BrandName;
            DrugForm = medObj.DrugForm;
            Manufacturer = medObj.Manufacturer;
            MedicineId = medObj.MedicineId;
            PackSize = medObj.PackSize;
            PricePerUnit = medObj.PricePerUnit;
            TradePrice = medObj.TradePrice;

            Boolean flagToCheckForFirstEntry = true;
            foreach (Salt saltObj in medObj.Salt)
            {
                //in case of first run
                if (flagToCheckForFirstEntry)
                {
                    flagToCheckForFirstEntry = false;
                    Salt = saltObj.SaltName;
                    SaltAmount = saltObj.Amount + saltObj.Unit;
                }
                else
                {
                    //append '+' to all salt attributes
                    Salt = Salt + " + ";
                    SaltAmount = SaltAmount + " + ";

                    Salt += saltObj.SaltName;
                    SaltAmount += saltObj.Amount + saltObj.Unit;
                }

            }
        }

        public MedicineUserInterface()
        {

        }


        ////////////////////////////////////////////////////////////////////
        //Static Code
        //get a joined salt and medicine record and rets a MedicineUSerInterface
        //object
        //caution only initializes the Medicine Fields and not Salt fields
        public static MedicineUserInterface MedicineFieldInitForMedicineUserInterface(DataRow row)
        {
            MedicineUserInterface medObj = new MedicineUserInterface();

            medObj.BrandName = row["BrandName"].ToString();
            medObj.DrugForm = row["DrugForm"].ToString();
            medObj.Manufacturer = row["Manufacturer"].ToString();
            medObj.PackSize = row["PackSize"].ToString();
            medObj.PricePerUnit = (double)row["PricePerUnit"];
            medObj.TradePrice = (double)row["TradePrice"];
            medObj.MedicineId = (int)row["MedicineId"];

            return medObj;
        }

        //takes a MedicineUserInterface object and puts the salts attributes in
        //it considering MEdicine Fields are already set
        //caution only initializes the Salt Fields 
        public static MedicineUserInterface SaltFieldInitForMedicineUserInterface(DataRow row, MedicineUserInterface medObj)
        {
            medObj.Salt += row["SaltName"].ToString();
            //appending salt amount with its units
            medObj.SaltAmount += row["Amount"].ToString() + row["Unit"].ToString();



            return medObj;
        }

        //takes list of Medicine objects and return MedicineUserInterface List
        //may return null
        public static List<MedicineUserInterface> GetMedUserInterfaceForMedicines(List<Medicine> medList)
        {
            if (medList == null)
            {
                return null;
            }
            else
            {
                //do nothing
            }

            List<MedicineUserInterface> medUIList = new List<MedicineUserInterface>();

            foreach(Medicine med in medList)
            {
                MedicineUserInterface temp = new MedicineUserInterface(med);

                medUIList.Add(temp);
            }

            return medUIList;
        }


    }
}
