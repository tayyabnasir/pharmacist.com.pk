﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    //serve as Medicine model with additonal features of availability and retail price
    public class MedicineStock
    {
        public Medicine Medicine { get; set; }
        public Double RetailPrice { get; set; }
        public String Pharmacy { get; set; }
    }
}
