﻿using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    //reps the salt entity in database
    public class Salt
    {
        public Int32 SaltNumber { get; set; }
        public Int32 MedicineId { get; set; }
        public String SaltName { get; set; }
        public Double Amount { get; set; }
        public String Unit { get; set; }

        ////////////////////////////////////////////////////////////////
        ////////Some Functions To Validate the Medicine Object//////////

        //must be called before passing an object to BAL
        public Boolean ValidateSalt(Salt salt)
        {
            //check for string lengths
            if (salt.SaltName == null || salt.SaltName.Equals("") || salt.SaltName.Length > 40)
            {
                return false;
            }
            if (salt.Unit == null || salt.Unit.Equals("") || !StaticValuesInitializer.IfSIUnitsExists(salt.Unit))
            {
                return false;
            }

            if (salt.Amount < 1)
            {
                return false;
            }

            return true;
        }

        //remove extra spaces and assign values
        public Boolean FormatSalt(Salt salt, Int32 medid)
        {
            //set Medicne of salt to be that of Medicine
            salt.MedicineId = medid;

            //reomve spaces
            salt.SaltName.Trim();
            salt.Unit.Trim();

            return true;
        }




        /////////////////////////////////////////////////////////////////////
        //Static Codes
        //to get a datatable with Salt objects and convert to list of Salt
        //may return null so care for it when calling
        public static List<Salt> ConvertSaltTableToList(DataTable table)
        {
            List<Salt> list = new List<Salt>();

            foreach (DataRow row in table.Rows)
            {
                Salt obj = new Salt();
                obj.Amount = (double)row["Amount"];
                obj.MedicineId = (int)row["MedicineId"];
                obj.SaltName = row["SaltName"].ToString();
                obj.SaltNumber = (int)row["SaltNumber"];
                obj.Unit = row["Unit"].ToString();

                //and now add this object to Salt list
                list.Add(obj);
            }

            //finally return the list
            return list;
        }

    }
}
