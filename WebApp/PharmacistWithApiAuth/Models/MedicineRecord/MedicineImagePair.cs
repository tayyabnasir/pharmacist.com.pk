﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    public class MedicineImagePair
    {
        public MedicineUserInterface medicine { get; set; }
        public List<MedicineImages> Images { get; set; }

    }
}
