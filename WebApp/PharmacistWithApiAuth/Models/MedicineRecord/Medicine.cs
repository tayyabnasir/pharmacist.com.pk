﻿using Models.MedicineRecord;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Models
{
    //reps the Medicine entity in database
    public class Medicine
    {
        public Int32 MedicineId { get; set; }
        public String BrandName { get; set; }
        public String Manufacturer { get; set; }
        public String DrugForm { get; set; }
        public String PackSize { get; set; }
        public Int32 TotalUnitsPerPack { get; set; }
        public Double TradePrice { set; get; }
        public Double PricePerUnit { get; set; }
        public Int32 SaltCount { get; set; }
        public List<Salt> Salt { get; set; }


        ////////////////////////////////////////////////////////////////
        ////////Some Functions To Validate the Medicine Object//////////

        //must be called before passing an object to BAL
        public Boolean ValidateMedicine(Medicine med)
        {
            //validate brandname to be not more than field 
            //length in DB
            if (med.BrandName == null || med.BrandName.Equals("") || med.BrandName.Length > 40)
            {
                return false;
            }
            //validate Manufacturer to be not more than field 
            //length in DB
            if (med.Manufacturer == null || med.Manufacturer.Equals("") || med.Manufacturer.Length > 40)
            {
                return false;
            }
            
            //validate TradePrice not to be negative
            if (med.TradePrice < 0)
            {
                return false;
            }
            //validate TotalUnitsPerPack not to be negative
            if (med.TotalUnitsPerPack < 0)
            {
                return false;
            }
            //validate PricePerUnit not to be negative and be equal to
            //division of RetailPrice and TotalUnits
            if (med.PricePerUnit < 0 || med.PricePerUnit != (med.TradePrice / med.TotalUnitsPerPack))
            {
                return false;
            }
            //check if Salt list is not empty
            if (med.Salt == null || med.Salt.Count < 1)
            {
                return false;
            }
            //moreover validate eac salt as well
            foreach (Salt salt in med.Salt)
            {
                if (salt.ValidateSalt(salt) == false)
                {
                    return false;
                }
            }

            //check if salt count is correctly calculated
            if (med.SaltCount <= 0 || med.SaltCount != med.Salt.Count)
            {
                return false;
            }
            //check if Form is a supported field
            if (med.DrugForm == null || med.DrugForm.Equals("") || !StaticValuesInitializer.IfFormExists(med.DrugForm))
            {
                return false;
            }


            return true;
        }

        //to remove extra spaces and calculate certain fields
        //returns false in case any field cannot be calculated
        public Boolean FormatMedicineObject(Medicine med)
        {
            //trim extra spaces
            med.BrandName = med.BrandName.Trim();
            med.DrugForm = med.DrugForm.Trim();
            med.Manufacturer = med.Manufacturer.Trim();
            med.PackSize = med.PackSize.Trim();

            //calculate fields

            //calculate total units
            med.TotalUnitsPerPack = CalculateUnitsForPackaging(med.PackSize);

            //calculate priceper unit
            if (med.TotalUnitsPerPack < 1)
            {
                return false;
            }
            else
            {
                med.PricePerUnit = med.TradePrice / med.TotalUnitsPerPack;
            }

            //calculate Salts
            if (med.Salt == null)
            {
                return false;
            }
            else
            {
                med.SaltCount = med.Salt.Count;
                if (med.SaltCount == 0)
                {
                    return false;
                }
            }


            //also format salt fields
            foreach (Salt salt in med.Salt)
            {
                if (salt.FormatSalt(salt, med.MedicineId) == false)
                {
                    return false;
                }
            }

            return true;
        }

        //to calculate units for a valid packaging
        //returns the units calculated
        //please always verify the pattern before calling this function
        public Int32 CalculateUnitsForPackaging(String packaging)
        {
            Int32 units = 1;


            MatchCollection match = Regex.Matches(packaging, @"[0-9]+");

            foreach (var v in match)
            {
                units = units * Int32.Parse(v.ToString());
            }

            return units;
        }

        ////////////////////////////////////////////////////////////////
        //this function is to match any passed packaging string to
        //the pattern that is supported
        //return true for match and false for no match
        public Boolean CheckForPackagePattern(String package)
        {
            package = package.Trim();

            //upto niow the supported patterns are
            Regex regx0 = new Regex(@"^[0-9]+$");
            Regex regx1 = new Regex(@"^[0-9]+[ ]*[Xx][ ]*[0-9]+$");
            Regex regx2 = new Regex(@"^[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+$");
            Regex regx3 = new Regex(@"^[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+$");
            Regex regx4 = new Regex(@"^[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+[ ]*[Xx][ ]*[0-9]+$");

            if (regx0.Match(package).Success)
            {
                return true;
            }
            else if (regx1.Match(package).Success)
            {
                return true;
            }
            else if (regx2.Match(package).Success)
            {
                return true;
            }
            else if (regx3.Match(package).Success)
            {
                return true;
            }
            else if (regx4.Match(package).Success)
            {
                return true;
            }
            else
            {
                return false;
            }

        }




        ///////////////////////////////////////////////////////////////////////
        //Static Codes

        //init Medicine DTO for given matching records (DataRow) with one medid
        //caution returns null so please check when calling
        public static Medicine InitMedicine(DataRow[] record)
        {
            //in case there is no record return null
            if (record == null)
            {
                return null;
            }
            if (record.Length == 0)
            {
                return null;
            }

            //create Medicine object
            Medicine medObj = new Medicine();

            //assign the fields that are common to Medicine
            medObj.BrandName = record[0]["BrandName"].ToString();
            medObj.DrugForm = record[0]["DrugForm"].ToString();
            medObj.Manufacturer = record[0]["Manufacturer"].ToString();
            medObj.MedicineId = (Int32)record[0]["MedicineId"];
            medObj.PackSize = record[0]["PackSize"].ToString();
            medObj.PricePerUnit = (double)record[0]["PricePerUnit"];
            medObj.TradePrice = (double)record[0]["TradePrice"];
            medObj.TotalUnitsPerPack = (Int32)record[0]["TotalUnitsPerPack"];
            medObj.SaltCount = (Int32)record[0]["SaltCount"];

            //and now put down all the salts
            medObj.Salt = new List<Salt>();

            foreach (DataRow row in record)
            {
                Salt saltObj = new Salt();
                saltObj.Amount = (double)row["Amount"];
                saltObj.MedicineId = (Int32)row["MedicineId"];
                saltObj.SaltName = row["SaltName"].ToString();
                saltObj.SaltNumber = (Int32)row["SaltNumber"];
                saltObj.Unit = row["Unit"].ToString();

                //and append salt object to Medicine salt list
                medObj.Salt.Add(saltObj);
            }

            //and finally return the Medicne DTO
            return medObj;
        }


        //responsible to get a DataTable containing medicine records for same id
        //and merge the duplicate ids by concatinating the salt part
        //may return null so be cautious
        public static List<Medicine> GetRecordsInMedicine(DataTable table)
        {
            //list to store the results
            List<Medicine> medsList = new List<Medicine>();

            //1. for each row find row with matching MedicineId
            //and merge them all
            foreach (DataRow row in table.Rows)
            {
                //also check if a row is marked delete then continue to next
                //iteration
                if (row.RowState == DataRowState.Deleted)
                {
                    continue;
                }

                //2. find all the matching records wiht same MedicineId
                Int32 medid = (Int32)row["MedicineId"];
                DataRow[] matchingRows = table.Select("MedicineId=" + medid);

                //3. Make a Medicine Object
                Medicine refObj = InitMedicine(matchingRows);
                if (refObj != null)
                {
                    medsList.Add(refObj);
                }
                //and remove the matching rows from the datatable
                //so that there is no repetition
                foreach (DataRow matchingRow in matchingRows)
                {
                    matchingRow.Delete();
                    //table.Rows.Remove(matchingRow);
                }

            }
            //7. return the resulting list
            return medsList;
        }
    }
}
