﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    class StaticValuesInitializer
    {
        private static List<String> medicineForms=new List<string>();
        private static List<String> siUnits=new List<string>();

        static StaticValuesInitializer()
        {
            medicineForms.Add("tab");
            medicineForms.Add("cap");
            medicineForms.Add("inj");
            medicineForms.Add("inf");

            siUnits.Add("ml");
            siUnits.Add("mg");
            siUnits.Add("ml/mg");
            siUnits.Add("IU");
        }

        //to get and put all forms from db to our List
        //returns true if successfully get all forms else false
        public static Boolean SetMedicineForms()
        {


            return true;
        }

        //to get all forms 
        //return null so be cautious
        public static List<String> GetAllMedicineForms()
        {

            return medicineForms;
        }

        //to check if a form exists in our list or not
        //return true or false for existing and non existing respectively
        public static Boolean IfFormExists(String form)
        {
            form = form.ToLower();
            foreach (var f in medicineForms)
            {
                if (f.ToLower().Equals(form))
                {
                    return true;
                }
            }
            return false;
        }



        //to get and put all siunits from db to our List
        //returns true if successfully get all forms else false
        public static Boolean SetMedicineSIUnits()
        {


            return true;
        }

        //to get all SIUnits 
        //return null so be cautious
        public static List<String> GetAllMedicineSIUnits()
        {

            return medicineForms;
        }

        //to check if a SIUnits exists in our list or not
        //return true or false for existing and non existing respectively
        public static Boolean IfSIUnitsExists(String SIUnits)
        {
            SIUnits = SIUnits.ToLower();
            foreach (var f in siUnits)
            {
                if (f.ToLower().Equals(SIUnits))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
