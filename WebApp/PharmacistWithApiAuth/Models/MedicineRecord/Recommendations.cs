﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    //to hold recommendations 
    public class Recommendations
    {
        public Int32 id { get; set; }
        public String label { get; set; }


        //some static code
        static public List<Recommendations> GetRecommendationsForRecord(DataRowCollection rows,String field)
        {

            List<Recommendations> list = new List<Recommendations>();
            Int32 i = 1;
            foreach(DataRow row in rows)
            {
                Recommendations obj = new Recommendations();
                obj.id = i++;
                obj.label = row[field].ToString();

                list.Add(obj);
            }

            return list;
        } 
    }
}
