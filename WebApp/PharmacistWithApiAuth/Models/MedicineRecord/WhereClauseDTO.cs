﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MedicineRecord
{
    //a dto for containing dynamic where clause string and
    //count of salt involved
    //used by the StoredProcedure
    public class WhereClauseDTO
    {
        public String where { get; set; }
        public Int32 saltCount { get; set; }

        public static WhereClauseDTO getWhereClause(List<String> salts)
        {
            WhereClauseDTO obj = new WhereClauseDTO();

            obj.saltCount = 0;
            obj.where = "";

            foreach (String saltName in salts)
            {
                //for first run no need to put or
                if (obj.saltCount == 0)
                {
                    obj.where = obj.where + " saltname='" + saltName + "' ";
                }
                //for every other an or has to be also added
                else
                {
                    obj.where = obj.where + " or saltname='" + saltName + "' ";
                }
                obj.saltCount++;
            }

            return obj;
        }
    }
}
