﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Others
{
    public class UserInfo
    {
        public String FirstName { get; set; }
        public String LastName { get; set; } 
        
        public static UserInfo GetUserInfoFromRecord(DataRow row)
        {
            UserInfo obj = new UserInfo();
            obj.FirstName = row["FirstName"].ToString();
            obj.LastName = row["LastName"].ToString();

            return obj;
        }
    }
}
