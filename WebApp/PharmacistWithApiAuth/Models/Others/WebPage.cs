﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Others
{
    //represents web page entity
    public class WebPage
    {
        public Int32 PageId { get; set; }
        public String PageHtml { get; set; }
        public Int32 UserId { get; set; } 

        
        //some static functionality
        public static WebPage GetWebPageForRecord(DataRow row)
        {
            WebPage obj = new WebPage();
            obj.PageId = (int)row["PageId"];
            obj.PageHtml = row["PageHtml"].ToString();
            obj.UserId = (int)row["UserId"];

            return obj;
        }
    }
}
