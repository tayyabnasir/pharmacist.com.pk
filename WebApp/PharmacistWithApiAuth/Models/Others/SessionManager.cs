﻿using Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Others
{
    //information to be maintained in session for user
    public class SessionManager
    {
        public User user { get; set; }
        public DateTime loggedInAt { get; set; }

        public SessionManager(User u)
        {
            user = u;
            loggedInAt = DateTime.Now;
        }
    }
}
