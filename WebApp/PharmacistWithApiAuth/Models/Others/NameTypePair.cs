﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Others
{
    //to be used with creating parameterized queries
    public class NameTypePair
    {
        public String Name { get; set; }
        public SqlDbType Type { get; set; }
        public String Value { get; set; }

        public NameTypePair(String name, SqlDbType type, String value)
        {
            Name = name;
            Type = type;
            Value = value;
        }
    }
}
